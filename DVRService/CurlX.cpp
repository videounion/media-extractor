#include "StdAfx.h"
#include "CurlX.h"

size_t writeToString(void *ptr, size_t size, size_t count, void *stream) {
  ((string*)stream)->append((char*)ptr, 0, size*count);
  return size*count;
}

void showMessageCurlX(string message){
	//std::cout << message << std::endl;
}

int getSize( string path ) {
	// #include <fstream>
	FILE *pFile = NULL;

	// get the file stream
	fopen_s( &pFile, path.c_str(), "rb" );

	// set the file pointer to end of file
	fseek( pFile, 0, SEEK_END );

	// get the file size
	int Size = ftell( pFile );

	// return the file pointer to begin of file if you want to read it
	// rewind( pFile );

	// close stream and release buffer
	fclose( pFile );

	return Size;
}


curl_off_t fsize = 0;
int readComplete = 0;
std::string response = "";

std::string CurlX::get(string url, string name, string code, string request){
	response = "";
	CURL *curl;
	CURLcode res;

	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;
	struct curl_slist *headerlist = NULL;
	static const char buf[] = "Expect:";

	/* Fill in the name, code & request key for proper request field */
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "name", CURLFORM_COPYCONTENTS, &name, CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "code", CURLFORM_COPYCONTENTS, &code, CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "request", CURLFORM_COPYCONTENTS, &request, CURLFORM_END);

	/* get a curl handle */ 
	curl = curl_easy_init();

	/* initalize custom header list (stating that Expect: 100-continue is not
     wanted */
	headerlist = curl_slist_append(headerlist, buf);
	if(curl) {
		/* First set the URL that is about to receive our POST. This URL can
		   just as well be a https:// URL if that is what should receive the
		   data. */ 
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		/* only disable 100-continue header if explicitly requested */ 
		//curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		
		/* Now specify the POST data for authentication*/ 
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

		/* Now specify the general settings for call*/ 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToString);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);


		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L );
		
		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);
		
		/* Check for errors */ 
		if(res != CURLE_OK)	//curl_easy_strerror(res)
			showMessageCurlX("curl_easy_perform() failed");
 
		/* always cleanup */ 
		curl_easy_cleanup(curl);
	}
	/* always cleanup */ 
	curl_global_cleanup();
	return response;
}
 
/* NOTE: if you want this example to work on Windows with libcurl as a
   DLL, you MUST also provide a read callback with CURLOPT_READFUNCTION.
   Failing to do so will give you a crash since a DLL may not use the
   variable's memory when passed in to it from an app like this. */ 
static size_t read_callback(void *ptr, size_t size, size_t nmemb, FILE *stream){
	curl_off_t nread;
	/* in real-world cases, this would probably get this data differently
		as this fread() stuff is exactly what the library already would do
		by default internally */ 
	size_t retcode = fread(ptr, size, nmemb, stream);
 
	nread = (curl_off_t)retcode;

	readComplete+= (int)nread;
 
	//fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T " bytes from file\n", nread);
	//cout<<"            --Upload Done : " << (float)readComplete << "% of " << fsize << "\t\t\r";
	//cout.flush();
	return retcode;
}

std::string CurlX::ftpUpload( const char * local_file, const char * remote_url, string name ){
	readComplete = 0;
	string retUrl = "";

	CURL *curl;
	CURLcode res;
	FILE *hd_src;
	struct stat file_info;
	fsize = 0;

	struct curl_slist *headerlist = NULL;
 
	/* get the file size of the local file */ 
	if(stat(local_file, &file_info)) {
		showMessageCurlX("Couldnt open " + (string)local_file + ":" + strerror(errno));
		return "";
	}
	fsize = (curl_off_t)file_info.st_size;
 
	/* get a FILE * of the same file */ 
	hd_src = fopen(local_file, "rb");
 
	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);
 
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
		/* build a list of commands to pass to libcurl */ 
		//headerlist = curl_slist_append(headerlist, buf_1);
		//headerlist = curl_slist_append(headerlist, buf_2);
 
		/* we want to use our own read function */ 
		curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
 
		/* enable uploading */ 
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
 
		/* specify target */ 
		curl_easy_setopt(curl,CURLOPT_URL, remote_url);
 
		/* pass in that last of FTP commands to run after the transfer */ 
		curl_easy_setopt(curl, CURLOPT_POSTQUOTE, headerlist);
 
		/* now specify which file to upload */ 
		curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
 
		/* Set the size of the file to upload (optional).  If you give a *_LARGE
			option you MUST make sure that the type of the passed-in argument is a
			curl_off_t. If you use CURLOPT_INFILESIZE (without _LARGE) you must
			make sure that to pass in a type 'long' argument. */ 
		curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);
 
		/* Now run off and do what you've been told! */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "            --curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		else
			retUrl = name;
 
		/* clean up the FTP commands list */ 
		curl_slist_free_all (headerlist);
 
		/* always cleanup */ 
		curl_easy_cleanup(curl);
	}
	fclose(hd_src); /* close the local file */ 
 
	curl_global_cleanup();
	return retUrl;
}

std::string CurlX::upload( string url, string name, string code, string status, string status_message, boost::property_tree::ptree::value_type p, string filePath, string newName ) {
	//p.second.get<WORD>("dvr_port")
	//p.second.get_child("dvr_ip").data().c_str()

	string fileUrl = "";
	/* Fill in the file upload field */
	if((string)filePath.c_str() != ""){
		string s = p.second.get_child("ftp_link").data() + newName;
		const char * local_file = filePath.c_str();
		const char * remote_url = s.c_str();
		fileUrl = ftpUpload( local_file, remote_url, newName );
		
		if(fileUrl != ""){
			fileUrl = ((string)p.second.get_child("ftp_href").data() + (string)fileUrl).c_str();
		}

		//curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "media", CURLFORM_FILE, filePath.c_str(), CURLFORM_END);
		//curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "media", CURLFORM_FILE, filePath.c_str(), CURLFORM_CONTENTTYPE, "application/binary", CURLFORM_END);
		//fileSize+= getSize(filePath.c_str());
	}
	response = "";
	int fileSize = 1000;
	CURL *curl;
	CURLcode res;

	struct curl_httppost *formpost=NULL;
	struct curl_httppost *lastptr=NULL;
	struct curl_slist *headerlist=NULL;
	static const char buf[] = "Expect:";
 
	curl_global_init(CURL_GLOBAL_ALL);

	/* Fill name, code & request key for proper request field */
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "name", CURLFORM_COPYCONTENTS, name.c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "code", CURLFORM_COPYCONTENTS, code.c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "request", CURLFORM_COPYCONTENTS, "process", CURLFORM_END);

	/* Fill all general fields */
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "user", CURLFORM_COPYCONTENTS, p.second.get_child("user").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "queue_id", CURLFORM_COPYCONTENTS, p.second.get_child("queue_id").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "manifest", CURLFORM_COPYCONTENTS, p.second.get_child("mainfest_id").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "date", CURLFORM_COPYCONTENTS, p.second.get_child("date").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "start_time", CURLFORM_COPYCONTENTS, p.second.get_child("start_time").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "end_time", CURLFORM_COPYCONTENTS, p.second.get_child("end_time").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "camera_id", CURLFORM_COPYCONTENTS, p.second.get_child("dvr_channel").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "event_type", CURLFORM_COPYCONTENTS, p.second.get_child("event_type").data().c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "status", CURLFORM_COPYCONTENTS, status.c_str(), CURLFORM_END);
	curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "status_message", CURLFORM_COPYCONTENTS, status_message.c_str(), CURLFORM_END);

	/* Fill media if any*/
	if((string)fileUrl.c_str() != ""){
		curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "media", CURLFORM_COPYCONTENTS, fileUrl.c_str(), CURLFORM_END);	
	}
	curl = curl_easy_init();
	/* initalize custom header list (stating that Expect: 100-continue is not wanted */ 
	headerlist = curl_slist_append(headerlist, buf);
	if(curl) {
		/* what URL that receives this POST */ 
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		/* only disable 100-continue header if explicitly requested */ 
		//curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		
		/* Now specify the POST data for process*/ 
		//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, fileSize); 
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
		
		/* Now specify the general settings for call*/ 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToString);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L );

		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);

		/* Check for errors */ 
		if(res != CURLE_OK)
		  fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
 
		/* always cleanup */ 
		curl_easy_cleanup(curl);
 
		/* then cleanup the formpost chain */ 
		curl_formfree(formpost);
		/* free slist */ 
		curl_slist_free_all (headerlist);
	}
	/* always cleanup */ 
	curl_global_cleanup();
	std::cout << "\n" << response << "\n" << endl;
	return response;
}