#include "StdAfx.h"
#include "RequestProcess.h"

void showMessageRequest(string message){
	//std::cout << message << std::endl;
}

void showExactError(DWORD err){
	if(NET_API_ERR_NOERROR == err)
		std::cout << "No Error." << endl;
	else if(NET_API_ERR_PASSWORD_ERROR == err)
		std::cout << "User password error." << endl;
	else if(NET_API_ERR_NOENOUGHPRI == err)
		std::cout << "No enough priviage." << endl;
	else if(NET_API_ERR_NOINIT == err)
		std::cout << "Not initialized." << endl; 
	else if(NET_API_ERR_CHANNEL_ERROR == err)
		std::cout << "Channel number error." << endl;
	else if(NET_API_ERR_OVER_MAXLINK == err)
		std::cout << "Max link number reached." << endl;
	else if(NET_API_ERR_VERSIONNOMATCH == err)
		std::cout << "Version not match." << endl;
	else if(NET_API_ERR_NETWORK_FAIL_CONNECT == err)
		std::cout << "Connect to device failed." << endl;
	else if(NET_API_ERR_NETWORK_SEND_ERROR == err)
		std::cout << "Send data failed." << endl;
	else if(NET_API_ERR_NETWORK_RECV_ERROR == err)
		std::cout << "Receive data failed." << endl;
	else if(NET_API_ERR_NETWORK_RECV_TIMEOUT == err)
		std::cout << "Receive data timeout." << endl;
	else if(NET_API_ERR_NETWORK_ERRORDATA == err)
		std::cout << "Got error data." << endl;
	else if(NET_API_ERR_ORDER_ERROR == err)
		std::cout << "Error calling order." << endl;
	else if(NET_API_ERR_OPERNOPERMIT == err)
		std::cout << "Operation not permit." << endl;
	else if(NET_API_ERR_COMMANDTIMEOUT == err)
		std::cout << "Command timeout." << endl;
	else if(NET_API_ERR_ERRORSERIALPORT == err)
		std::cout << "Serial port error." << endl;
	else if(NET_API_ERR_ERRORALARMPORT == err)
		std::cout << "Alarm port error." << endl;
	else if(NET_API_ERR_PARAMETER_ERROR == err)
		std::cout << "Parameter error." << endl;
	else if(NET_API_ERR_CHAN_EXCEPTION == err)
		std::cout << "Channel error on device." << endl;
	else if(NET_API_ERR_NODISK == err)
		std::cout << "No hard driver detected on device." << endl;
	else if(NET_API_ERR_ERRORDISKNUM == err)
		std::cout << "Hard driver number error." << endl;
	else if(NET_API_ERR_DISK_FULL == err)
		std::cout << "Hard driver full on device." << endl;
	else if(NET_API_ERR_DISK_ERROR == err)
		std::cout << "Hard driver error on device." << endl;
	else if(NET_API_ERR_NOSUPPORT == err)
		std::cout << "Not support on device." << endl;
	else if(NET_API_ERR_BUSY == err)
		std::cout << "Device is busy." << endl;
	else if(NET_API_ERR_MODIFY_FAIL == err)
		std::cout << "Modification to device failed." << endl;
	else if(NET_API_ERR_PASSWORD_FORMAT_ERROR == err)
		std::cout << "Password format error." << endl;
	else if(NET_API_ERR_DISK_FORMATING == err)
		std::cout << "Hard driver is formatting on device." << endl;
	else if(NET_API_ERR_DVRNORESOURCE == err)
		std::cout << "Insufficient resource on device." << endl;
	else if(NET_API_ERR_DVROPRATEFAILED == err)
		std::cout << "Operation failed on device." << endl;
	else if(NET_API_ERR_OPENHOSTSOUND_FAIL == err)
		std::cout << "SDK open sound device failed." << endl;
	else if(NET_API_ERR_DVRVOICEOPENED == err)
		std::cout << "No more voice channel available." << endl;
	else if(NET_API_ERR_TIMEINPUTERROR == err)
		std::cout << "Time input error." << endl;
	else if(NET_API_ERR_NOSPECFILE == err)
		std::cout << "No filename specified." << endl;
	else if(NET_API_ERR_CREATEFILE_ERROR == err)
		std::cout << "Create file error." << endl;
	else if(NET_API_ERR_FILEOPENFAIL == err)
		std::cout << "Openf file error." << endl;
	else if(NET_API_ERR_OPERNOTFINISH == err)
		std::cout << "Last operation not finished." << endl;
	else if(NET_API_ERR_GETPLAYTIMEFAIL == err)
		std::cout << "Get played time failed." << endl;
	else if(NET_API_ERR_PLAYFAIL == err)
		std::cout << "Play failed." << endl;
	else if(NET_API_ERR_FILEFORMAT_ERROR == err)
		std::cout << "File format error." << endl;
	else if(NET_API_ERR_DIR_ERROR == err)
		std::cout << "Path error." << endl;
	else if(NET_API_ERR_ALLOC_RESOURCE_ERROR == err)
		std::cout << "Alloc resource failed." << endl;
	else if(NET_API_ERR_AUDIO_MODE_ERROR == err)
		std::cout << "Audio mode error." << endl;
	else if(NET_API_ERR_NOENOUGH_BUF == err)
		std::cout << "Not enough buffer." << endl;
	else if(NET_API_ERR_CREATESOCKET_ERROR == err)
		std::cout << "Create socket failed." << endl;
	else if(NET_API_ERR_SETSOCKET_ERROR == err)
		std::cout << "Setup socket failed." << endl;
	else if(NET_API_ERR_MAX_NUM == err)
		std::cout << "Max number exceeded." << endl;
	else if(NET_API_ERR_USERNOTEXIST == err)
		std::cout << "User not exist." << endl;
	else if(NET_API_ERR_WRITEFLASHERROR == err)
		std::cout << "Write flash error." << endl;
	else if(NET_API_ERR_UPGRADEFAIL == err)
		std::cout << "Device upgrade failed." << endl;
	else if(NET_API_ERR_CARDHAVEINIT == err)
		std::cout << "Card have been initialized." << endl;
	else if(NET_API_ERR_PLAYERFAILED == err)
		std::cout << "Player failed." << endl;
	else if(NET_API_ERR_MAX_USERNUM == err)
		std::cout << "Max user number exceeded." << endl;
	else if(NET_API_ERR_GETLOCALIPANDMACFAIL == err)
		std::cout << "Get local MAC or IP failed." << endl;
	else if(NET_API_ERR_NOENCODEING == err)
		std::cout << "No encoding on this channel." << endl;
	else if(NET_API_ERR_IPMISMATCH == err)
		std::cout << "IP address not match." << endl;
	else if(NET_API_ERR_MACMISMATCH == err)
		std::cout << "MAC address not match." << endl;
	else if(NET_API_ERR_UPGRADELANGMISMATCH == err)
		std::cout << "Upgrade using wrong language." << endl;
	else if(NET_API_ERR_MAX_PLAYERPORT == err)
		std::cout << "Max player number exceeded." << endl;
	else if(NET_API_ERR_NOSPACEBACKUP == err)
		std::cout << "Not enough space on backup device." << endl;
	else if(NET_API_ERR_NODEVICEBACKUP == err)
		std::cout << "No backup device found." << endl;
	else if(NET_API_ERR_PICTURE_BITS_ERROR == err)
		std::cout << "Picture bits format not supported." << endl;
	else if(NET_API_ERR_PICTURE_DIMENSION_ERROR == err)
		std::cout << "Picture dimension not supported." << endl;
	else if(NET_API_ERR_PICTURE_SIZ_ERROR == err)
		std::cout << "Picture size error." << endl;
	else if(NET_API_ERR_LOADPLAYERSDKFAILED == err)
		std::cout << "Load player sdk error." << endl;
	else if(NET_API_ERR_LOADPLAYERSDKPROC_ERROR == err)
		std::cout << "Player sdk interface not found." << endl;
	else if(NET_API_ERR_LOADDSSDKFAILED == err)
		std::cout << "Load sdk error." << endl;
	else if(NET_API_ERR_LOADDSSDKPROC_ERROR == err)
		std::cout << "SDK interface not found." << endl;
	else if(NET_API_ERR_DSSDK_ERROR == err)
		std::cout << "SDK error." << endl;
	else if(NET_API_ERR_VOICEMONOPOLIZE == err)
		std::cout << "Audio device is not available." << endl;
	else if(NET_API_ERR_JOINMULTICASTFAILED == err)
		std::cout << "Join multicast group failed." << endl;
	else if(NET_API_ERR_CREATEDIR_ERROR == err)
		std::cout << "Create directory failed." << endl;
	else if(NET_API_ERR_BINDSOCKET_ERROR == err)
		std::cout << "Bind socket failed." << endl;
	else if(NET_API_ERR_SOCKETCLOSE_ERROR == err)
		std::cout << "Socket closed." << endl;
	else if(NET_API_ERR_USERID_ISUSING == err)
		std::cout << "User is busy." << endl;
	else if(NET_API_ERR_SOCKETLISTEN_ERROR == err)
		std::cout << "Socket listen failed." << endl;
	else if(NET_API_ERR_PROGRAM_EXCEPTION == err)
		std::cout << "Programe exception." << endl;
	else if(NET_API_ERR_WRITEFILE_FAILED == err)
		std::cout << "Write file failed." << endl;
	else if(NET_API_ERR_FORMAT_READONLY == err)
		std::cout << "Format read only disk." << endl;
	else if(NET_API_ERR_WITHSAMEUSERNAME == err)
		std::cout << "User name already exist." << endl;
	else if(NET_API_ERR_DEVICETYPE_ERROR == err)
		std::cout << "Device type error." << endl;
	else if(NET_API_ERR_LANGUAGE_ERROR == err)
		std::cout << "Language not match." << endl;
	else if(NET_API_ERR_PARAVERSION_ERROR == err)
		std::cout << "Parameter version not match." << endl;
	else if(NET_API_ERR_IPCHAN_NOTALIVE == err)
		std::cout << "IPC offline." << endl;
	else if(NET_API_ERR_RTSP_SDK_ERROR == err)
		std::cout << "Load RTSP sdk failed." << endl;
	else if(NET_API_ERR_CONVERT_SDK_ERROR == err)
		std::cout << "Load converter failed." << endl;
	else if(NET_API_ERR_IPC_COUNT_OVERFLOW == err)
		std::cout << "IPC number exceeded." << endl;
	else if(NET_API_ERR_INVALID_USERID == err)
		std::cout << "Invalid User ID." << endl;
	else if(NET_API_ERR_INVALID_HANDLE == err)
		std::cout << "Invalid Handle." << endl;
}

string exec(string cmd) {
    FILE* pipe = _popen(cmd.c_str(), "r");
    if (!pipe)
		return "ERROR";
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
    	if(fgets(buffer, 128, pipe) != NULL)
    		result+= buffer;
    }
    _pclose(pipe);
    return result;
}

bool fileExists(boost::filesystem::path & destination){
	if( boost::filesystem::exists(destination) && boost::filesystem::is_regular_file(destination) ){ 
		return true;
	}
	return false;
}

bool createDir(boost::filesystem::path & destination){
	if( boost::filesystem::exists( destination ) ){  
        return true;  
	} else {
		return boost::filesystem::create_directory( destination );
	}
	return false;
}

string newUUID() {
	boost::uuids::uuid uuid = boost::uuids::random_generator()();
	std::stringstream ss;
	ss << uuid;
	return ss.str();
}

int getDVRFile(LONG hDVR, string fileName, string localFile){
	int pos = 0;
	// download this file
	LONG df = NET_API_GetFileByName(hDVR, fileName.c_str(), localFile.c_str());
	if(-1 == df) {
		return -2;
	}

	while(-1 != pos && pos != 100) {
		pos = NET_API_GetDownloadPos(df);
		//std::cout << "            --Dowload Done : " << pos << "%\r";
		//cout.flush();
		delay(10);
	}
	NET_API_StopGetFile(df);
	showMessageRequest("\n");
	delay(5000);
	return pos;
}

string oExt = ".mp4";

RequestProcess::RequestProcess(void){ }

RequestProcess::~RequestProcess(void){ }

void downloadVideo(std::string url, std::string name, std::string code, LONG hDVR, boost::property_tree::ptree::value_type p){
	CurlX c;
	map<int, string> downloadedFiles;
	map<int, string> downloadedMp4Files;
	string uuid = newUUID();

	std::stringstream json;
	boost::property_tree::ptree pt;

	NET_API_TIME b, e;
#define fill_time(a, y, m, d, h, mm, s) 	a.dwYear = y;	a.dwMonth = m;	a.dwDay = d;	a.dwHour = h;	a.dwMinute = mm;	a.dwSecond = s;

	string globalStatus = "Processing";

	c.upload(url, name, code, globalStatus, "", p, "", "");

	fill_time(b, p.second.get<DWORD>("start_year"), p.second.get<DWORD>("start_month"), p.second.get<DWORD>("start_cal_date"), p.second.get<DWORD>("start_hour"), p.second.get<DWORD>("start_minute"), p.second.get<DWORD>("start_second"));
	fill_time(e, p.second.get<DWORD>("end_year"), p.second.get<DWORD>("end_month"), p.second.get<DWORD>("end_cal_date"), p.second.get<DWORD>("end_hour"), p.second.get<DWORD>("end_minute"), p.second.get<DWORD>("end_second"));

	bool no_error = true;
	int file_count = 0;

	LONG ff = NET_API_FindFile(hDVR, p.second.get<LONG>("dvr_channel"), RECORD_BY_RESUMING, &b, &e);
	if (ff != -1) {
		globalStatus = "Failed";
		int ff_ret = 0;
		NET_API_FIND_DATA fd;

		std::string local_path("dvr_videos/");

		do {
			// check files
			ff_ret = NET_API_FindNextFile(ff, &fd);
			if(ff_ret != NET_API_FILE_SUCCESS) {		
				no_error = (NET_API_NOMOREFILE == ff_ret || NET_API_FILE_NOFIND == ff_ret); // set no error flag
				break;
			}

			int pos = 0;

			if(fileExists(boost::filesystem::path(local_path + (string)fd.sFileName)) && fileExists(boost::filesystem::path(local_path + (string)fd.sFileName + oExt))){
				pos = 100;
				downloadedMp4Files.insert(std::pair<int, string>(downloadedMp4Files.size(), (string)fd.sFileName + oExt));
				showMessageRequest("Status : Download complete file to " + local_path + fd.sFileName);
			} else {
				//Attempt multitime
				int ati = 0;
				for(ati = 0; ati <= 3; ati++){
					std::cout << "\n\nAttempt for : " << ati << " time(s)\n\n" << endl;
					pos = getDVRFile(hDVR, fd.sFileName, (local_path + (string)fd.sFileName));
					if(pos == 100) {
						//exec("ffmpeg -y -i dvr_videos/" + (string)fd.sFileName + " -c:v libx264 -preset slow -crf 22 -c:a copy dvr_videos/" + (string)fd.sFileName + oExt);
						if(fileExists(boost::filesystem::path(local_path + (string)fd.sFileName + oExt))){
							downloadedMp4Files.insert(std::pair<int, string>(downloadedMp4Files.size(), (string)fd.sFileName + oExt));
							ati = 4;
						} else {
							exec("TVCC.exe -f dvr_videos/" + (string)fd.sFileName + " -o dvr_videos/" + (string)fd.sFileName + oExt);
							if(fileExists(boost::filesystem::path(local_path + (string)fd.sFileName + oExt))){
								downloadedMp4Files.insert(std::pair<int, string>(downloadedMp4Files.size(), (string)fd.sFileName + oExt));
								ati = 4;
							}
						}
						if(ati < 3){
							remove((local_path + (string)fd.sFileName).c_str());
						}
					}
				}
			}
			
			if(pos == 100) {
				// reset downloaded file's flag
				string uploadFile = local_path + fd.sFileName;
				downloadedFiles.insert(std::pair<int, string>(downloadedFiles.size(), fd.sFileName));
			} else {
				showMessageRequest("Download not complete");
			}
		} while(ff_ret == NET_API_FILE_SUCCESS);

		ofstream __join;
		__join.open ("dvr_videos/" + uuid + ".txt", std::ofstream::out | std::ofstream::app);
		for( map<int, string>::iterator dcf = downloadedMp4Files.begin(); dcf != downloadedMp4Files.end(); ++dcf){
			__join << "-f " << (string)(*dcf).second << "\n";
		}
		__join.close();

		//Str
		if(p.second.get_child("media_type").data() == "Video"){
			if(fileExists(boost::filesystem::path("dvr_videos/" + uuid + ".txt"))){
				//exec("ffmpeg -y -f concat -i dvr_videos/" + uuid + ".txt -c copy dvr_videos/" + uuid + oExt);
				//exec("TVCC.exe -y -ffv dvr_videos/" + uuid + ".txt -o dvr_videos/" + uuid + oExt);
				remove(("dvr_videos/" + uuid + ".txt").c_str());
			}

			if(fileExists(boost::filesystem::path("dvr_videos/" + uuid + oExt))){
				delay(2000);

				string uploadName = (string)uuid + oExt;
				string uploadFile = local_path + uploadName;

				globalStatus = "Processed";

				json << c.upload(url, name, code, "Processing", "DRV Server is trying to upload the file.", p, uploadFile, uploadName);
				boost::property_tree::read_json(json, pt);
				if(pt.get_child("set.code").data() == "SUCCESS"){
					showMessageRequest("Message : " + pt.get_child("set.message").data());
					showMessageRequest("Record ID(s) : " + pt.get_child("set.result.record_id").data());
				} else {
					showMessageRequest("Message : " + pt.get_child("set.message").data());
				}
				//remove(("dvr_videos/" + uuid + oExt).c_str());
			} else {
				for( map<int, string>::iterator df = downloadedMp4Files.begin(); df != downloadedMp4Files.end(); ++df){
					//exec("ffmpeg.exe -y -i dvr_videos/" + (string)(*df).second + " dvr_videos/" + (string)(*df).second + oExt);
					//delay(5000);
					if(fileExists(boost::filesystem::path("dvr_videos/" + (string)(*df).second + oExt))){
						string uploadName = (string)(*df).second + oExt;
						string uploadFile = local_path + uploadName;

						globalStatus = "Processed";
						
						json << c.upload(url, name, code, "Processing", "DRV Server is trying to upload the file.", p, uploadFile, uploadName);
						boost::property_tree::read_json(json, pt);
						if(pt.get_child("set.code").data() == "SUCCESS"){
							showMessageRequest("Message : " + pt.get_child("set.message").data());
							showMessageRequest("Record ID(s) : " + pt.get_child("set.result.record_id").data());
						} else {
							showMessageRequest("Message : " + pt.get_child("set.message").data());
						}
					} else if(fileExists(boost::filesystem::path("dvr_videos/" + (string)(*df).second))){
						string uploadName = (string)(*df).second;
						string uploadFile = local_path + uploadName;

						globalStatus = "Processed";
						
						json << c.upload(url, name, code, "Processing", "File is not converted. DRV Server is trying to upload the file.", p, uploadFile, uploadName);
						boost::property_tree::read_json(json, pt);
						if(pt.get_child("set.code").data() == "SUCCESS"){
							showMessageRequest("Message : " + pt.get_child("set.message").data());
							showMessageRequest("Record ID(s) : " + pt.get_child("set.result.record_id").data());
						} else {
							showMessageRequest("Message : " + pt.get_child("set.message").data());
						}
					}
					delay(100);
				}
			}
		} else if(p.second.get_child("media_type").data() == "Image"){
			for( map<int, string>::iterator df = downloadedFiles.begin(); df != downloadedFiles.end(); ++df){
				if(createDir(boost::filesystem::path( "dvr_videos/dvr_images/" + (string)(*df).second ))){
					showMessageRequest("Status : Directory ceated successfully");
						
					exec("ffmpeg.exe -i dvr_videos/" + (string)(*df).second + " -r 4 -f image2 dvr_videos/dvr_images/" + (string)(*df).second + "/" + (string)(*df).second + "_%05d.jpeg");

					boost::filesystem::path targetDir( "dvr_videos/dvr_images/" + (string)(*df).second );
					boost::filesystem::directory_iterator eod;

					if ( boost::filesystem::exists(targetDir) && boost::filesystem::is_directory(targetDir)){
						for( boost::filesystem::directory_iterator dir_iter(targetDir) ; dir_iter != eod ; ++dir_iter){
							if (boost::filesystem::is_regular_file(dir_iter->status()) ){
								std::string uploadFileName = dir_iter->path().filename().string();
								std::string uploadFile = targetDir.string() + "/" + uploadFileName;
								// Submit to server and Processing
								globalStatus = "Processed";
								json << c.upload(url, name, code, "Processing", "DRV Server is trying to upload the file.", p, uploadFile, uploadFileName);
								boost::property_tree::read_json(json, pt);
								if(pt.get_child("set.code").data() == "SUCCESS"){
									showMessageRequest("Message : " + pt.get_child("set.message").data());
									showMessageRequest("Record ID(s) : " + pt.get_child("set.result.record_id").data());
								} else {
									showMessageRequest("Message : " + pt.get_child("set.message").data());
								}
							}
						}
					}
				} else {
					showMessageRequest("Status : Directory not ceated");
				}
			}
		}

		// end of find
		NET_API_FindClose(ff);
		// Submit to server and Processed
		json << c.upload(url, name, code, globalStatus, "Request process complete.", p, "", "");
		boost::property_tree::read_json(json, pt);
		if(pt.get_child("set.code").data() == "SUCCESS"){
			showMessageRequest("Message : " + pt.get_child("set.message").data());
		}
	}
}

bool RequestProcess::start(std::string url, std::string name, std::string code, boost::property_tree::ptree::value_type p){
	CurlX c;
	try {
		if(NET_API_Init()) {
			NET_API_DEVICEINFO DeviceInfo;
			LONG hDVR = NET_API_Login(p.second.get_child("dvr_ip").data().c_str(), p.second.get<WORD>("dvr_port"), p.second.get_child("dvr_user").data().c_str(), p.second.get_child("dvr_password").data().c_str(), &DeviceInfo);
			if (hDVR != 0) {
				downloadVideo(url, name, code, hDVR, p);
				NET_API_Logout(hDVR);
				NET_API_Cleanup();
				return true;
			} else {
				showExactError(NET_API_GetLastError());
				c.upload(url, name, code, "Failed", "DVR Login Error.", p, "", "");
				NET_API_Cleanup();
				return false;
			}
		}
		c.upload(url, name, code, "Failed", "DVR SDK Init Error.", p, "", "");
		NET_API_Cleanup();
		return false;
	} catch (std::exception const& e) {
		NET_API_Cleanup();
		showMessageRequest(e.what());
	}
	return false;
}

bool RequestProcess::test(std::string url, std::string name){
	exec("ffmpeg -y -f concat -i dvr_videos/" + url + ".txt -c dvr_videos/" + name + oExt);
	return false;
}