#ifndef _platform_adpater_api_h
#define _platform_adpater_api_h

#include "H264_NET_API.h"

#ifdef   __cplusplus
extern "C" {
#endif //__cplusplus

#ifdef WIN32
#ifdef DLL_EXPORT
#define PLATFORM_ADAPTER_API extern "C"__declspec(dllexport) 
#else 
#define PLATFORM_ADAPTER_API extern "C"__declspec(dllimport) 
#endif //DLL_EXPORT
#else
#define PLATFORM_ADAPTER_API 
#endif //WIN32

struct DVR_LOGIN_INFO
{
	const char* sDVRAddr;
	unsigned short iDVRPort;
	const char* sUser;
	const char* sPassword;
};

struct SERVER_LOGIN_INFO
{
	const char* sServerAddr;
	unsigned short iServerPort;
	const char* sUser;
	const char* sPassword;
};

PLATFORM_ADAPTER_API BOOL CAL_CALL platform_adapter_startup(DVR_LOGIN_INFO *dvr_login_info, SERVER_LOGIN_INFO *server_login_info);

PLATFORM_ADAPTER_API BOOL CAL_CALL platform_adapter_cleanup();

#ifdef   __cplusplus
}
#endif //__cplusplus

#endif //_platform_adpater_api_h

