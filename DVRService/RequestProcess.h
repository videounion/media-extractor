#pragma once
class RequestProcess
{
public:
	RequestProcess(void);
	~RequestProcess(void);
	bool start(std::string url, std::string name, std::string code, boost::property_tree::ptree::value_type p);
	bool test(std::string url, std::string name);
};

