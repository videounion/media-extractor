// DVRService.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

void showMessage(string message){
	//std::cout << message << std::endl;
}

string url = "";
string name = "";
string code = "";

CurlX c;
map<string, bool> dvrIPProcessing;

std::stringstream json;
boost::property_tree::ptree pt;
bool serverRun = true;
boost::thread_group tgroup;

void requestProcess(boost::property_tree::ptree::value_type v){
	std::string key = v.first.data();
	//showMessage("Thread start for Queue ID: " + key);
	std::cout << "Thread start for Queue ID: " + key << endl;
	RequestProcess rp;
	rp.start(url, name, code, v);
}

void processQueues(){
	json << c.get(url, name, code, "queue");
	boost::property_tree::read_json(json, pt);
	if(pt.get_child("set.code").data() == "SUCCESS"){
		showMessage("\n");
		showMessage(pt.get_child("set.message").data());
		BOOST_FOREACH(boost::property_tree::ptree::value_type v, pt.get_child("set.request")){
			BOOST_FOREACH(boost::property_tree::ptree::value_type v1, v.second){
				dvrIPProcessing.insert(std::pair<string, bool>(v1.second.get_child("dvr_ip").data(),true));
				requestProcess(v1);
				//tgroup.create_thread( boost::bind( &requestProcess, v1) );
			}
		}
		
		//tgroup.join_all();
		dvrIPProcessing.clear();
	} else if(pt.get_child("set.code").data() == "WARNING"){
		std::cout << pt.get_child("set.message").data() << "\r";
		std::cout.flush();
	} else {
		std::cout << pt.get_child("set.message").data() << "\r";
		std::cout.flush();
	}
}

bool startServer(){
	// Cecking if al credential is correct.
	//showMessage("Checking URL and Authentiation.");
	std::cout << "Checking URL and Authentiation." << endl;
	json << c.get(url, name, code, "check");
	boost::property_tree::read_json(json, pt);
	if(pt.get_child("set.code").data() == "SUCCESS"){
		//showMessage(pt.get_child("set.message").data());
		std::cout << pt.get_child("set.message").data() << endl;
		while(serverRun){
			processQueues();
		}
		return true;
	} else {
		std::cout << pt.get_child("set.message").data() << endl;
	}
	return false;
}

int _tmain(int argc, _TCHAR* argv[]){
	showMessage("DVR Server is trying to start.");
	try {
		boost::property_tree::ptree pt;
		boost::property_tree::read_json("data.json", pt);
		BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("settings")){
			std::string key = v.first.data();
			std::string value = v.second.data();
			if(key == "url"){
				url = value;
			} else if(key == "name"){
				name = value;
			} else if(key == "code"){
				code = value;
			}
        }
		startServer();
		delay(5000);
		return EXIT_SUCCESS;
	} catch (std::exception const& e) {
		std::cerr << e.what() << std::endl;
	}
	delay(5000);
	return EXIT_FAILURE;
}

