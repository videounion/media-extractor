/************************************************************************    
    File:   VersionBase.h
************************************************************************/
#define FILE_VERSIONSTRING     "1.6.5.10-Release\0"
#define PRODUCT_VERSIONSTRING  "20140425\0"
#define FILE_MAJOR             1
#define FILE_MINOR             6
#define FILE_BUILD             5
#define FILE_REVISION          10

#define LEGAL_COPYRIGHT   "Copyright (C) 2009-2014\0"
