/** \file	H264_NET_API.h
 *  \eng
 *  \brief	This file contains interfaces for access devices.
 *	\else
 *  \brief	此文件包含访问设备的接口函数
 *	\endif
 *  \author Jian.Xiao
 *	\version 1.0
 */

#ifndef _H264_NET_API_H_
#define _H264_NET_API_H_

#ifdef   __cplusplus
extern "C" {
#endif

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#ifndef _CRT_SECURE_NO_WARNINGS 
	#define _CRT_SECURE_NO_WARNINGS 
	#endif

	#include <Windows.h>
	#define CAL_CALL __stdcall

	#ifdef DLL_EXPORT
	#define NET_API extern "C"__declspec(dllexport) 
	#else
	#define NET_API extern "C"__declspec(dllimport)
	#endif
#else
#ifdef __x86_64__
	typedef void* HWND;
	typedef int LONG;
	typedef unsigned short USHORT;
	typedef unsigned int ULONG;
	typedef int BOOL;
	typedef unsigned char BYTE;
	typedef unsigned short WORD;
	typedef unsigned int DWORD;
	typedef unsigned int UINT;
	typedef unsigned int COLORREF;
	typedef void* HDC;
	typedef void* LPVOID;
	typedef unsigned int* LPDWORD;
	#define NET_API
	#define CAL_CALL
	#ifndef HANDLE
		typedef void* HANDLE;
	#endif
	typedef long LONG_PTR;
	typedef long int LONG64;
#else
	typedef void* HWND;
	typedef long LONG;
	typedef unsigned short USHORT;
	typedef unsigned long ULONG;
	typedef int BOOL;
	typedef unsigned char BYTE;
	typedef unsigned short WORD;
	typedef unsigned int DWORD;
	typedef unsigned int UINT;
	typedef unsigned int COLORREF;
	typedef void* HDC;
	typedef void* LPVOID;
	typedef unsigned int* LPDWORD;
	#define NET_API
	#define CAL_CALL
	#ifndef HANDLE
		typedef void* HANDLE;
	#endif
	typedef long LONG_PTR;
	typedef long long LONG64;
#endif

	#ifndef CALLBACK
	#define CALLBACK
	#endif

	#ifndef TRUE
	#define TRUE 1
	#endif

	#ifndef FALSE
	#define FALSE 0
	#endif
#endif

//! \~english Default values \~chinese 默认数值
enum enumDefautValues
{
	MAX_ETHERNET       = 2,
	MAX_ALARMOUT       = 4,
	MAX_DAYS           = 7,
	MAX_TIMESEGMENT    = 4,
	MAX_CHANNUM        = 32,
	MAX_PRESET         = 128,
	MAX_SHELTERNUM     = 4,
	MAX_RIGHT          = 32,
	MAX_USERNUM        = 16,
	MAX_EXCEPTIONNUM   = 16,
	MAX_LINK           = 6,
	MAX_DISKNUM        = 16,
	MAX_ALARMIN        = 32,
	MAX_WINDOW         = 20,
	MAX_VIDEOOUT       = 2,
	MAX_FORMATCOUNT    = 12,
	MAX_VGA            = 1,
	MAX_NFS_DISK       = 8,
	MAX_STRINGNUM      = 4,
	MAX_CRUISE_POINT   = 16,
	PICNAME_MAXITEM    = 15,
	SERIALNO_LEN       = 48,
	MAILADDR_LEN       = 48,
	NAME_LEN           = 32,
	NAME_LEN_V20      = 50,
	MACADDR_LEN        = 6,
	PATHNAME_LEN       = 128,
	PASSWD_LEN         = 16,
	PHONENUMBER_LEN    = 32,
	MULTICAST_PORT_START   = 30000, //!< \~english The multicast port \~chinese 组播起始端口
	SERVER_PORT		   = 30000, //!< \~english The default device serve port \~chinese 默认服务端口
	MAX_CVBS		   = 4,
	MAX_STRINGNUM_EX   = 8,
	MAX_USER_OSD		   = 2,
	MAX_LANE_NUM	   = 2,
	//---------------cls440x decoder add begin---------------------
	MAX_IPADDRESS_LEN  = 16,
	MAX_PASSWORD_LEN   = 16,
	MAX_DEC_POOL_NUM   = 9,
	MAX_DEC_WIN_DIS_NUM = 9,
	//---------------cls440x decoder add end---------------------
};
	
//! \~english PTZ types \~chinese 云台类型
enum enumPTZTypes
{
	YouLi                     =	0,
	PELCO_P                   = 3,
	PELCO_D                   = 8
};

//! \~chinese SDK 错误代码 \~english SDK error codes
enum enumErrorCodes
{
    NET_API_ERR_NOERROR 					=0	,//!< \~chinese 没有错误 \~english No Error
    NET_API_ERR_PASSWORD_ERROR 				=1	,//!< \~chinese 用户名密码错误 \~english User password error
    NET_API_ERR_NOENOUGHPRI 				=2	,//!< \~chinese 权限不足 \~english No enough priviage
    NET_API_ERR_NOINIT 						=3	,//!< \~chinese 没有初始化 \~english Not initialized
    NET_API_ERR_CHANNEL_ERROR 				=4	,//!< \~chinese 通道号错误 \~english Channel number error
    NET_API_ERR_OVER_MAXLINK 				=5	,//!< \~chinese 连接到设备的客户端个数超过最大 \~english Max link number reached
    NET_API_ERR_VERSIONNOMATCH				=6	,//!< \~chinese 版本不匹配 \~english Version not match
    NET_API_ERR_NETWORK_FAIL_CONNECT		=7	,//!< \~chinese 连接服务器失败 \~english Connect to device failed
    NET_API_ERR_NETWORK_SEND_ERROR			=8	,//!< \~chinese 向服务器发送失败 \~english Send data failed
    NET_API_ERR_NETWORK_RECV_ERROR			=9	,//!< \~chinese 从服务器接收数据失败 \~english Receive data failed
    NET_API_ERR_NETWORK_RECV_TIMEOUT		=10	,//!< \~chinese 从服务器接收数据超时 \~english Receive data timeout
    NET_API_ERR_NETWORK_ERRORDATA			=11	,//!< \~chinese 传送的数据有误 \~english Got error data
    NET_API_ERR_ORDER_ERROR					=12	,//!< \~chinese 调用次序错误 \~english Error calling order
    NET_API_ERR_OPERNOPERMIT				=13	,//!< \~chinese 无此权限 \~english Operation not permit
    NET_API_ERR_COMMANDTIMEOUT				=14	,//!< \~chinese 设备命令执行超时 \~english Command timeout
    NET_API_ERR_ERRORSERIALPORT				=15	,//!< \~chinese 串口号错误 \~english Serial port error
    NET_API_ERR_ERRORALARMPORT				=16	,//!< \~chinese 报警端口错误 \~english Alarm port error
    NET_API_ERR_PARAMETER_ERROR 			=17  ,//!< \~chinese 参数错误 \~english Parameter error
    NET_API_ERR_CHAN_EXCEPTION				=18	,//!< \~chinese 服务器通道处于错误状态 \~english Channel error on device
    NET_API_ERR_NODISK						=19	,//!< \~chinese 没有硬盘 \~english No hard driver detected on device
    NET_API_ERR_ERRORDISKNUM				=20	,//!< \~chinese 硬盘号错误 \~english Hard driver number error
    NET_API_ERR_DISK_FULL					=21	,//!< \~chinese 服务器硬盘满 \~english Hard driver full on device
    NET_API_ERR_DISK_ERROR					=22	,//!< \~chinese 服务器硬盘出错 \~english Hard driver error on device
    NET_API_ERR_NOSUPPORT					=23	,//!< \~chinese 服务器不支持 \~english Not support on device 
    NET_API_ERR_BUSY						=24	,//!< \~chinese 服务器忙 \~english Device is busy
    NET_API_ERR_MODIFY_FAIL					=25	,//!< \~chinese 服务器修改不成功 \~english Modification to device failed
    NET_API_ERR_PASSWORD_FORMAT_ERROR		=26	,//!< \~chinese 密码输入格式不正确 \~english Password format error
    NET_API_ERR_DISK_FORMATING				=27	,//!< \~chinese 硬盘正在格式化, 不能启动操作 \~english Hard driver is formatting on device
    NET_API_ERR_DVRNORESOURCE				=28	,//!< \~chinese 设备资源不足 \~english Insufficient resource on device
    NET_API_ERR_DVROPRATEFAILED				=29  ,//!< \~chinese 设备操作失败 \~english Operation failed on device
    NET_API_ERR_OPENHOSTSOUND_FAIL 			=30  ,//!< \~chinese 打开PC声音失败 \~english SDK open sound device failed
    NET_API_ERR_DVRVOICEOPENED 				=31  ,//!< \~chinese 服务器语音对讲被占用 \~english No more voice channel available 
    NET_API_ERR_TIMEINPUTERROR				=32  ,//!< \~chinese 时间输入不正确 \~english Time input error
    NET_API_ERR_NOSPECFILE					=33  ,//!< \~chinese 回放时服务器没有指定的文件 \~english No filename specified
    NET_API_ERR_CREATEFILE_ERROR			=34	,//!< \~chinese 创建文件出错 \~english Create file error
    NET_API_ERR_FILEOPENFAIL				=35  ,//!< \~chinese 打开文件出错 \~english Openf file error
    NET_API_ERR_OPERNOTFINISH				=36  ,//!< \~chinese 上次的操作还没有完成 \~english Last operation not finished
    NET_API_ERR_GETPLAYTIMEFAIL				=37  ,//!< \~chinese 获取当前播放的时间出错 \~english Get played time failed
    NET_API_ERR_PLAYFAIL					=38  ,//!< \~chinese 播放出错 \~english Play failed
    NET_API_ERR_FILEFORMAT_ERROR			=39  ,//!< \~chinese 文件格式不正确 \~english File format error
    NET_API_ERR_DIR_ERROR					=40	,//!< \~chinese 路径错误 \~english Path error
    NET_API_ERR_ALLOC_RESOURCE_ERROR		=41  ,//!< \~chinese 资源分配错误 \~english Alloc resource failed
    NET_API_ERR_AUDIO_MODE_ERROR			=42	,//!< \~chinese 声卡模式错误 \~english Audio mode error
    NET_API_ERR_NOENOUGH_BUF				=43	,//!< \~chinese 缓冲区太小 \~english Not enough buffer
    NET_API_ERR_CREATESOCKET_ERROR		 	=44	,//!< \~chinese 创建SOCKET出错 \~english Create socket failed
    NET_API_ERR_SETSOCKET_ERROR				=45	,//!< \~chinese 设置SOCKET出错 \~english Setup socket failed
    NET_API_ERR_MAX_NUM						=46	,//!< \~chinese 个数达到最大 \~english Max number exceeded
    NET_API_ERR_USERNOTEXIST				=47	,//!< \~chinese 用户不存在 \~english User not exist
    NET_API_ERR_WRITEFLASHERROR				=48  ,//!< \~chinese 写FLASH出错 \~english Write flash error
    NET_API_ERR_UPGRADEFAIL					=49  ,//!< \~chinese 设备升级失败 \~english Device upgrade failed
    NET_API_ERR_CARDHAVEINIT				=50  ,//!< \~chinese 解码卡已经初始化过 \~english Card have been initialized
    NET_API_ERR_PLAYERFAILED				=51	,//!< \~chinese 调用播放库中某个函数失败 \~english Player failed
    NET_API_ERR_MAX_USERNUM					=52  ,//!< \~chinese 设备端用户数达到最大 \~english Max user number exceeded
    NET_API_ERR_GETLOCALIPANDMACFAIL		=53  ,//!< \~chinese 获得客户端的IP地址或物理地址失败 \~english Get local MAC or IP failed
    NET_API_ERR_NOENCODEING					=54	,//!< \~chinese 该通道没有编码 \~english No encoding on this channel
    NET_API_ERR_IPMISMATCH					=55	,//!< \~chinese IP地址不匹配 \~english IP address not match
    NET_API_ERR_MACMISMATCH					=56	,//!< \~chinese MAC地址不匹配 \~english MAC address not match
    NET_API_ERR_UPGRADELANGMISMATCH			=57	,//!< \~chinese 升级文件语言不匹配 \~english Upgrade using wrong language
    NET_API_ERR_MAX_PLAYERPORT				=58	,//!< \~chinese 播放器路数达到最大 \~english Max player number exceeded
    NET_API_ERR_NOSPACEBACKUP				=59	,//!< \~chinese 备份设备中没有足够空间进行备份 \~english Not enough space on backup device
    NET_API_ERR_NODEVICEBACKUP				=60	,//!< \~chinese 没有找到指定的备份设备 \~english No backup device found
    NET_API_ERR_PICTURE_BITS_ERROR			=61	,//!< \~chinese 图像素位数不符, 限24色 \~english Picture bits format not supported
    NET_API_ERR_PICTURE_DIMENSION_ERROR		=62	,//!< \~chinese 图片高*宽超限,  限128*256 \~english Picture dimension not supported
    NET_API_ERR_PICTURE_SIZ_ERROR			=63	,//!< \~chinese 图片大小超限, 限100K \~english Picture size error
    NET_API_ERR_LOADPLAYERSDKFAILED			=64	,//!< \~chinese 载入当前目录下Player Sdk出错 \~english Load player sdk error
    NET_API_ERR_LOADPLAYERSDKPROC_ERROR		=65	,//!< \~chinese 找不到Player Sdk中某个函数入口 \~english Player sdk interface not found
    NET_API_ERR_LOADDSSDKFAILED				=66	,//!< \~chinese 载入当前目录下DSsdk出错 \~english Load sdk error
    NET_API_ERR_LOADDSSDKPROC_ERROR		    =67	,//!< \~chinese 找不到DsSdk中某个函数入口 \~english SDK interface not found
    NET_API_ERR_DSSDK_ERROR					=68	,//!< \~chinese 调用硬解码库DsSdk中某个函数失败 \~english SDK error
    NET_API_ERR_VOICEMONOPOLIZE				=69	,//!< \~chinese 声卡被独占 \~english Audio device is not available
    NET_API_ERR_JOINMULTICASTFAILED			=70	,//!< \~chinese 加入多播组失败 \~english Join multicast group failed
    NET_API_ERR_CREATEDIR_ERROR				=71	,//!< \~chinese 建立日志文件目录失败 \~english Create directory failed
    NET_API_ERR_BINDSOCKET_ERROR			=72	,//!< \~chinese 绑定套接字失败 \~english Bind socket failed
    NET_API_ERR_SOCKETCLOSE_ERROR			=73	,//!< \~chinese socket连接中断, 此错误通常是由于连接中断或目的地不可达 \~english Socket closed
    NET_API_ERR_USERID_ISUSING			    =74	,//!< \~chinese 注销时用户ID正在进行某操作 \~english User is busy
    NET_API_ERR_SOCKETLISTEN_ERROR			=75	,//!< \~chinese 监听失败 \~english Socket listen failed
    NET_API_ERR_PROGRAM_EXCEPTION			=76	,//!< \~chinese 程序异常 \~english Programe exception
    NET_API_ERR_WRITEFILE_FAILED			=77	,//!< \~chinese 写文件失败 \~english Write file failed
    NET_API_ERR_FORMAT_READONLY				=78  ,//!< \~chinese 禁止格式化只读硬盘 \~english Format read only disk
    NET_API_ERR_WITHSAMEUSERNAME		    =79  ,//!< \~chinese 用户配置结构中存在相同的用户名 \~english User name already exist
    NET_API_ERR_DEVICETYPE_ERROR            =80  ,//!< \~chinese 导入参数时设备型号不匹配 \~english Device type error
    NET_API_ERR_LANGUAGE_ERROR              =81  ,//!< \~chinese 导入参数时语言不匹配 \~english Language not match
    NET_API_ERR_PARAVERSION_ERROR           =82  ,//!< \~chinese 导入参数时软件版本不匹配 \~english Parameter version not match
    NET_API_ERR_IPCHAN_NOTALIVE             =83  ,//!< \~chinese 预览时外接IP通道不在线 \~english IPC offline
    NET_API_ERR_RTSP_SDK_ERROR				=84	,//!< \~chinese 加载RTSP SDK 失败 \~english Load RTSP sdk failed
    NET_API_ERR_CONVERT_SDK_ERROR			=85	,//!< \~chinese 加载转码库失败 \~english Load converter failed
    NET_API_ERR_IPC_COUNT_OVERFLOW			=86  ,//!< \~chinese 超出最大的ip接入通道数 \~english IPC number exceeded
    NET_API_ERR_INVALID_USERID			    =87	,//!< \~chinese 用户ID不合法 \~english Invalid User ID 
    NET_API_ERR_INVALID_HANDLE			    =88	,//!< \~chinese 传入的句柄不合法 \~english Invalid Handle 
};

//! \~chinese 播放器 SDK 错误代码 \~english Player SDK error codes
enum enumPlayerErrorCodes{
    NET_PLAYER_ERR_NOERROR                     =500 ,//!< \~chinese 没有错误 \~english No error
    NET_PLAYER_ERR_PARAM_ERROR                 =501 ,//!< \~chinese 输入参数错误 \~english Input parameter is invalid
    NET_PLAYER_ERR_ORDER_ERROR                 =502 ,//!< \~chinese 调用顺序不正确 \~english The order of the function to be called is error.
    NET_PLAYER_ERR_TIMER_ERROR                 =503 ,//!< \~chinese 创建多媒体时钟失败 \~english Create multimedia clock failed;
    NET_PLAYER_ERR_DEC_VIDEO_ERROR             =504 ,//!< \~chinese 解码视频数据失败 \~english Decode video data failed.
    NET_PLAYER_ERR_DEC_AUDIO_ERROR             =505 ,//!< \~chinese 解码音频数据失败 \~english Decode audio data failed.
    NET_PLAYER_ERR_ALLOC_MEMORY_ERROR          =506 ,//!< \~chinese 分配内存失败 \~english Allocate memory failed.
    NET_PLAYER_ERR_OPEN_FILE_ERROR             =507 ,//!< \~chinese 打开文件失败 \~english Open the file failed.
    NET_PLAYER_ERR_CREATE_OBJ_ERROR            =508 ,//!< \~chinese 创建线程或事件失败 \~english Create thread or event failed
    NET_PLAYER_ERR_CREATE_DDRAW_ERROR          =509 ,//!< \~chinese 创建DirectDraw失败 \~english Create DirectDraw object failed.
    NET_PLAYER_ERR_CREATE_OFFSCREEN_ERROR      =510 ,//!< \~chinese 创建离屏表明失败 \~english Failed when creating off-screen surface.
    NET_PLAYER_ERR_BUFFER_OVERFLOW             =511 ,//!< \~chinese 缓冲区溢出 \~english Buffer is overflow
    NET_PLAYER_ERR_CREATE_SOUND_ERROR          =512 ,//!< \~chinese 创建音频设备失败 \~english Failed when creating audio device.    
    NET_PLAYER_ERR_SET_VOLUME_ERROR            =513 ,//!< \~chinese 设置音量失败 \~english Set volume failed
    NET_PLAYER_ERR_SUPPORT_FILE_ONLY           =514 ,//!< \~chinese 本接口只支持文件播放 \~english The function only support play file.
    NET_PLAYER_ERR_SUPPORT_STREAM_ONLY         =515 ,//!< \~chinese 本接口只支持流播放 \~english The function only support play stream.
    NET_PLAYER_ERR_SYS_NOT_SUPPORT             =516 ,//!< \~chinese 系统不支持 \~english System not support.
    NET_PLAYER_ERR_SPS_PPS_UNKNOWN			   =517 ,//!< \~chinese 没有媒体头SPS PPS信息 \~english No SPS PPS information.
    NET_PLAYER_ERR_VERSION_INCORRECT           =518 ,//!< \~chinese 解码器与码流不匹配 \~english The version of decoder and encoder is not adapted.  
    NET_PLAYER_ERR_INIT_DECODER_ERROR          =519 ,//!< \~chinese 初始化解码器失败 \~english Initialize decoder failed.
    NET_PLAYER_ERR_CHECK_FILE_ERROR            =520 ,//!< \~chinese 文件数据不合法 \~english The file data is unknown.
    NET_PLAYER_ERR_INIT_TIMER_ERROR            =521 ,//!< \~chinese 初始化多媒体时钟失败 \~english Initialize multimedia clock failed.
    NET_PLAYER_ERR_BLT_ERROR                   =522 ,//!< \~chinese 表面拷贝失败 \~english Blt failed.
    NET_PLAYER_ERR_UPDATE_ERROR                =523 ,//!< \~chinese 表明更新失败 \~english Update failed.
    NET_PLAYER_ERR_OPEN_FILE_ERROR_MULTI       =524 ,//!< \~chinese 打开符合流文件失败 \~english Open multi-stream file error
    NET_PLAYER_ERR_OPEN_FILE_ERROR_VIDEO       =525 ,//!< \~chinese 打开视频流文件失败 \~english Open video stream file
    NET_PLAYER_ERR_JPEG_COMPRESS_ERROR         =526 ,//!< \~chinese JPEG 编码失败 \~english JPEG compress error
    NET_PLAYER_ERR_EXTRACT_NOT_SUPPORT         =527 ,//!< \~chinese 文件版本不支持 \~english Don't support the version of this file.
    NET_PLAYER_ERR_EXTRACT_DATA_ERROR          =528 ,//!< \~chinese 转换视频数据失败 \~english Extract video data failed..
	NET_PLAYER_ERR_INVALID_WINDOW_HANDLE	   =529 ,//!< \~chinese 非法窗口句柄 \~english Invalid window handle
	NET_PLAYER_ERR_FPS_UNKNOWN				   =530 ,//!< \~chinese 未知帧率 \~english FPS unknown
	NET_PLAYER_ERR_INVALID_PLAYER_HANDLE	   =531 ,//!< \~chinese 非法播放器句柄 \~english Invalid player handle
	NET_PLAYER_ERR_NOT_STARTUP				   =532 ,//!< \~chinese 播放器没有初始化 \~english Start up not called
	NET_PLAYER_ERR_NO_PICTURE_AVAILABLE		   =533 //!< \~chinese 没有解码后的数据 \~english No picture available
};

//! \~english Frame types in voice communication \~chinese 对讲时的帧类型
enum enumVoiceMR_FrameType
{
    CA_I_FRAME	        =   0x65,   //!< H.264 \~english I frame \~chinese I 帧
    CA_P_FRAME			=   0x41,   //!< H.264 \~english P frame \~chinese P 帧
    CA_SPS_FRAME		=   0x67,   //!< H.264 \~english SPS information \~chinese SPS 信息
    CA_PPS_FRAME		=   0x68,   //!< H.264 \~english PPS information \~chinese PPS 信息
    CA_AUDIO_FRAME		=   0x10,   //!< H.264 \~english PPS information \~chinese PPS 信息
    CA_END_OF_FRAME		=   0x8000  //!< \~english End of frame flag, can be combined with other frame type flags \~chinese 帧结束标记, 可以与上述标记同时使用
};

//! \~english Encoder types in voice communication \~chinese 对讲时的编码类型
enum enumVoiceMR_EncoderType
{
    CA_CODEC_H264		=   0x00,    //!< H.264 \~english encoder \~chinese 编码
    CA_CODEC_G711		=   0x10     //!< G.711 \~english encoder \~chinese 编码
};

//! \~english Frame types in callback \~chinese 回调函数中的帧格式
enum enumFrameType
{
	audio_8k_16bit_mono_g711u  = 0, //!< 8k & 16bit & mono & g711u \~english format \~chinese 格式

	video_d1_pal   = 1,    //!< 720*576 D1 PAL \~english format \~chinese 格式
	video_cif_pal   = 2, //!< 352*288 CIF PAL \~english format \~chinese 格式

	video_d1_ntsc  = 101, //!< 720*480 D1 NTSC \~english format \~chinese 格式
	video_cif_ntsc  = 102, //!< 352*240 CIF NTSC \~english format \~chinese 格式
	video_vga  = 103,  //!< 720*288 2CIF PAL \~english format \~chinese 格式
	video_qvga  = 104,  //!< 720*240 2CIF NTSC \~english format \~chinese 格式

	video_2cif_pal  = 111,  //!< 720*288 2CIF PAL \~english format \~chinese 格式
	video_2cif_ntsc  = 112,  //!< 720*240 2CIF NTSC \~english format \~chinese 格式

	video_qcif_pal  = 113,  //!< 176*144 QCIF PAL \~english format \~chinese 格式
	video_qcif_ntsc  = 114, //!< 176*120 QCIF PAL \~english format \~chinese 格式

	video_720p   = 115, //!< 1280*720 720P \~english format \~chinese 格式
	video_1080i   = 116, //!< 1920*1080i 1080I \~english format \~chinese 格式
	video_1080p   = 117, //!< 1920*1080p 1080P \~english format \~chinese 格式
	
	video_2448_2048 = 118, // 2448x2048

	image_mjpeg = 150, // motion jpeg

	audio_48k_16bit_mono_g711u = 200, //!< 48k & 16bit & mono & g711u \~english format \~chinese 格式
	audio_48k_16bit_stereo_g711u = 201, //!< 48k & 16bit & stereo & g711u \~english format \~chinese 格式
	
	audio_8k_16bit_mono_adpcm_dvi4  = 202 //!< 8k & 16bit & mono & adpcm(dvi4) \~english format \~chinese 格式
};

//! \~chinese 设备视频格式 \~english Device Video Format
enum enumChannelFormat
{
	FORMAT_D1=1,
	FORMAT_720p30,
	FORMAT_1080p30,
	FORMAT_720p60,
	FORMAT_1080i50,
	FORMAT_1080i60,
	FORMAT_1080p60,
	FORMAT_1080p24,
	FORMAT_1080p25,
	FORMAT_1080p50,
	FORMAT_720p24,
	FORMAT_720p25,
	FORMAT_720p50,
	FORMAT_2M,
	FORMAT_5M,
};

//! \~english Command types ::MessageCallBack \~chinese 消息回调::MessageCallBack中的命令类型
enum enumCommandMessages
{
	COMM_ALARM                 = 0x1100,  //!< \~english Alarm information \~chinese 报警信息
	COMM_WORKSTATE             = 0x1200,  //!< \~english Device work state \~chinese 设备工作状态
	COMM_RESERVED_INFO         = 0x1600,  //!< \~english Reserved information \~chinese 保留信息
	COMM_FILE_CREATION_INFO    = 0x1700,   //!< \~english File creation information \~chinese 录像文件创建信息
	COMM_JPEG_CAPTURED_INFO    = 0x1800	 //!< \~english Capture jpeg by Alarm information \~chinese 报警抓图信息
};

//! \~english Exceptiong types in ::ExceptionCallBack \~chinese 异常回调::ExceptionCallBack中的异常类型
enum enumExceptionMessages
{
	EXCEPTION_EXCHANGE          = 0x8000,  //!< \~english SDK lost connection to device \~chinese SDK与设备之间的连接断开
	EXCEPTION_AUDIOEXCHANGE     = 0x8001,  //!< \~english Communicate exception \~chinese 语音对讲异常
	EXCEPTION_ALARM             = 0x8002,  //!< \~english Alarm exception \~chinese 报警异常
	EXCEPTION_PREVIEW           = 0x8003,  //!< \~english Preview exception \~chinese 网络预览异常
	EXCEPTION_SERIAL            = 0x8004,  //!< \~english Transplant channel exception \~chinese 透明通道异常
	EXCEPTION_RECONNECT         = 0x8005,  //!< \~english Reconnect exception \~chinese 预览时重连
	EXCEPTION_ALARMRECONNECT	= 0x8006,  //!< \~english Alarm reconnect exception \~chinese 报警时重连
	EXCEPTION_SERIALRECONNECT	= 0x8007,  //!< \~english Transplant channel reconnect exception \~chinese 透明通道重连
	EXCEPTION_SERIALRECONSUCCESS = 0x8008, //!< \~english Transplant channel reconnect succeed \~chinese 透明通道重连成功
	EXCEPTION_PLAYBACK	        = 0x8010,  //!< \~english Playback exception \~chinese 回放异常
	EXCEPTION_DISKFMT	        = 0x8011,  //!< \~english Disk format exception \~chinese 硬盘格式化
	EXCEPTION_UPDATE            = 0x10004  //!< \~english Upgrade exception \~chinese 正在升级
};

//! ::NET_API_FindFile \~english Record types \~chinese 录像检索类型
enum enumRecordTypes
{
	RECORD_BY_TIMER=0,					//!< \~english 0 - recorded by timer							\~chinese 0 - 定时触发
	RECORD_BY_MOTION_DETECT,			//!< \~english 1 - recorded by motion dectect				\~chinese 1 - 移动侦测触发
	RECORD_BY_SENSOR,					//!< \~english 2 - recorded by sensor						\~chinese 2 - 外接传感器触发
	RECORD_BY_SENSOR_OR_MOTION_DETECT,	//!< \~english 3 - recorded by sensor or motion				\~chinese 3 - 移动侦测或者外接传感器触发
	RECORD_BY_SENSOR_AND_MOTION_DETECT,	//!< \~english 4 - recorded by sensor and motion				\~chinese 4 - 移动侦测和外接传感器同时触发
	RECORD_BY_RESUMING,					//!< \~english 5 - recorded by resuming						\~chinese 5 - 续传录像
	RECORD_BY_MANUAL,					//!< \~english 6 - recorded by manual						\~chinese 6 - 手动触发
	RECORD_BY_ALL=0xff					//!< \~english 0xff - all type								\~chinese 0xff - 所有类型
};

//! ::NET_API_GetDVRConfig, ::NET_API_SetDVRConfig \~english config types \~chinese 的命令定义
enum enumConfigTypes
{
	NET_API_GET_DEVICECFG       = 100, 
	NET_API_SET_DEVICECFG       = 101, 
	NET_API_GET_NETCFG          = 102, 
	NET_API_SET_NETCFG          = 103, 
	NET_API_GET_RECORDCFG       = 108, 
	NET_API_SET_RECORDCFG       = 109, 
	NET_API_GET_DECODERCFG      = 110, 
	NET_API_SET_DECODERCFG      = 111, 
	NET_API_GET_RS232CFG        = 112, 
	NET_API_SET_RS232CFG        = 113, 
	NET_API_GET_ALARMINCFG      = 114, 
	NET_API_SET_ALARMINCFG      = 115, 
	NET_API_GET_ALARMOUTCFG     = 116, 
	NET_API_SET_ALARMOUTCFG     = 117, 
	NET_API_GET_TIMECFG         = 118, 
	NET_API_SET_TIMECFG         = 119, 
	NET_API_GET_PREVIEWCFG      = 120, 
	NET_API_SET_PREVIEWCFG      = 121, 
	NET_API_GET_VIDEOOUTCFG     = 122, 
	NET_API_SET_VIDEOOUTCFG     = 123, 
	NET_API_GET_EXCEPTIONCFG    = 126, 
	NET_API_SET_EXCEPTIONCFG    = 127, 
	NET_API_GET_SHOWSTRING      = 130, 
	NET_API_SET_SHOWSTRING      = 131, 
	NET_API_GET_EVENTCOMPCFG    = 132, 
	NET_API_SET_EVENTCOMPCFG    = 133, 
	NET_API_GET_AUXOUTCFG       = 140, 
	NET_API_SET_AUXOUTCFG       = 141, 
	NET_API_GET_PREVIEWCFG_AUX  = 142, 
	NET_API_SET_PREVIEWCFG_AUX  = 143, 
	NET_API_GET_PICCFG_EX       = 200, 
	NET_API_SET_PICCFG_EX       = 201, 
	NET_API_GET_USERCFG_EX      = 202, 
	NET_API_SET_USERCFG_EX      = 203, 
	NET_API_GET_COMPRESSCFG_EX  = 204, 
	NET_API_SET_COMPRESSCFG_EX  = 205, 
	NET_API_GET_PICCFG_EX_V20   = 206, 
	NET_API_SET_PICCFG_EX_V20   = 207, 
	NET_API_GET_PICCFG_OSD_USER = 208,
	NET_API_SET_PICCFG_OSD_USER = 209,
	NET_API_GET_NETAPPCFG       = 222, 
	NET_API_SET_NETAPPCFG       = 223, 
	NET_API_GET_NFSCFG          = 230, 
	NET_API_SET_NFSCFG          = 231,
	NET_DVR_GET_SHOWSTRING_EX   = 238,	// 获取叠加字符参数扩展
	NET_DVR_SET_SHOWSTRING_EX	= 239,	// 设置叠加字符参数扩展
	NET_API_GET_NETCFG_OTHER    = 244, 
	NET_API_SET_NETCFG_OTHER    = 245, 
	NET_API_GET_EMAILPARACFG    = 250, 
	NET_API_SET_EMAILPARACFG    = 251, 
	NET_API_GET_PTZ_CRUISE      = 276, 
	NET_API_GET_TIME_ZONE       = 277, 
	NET_API_SET_TIME_ZONE       = 278,
    NET_API_GET_VIDEO_INPUT_FORMAT   = 279, 
	NET_API_SET_VIDEO_INPUT_FORMAT   = 280,
    NET_API_GET_SPOT_CFG     = 281, 
	NET_API_SET_SPOT_CFG     = 282,
	NET_API_GET_DECODERCFG_EX   = 310, 
	NET_API_SET_DECODERCFG_EX   = 311, 
	NET_API_GET_SNAP_INFO   = 312, 
	NET_API_SET_SNAP_INFO   = 313,
	NET_API_GET_USERCFG_EX_V20      = 314, 
	NET_API_SET_USERCFG_EX_V20      = 315, 
	//---------------cls4000 add begin---------------------

	NET_API_GET_CHANNEL_VIDEO_INPUT_FORMAT =314,//输入制式
	NET_API_SET_CHANNEL_VIDEO_INPUT_FORMAT =315,

	NET_API_GET_IMAGEENHANCE_CFG = 1000,//图像增强
	NET_API_SET_IMAGEENHANCE_CFG = 1001,

	NET_API_GET_NOISEREDUCE_CFG  = 1002,//去噪
	NET_API_SET_NOISEREDUCE_CFG  = 1003,

	NET_API_GET_SHARPEN_CFG      = 1004,//锐化
	NET_API_SET_SHARPEN_CFG      = 1005,

	NET_API_GET_IMAGEINFO_CFG    = 1006, //视频图像参数
	NET_API_SET_IMAGEINFO_CFG    = 1007,

	NET_API_GET_AUDIOSWITCH_CFG  = 1008,//音频编码开关,语音对讲
	NET_API_SET_AUDIOSWITCH_CFG  = 1009,

	NET_API_RESTORE_IMAGEDEF_CFG	 = 1010, //视频图像参数恢复默认

	NET_API_SET_VIDEOOSD_CFG     = 1011,
	NET_API_GET_VIDEOOSD_TIME_CFG  =1012,
	NET_API_GET_VIDEOOSD_NAME_CFG  =1013,
	NET_API_GET_VIDEOOSD_USER_CFG  =1014,
	NET_API_GET_VIDEOOSD_PTZ_CFG   =1015,
	NET_API_GET_VIDEOOSD_ENCODE_CFG  =1016,

	NET_API_GET_DST_PARAM            = 1017,  //夏令时
	NET_API_SET_DST_PARAM = 1018,

	NET_API_GET_DDNSCFG_EX           = 1019,//DDNS
	NET_API_SET_DDNSCFG_EX           = 1020,

	NET_API_GET_LANE_CFG             = 1021, //FPGA
	NET_API_SET_LANE_CFG = 1022,
	NET_API_GET_LANE_RESULT = 1023,
	NET_API_RESET_LANE_COUNT = 1024,
	
	NET_API_GET_AUTOSENDRTP_CFG      = 1025,  //auto send rtp
	NET_API_SET_AUTOSENDRTP_CFG      = 1026,

	NET_API_GET_PTZ_DEV_CFG          = 1027,  //ptz dev cfg
	NET_API_SET_PTZ_DEV_CFG          = 1028,

	NET_API_GET_AUDIO_INFO_CFG       = 1029,  //audio config
	NET_API_SET_AUDIO_INFO_CFG       = 1030,

	NET_API_GET_RTMP_CFG			 = 1031,
	NET_API_SET_RTMP_CFG			 = 1032,

	NET_API_GET_PORT_MAP_CFG			 = 1033,
	NET_API_SET_PORT_MAP_CFG			 = 1034,

	NET_API_GET_AUTOSENDTCP_CFG      = 1035,  //auto send tcp
	NET_API_SET_AUTOSENDTCP_CFG      = 1036,

	//---------------cls4000 add end---------------------

	//---------------cls440x decoder add begin---------------------
	NET_API_GET_DECDEV_DEC_CFG		= 1051,
	NET_API_SET_DECDEV_DEC_CFG		= 1052,
	NET_API_GET_DECDEV_STATUS		= 1053,

	NET_API_GET_DECDEV_DISPLAY_CFG	= 1054,
	NET_API_SET_DECDEV_DISPLAY_CFG	= 1055,

	NET_API_GET_DECDEV_TRANS_CFG	= 1056,
	NET_API_SET_DECDEV_TRANS_CFG	= 1057,

	NET_API_GET_DECDEV_AUDIO_CFG	= 1058,
	NET_API_SET_DECDEV_AUDIO_CFG	= 1059,

	NET_API_GET_CHANNEL_VIDEO_OUTPUT_FORMAT =1060,//输出制式
	NET_API_SET_CHANNEL_VIDEO_OUTPUT_FORMAT =1061,
	//---------------cls440x decoder add end---------------------

	NET_API_GET_DEBUG_LEVEL          = 2000,//打印级别
	NET_API_SET_DEBUG_LEVEL          = 2001,
};

//! \~english Playback control commands \~chinese 播放控制命令宏定义 \~ \see ::NET_API_PlayBackControl
enum enumPlaybackControlCommands
{
	NET_API_PLAYSTART          = 1,
	NET_API_PLAYSTOP           = 2,
	NET_API_PLAYPAUSE          = 3,
	NET_API_PLAYRESTART        = 4,
	NET_API_PLAYFAST           = 5,
	NET_API_PLAYSLOW           = 6,
	NET_API_PLAYNORMAL         = 7,
	NET_API_PLAYFRAME          = 8,
	NET_API_PLAYSTARTAUDIO     = 9,
	NET_API_PLAYSTOPAUDIO      = 10,
	NET_API_PLAYAUDIOVOLUME    = 11,
	NET_API_PLAYSETPOS         = 12,
	NET_API_PLAYGETPOS         = 13,
	NET_API_PLAYGETTIME        = 14,
	NET_API_PLAYGETFRAME       = 15,
	NET_API_GETTOTALFRAMES     = 16,
	NET_API_GETTOTALTIME       = 17,
	NET_API_PLAYGETSPEED       = 18,
	NET_API_THROWBFRAME        = 20,
	NET_API_LASTIFRAME         = 21,
	NET_API_NEXTIFRAME         = 22,
	NET_API_GET_AUDIO_STATUS   = 23,
	NET_API_PLAYFAST_BACK      = 24,
	NET_API_PLAYSLOW_BACK      = 25,
	NET_API_PLAY_REVERSE       = 26,
	
	NET_API_PLAYSETTIME		   = 30,
	NET_API_DOWNLOAD		   = 40
};

//! \~english Return values for finding files or logs \~chinese 查找文件和日志函数返回值 \~ \see NET_API_FindNextLog, NET_API_FindNextFile, NET_API_FindNextJpeg
enum enumReturnValuesForFinding	
{
	NET_API_FILE_SUCCESS       = 1000, 
	NET_API_FILE_NOFIND        = 1001, 
	NET_API_ISFINDING          = 1002, 
	NET_API_NOMOREFILE         = 1003, 
	NET_API_FILE_EXCEPTION     = 1004  
};

//! \~english Log major types \~chinese 日志主类型
enum enumLogMajorTypes	
{
	MAJOR_ALARM            = 0x1,	/* 报警 */
	MAJOR_EXCEPTION        = 0x2,	/* 异常 */
	MAJOR_OPERATION        = 0x3	/* 操作 */
};

//! \~english Minor log types for alarm \~chinese 报警日志次类型
enum enumLogAlarmMinorTypes	
{
	MINOR_ALARM_IN         = 0x1,     /* 报警输入 */
	MINOR_ALARM_OUT        = 0x2,     /* 报警输出 */
	MINOR_MOTDET_START     = 0x3,     /* 移动侦测报警开始 */
	MINOR_MOTDET_STOP      = 0x4,     /* 移动侦测报警结束 */
	MINOR_HIDE_ALARM_START = 0x5,     /* 遮挡报警开始 */
	MINOR_HIDE_ALARM_STOP  = 0x6      /* 遮挡报警结束 */
};

//! \~english Minor log types for exceptions \~chinese 异常日志次类型
enum enumLogExceptionMinorTypes
{
	MINOR_VI_LOST           = 0x21,    /* 信号丢失 */
	MINOR_ILLEGAL_ACCESS    = 0x22,    /* 非法访问 */
	MINOR_HD_FULL           = 0x23,    /* 硬盘满 */
	MINOR_HD_ERROR          = 0x24,    /* 硬盘错误 */
	MINOR_DCD_LOST          = 0x25,    /* MODEM 掉线 */
	MINOR_IP_CONFLICT       = 0x26,    /* IP地址冲突 */
	MINOR_NET_BROKEN        = 0x27,	/* 网线断 */
	MINOR_MAIL_ERR			= 0x28,	/* 邮件发送失败 */
	MINOR_VIDEO_UNMATCH		= 0x29,	/* 视频制式不匹配 */
	MINOR_VIDEO_EXCEPTION	= 0x2A,	/* 编码器异常 */
	MINOR_HIGH_TEMPERATURE	= 0x2B,	/* 温度异常 */
	MINOR_NO_FAN_SPEED		= 0x2C,	/* 风扇异常 */

	MINOR_VI_LOST_RESUME    = 0x61, /*视频丢失恢复*/
	MINOR_SYSTEM_CALL_ERROR	= 0xA0	/* system() return -1 */
};

//! \~english Minor log types for operations \~chinese 操作日志次类型
enum enumLogOperationMinorTypes
{
	 MINOR_START_DVR         = 0x41,    /* 开机 */
	 MINOR_STOP_DVR          = 0x42,    /* 关机 */
	 MINOR_STOP_ABNORMAL     = 0x43,    /* 非法关机 */
	 MINOR_LOCAL_LOGIN       = 0x50,    /* 本地登陆 */
	 MINOR_LOCAL_LOGOUT      = 0x51,    /* 本地注销登陆 */
	 MINOR_LOCAL_CFG_PARM    = 0x52,    /* 本地配置参数 */
	 MINOR_LOCAL_PLAYBYFILE  = 0x53,    /* 本地按文件回放 */
	 MINOR_LOCAL_PLAYBYTIME  = 0x54,    /* 本地按时间回放 */
	 MINOR_LOCAL_START_REC   = 0x55,    /* 本地开始录像 */
	 MINOR_LOCAL_STOP_REC    = 0x56,    /* 本地停止录像 */
	 MINOR_LOCAL_PTZCTRL     = 0x57,    /* 本地云台控制 */
	 MINOR_LOCAL_PREVIEW     = 0x58,    /* 本地预览 */
	 MINOR_LOCAL_MODIFY_TIME = 0x59,    /* 本地修改时间 */
	 MINOR_LOCAL_UPGRADE     = 0x5a,    /* 本地升级 */
	 MINOR_LOCAL_COPYFILE    = 0x5b,    /* 本地备份文件 */
	 MINOR_LOCAL_FORMAT_DISK = 0x5c,    /* 本地格式化硬盘 */
	 MINOR_REMOTE_LOGIN      = 0x70,    /* 远程登录 */
	 MINOR_REMOTE_LOGOUT     = 0x71,    /* 远程注销登陆 */
	 MINOR_REMOTE_START_REC  = 0x72,    /* 远程开始录像 */
	 MINOR_REMOTE_STOP_REC   = 0x73,    /* 远程停止录像 */
	 MINOR_START_TRANS_CHAN  = 0x74,    /* 开始透明传输 */
	 MINOR_STOP_TRANS_CHAN   = 0x75,    /* 停止透明传输 */
	 MINOR_REMOTE_GET_PARM   = 0x76,    /* 远程获得参数 */
	 MINOR_REMOTE_CFG_PARM   = 0x77,    /* 远程配置参数 */
	 MINOR_REMOTE_GET_STATUS = 0x78,    /* 远程获得状态 */
	 MINOR_REMOTE_ARM        = 0x79,    /* 远程布防 */
	 MINOR_REMOTE_DISARM     = 0x7a,    /* 远程撤防 */
	 MINOR_REMOTE_REBOOT     = 0x7b,    /* 远程重启 */
	 MINOR_START_VT          = 0x7c,    /* 开始语音对讲 */
	 MINOR_STOP_VT           = 0x7d,    /* 停止语音对讲 */
	 MINOR_REMOTE_UPGRADE    = 0x7e,    /* 远程升级 */
	 MINOR_REMOTE_PLAYBYFILE = 0x7f,    /* 远程按文件回放 */
	 MINOR_REMOTE_PLAYBYTIME = 0x80,    /* 远程按时间回放 */
	 MINOR_REMOTE_PTZCTRL    = 0x81,    /* 远程云台控制 */
	 MINOR_REMOTE_FORMAT_DISK = 0x82    /* 远程格式化硬盘 */
};
	
//! \~english Types for configuration operation \~chinese 当日志的主类型为MAJOR_OPERATION=03, 次类型为MINOR_LOCAL_CFG_PARM=0x52或者MINOR_REMOTE_GET_PARM=0x76或者MINOR_REMOTE_CFG_PARM=0x77时, dwParaType:参数类型有效, 其含义如下：
enum enumLogOperationConfigTypes
{
	PARA_VIDEOOUT               = 0x1,
	PARA_IMAGE                  = 0x2,
	PARA_ENCODE                 = 0x3,
	PARA_NETWORK                = 0x4,
	PARA_ALARM                  = 0x5,
	PARA_EXCEPTION              = 0x6,
	PARA_DECODER                = 0x7,   /*解码器*/
	PARA_RS232                  = 0x8,
	PARA_PREVIEW                = 0x9,
	PARA_SECURITY               = 0xa,
	PARA_DATETIME               = 0xb,
	PARA_FRAMETYPE              = 0xc    /*帧格式*/
};

//! \~english PTZ commands \~chinese 云台操作命令 \~ \see NET_API_PTZControl, NET_API_PTZControlWithSpeed, NET_API_PTZControl_Ex, NET_API_PTZPreset, NET_API_PTZGuardPoint, NET_API_PTZCruise, NET_API_PTZTrack
enum enumPTZCommands
{
	PTZ_SET_GUARD_POINT		= 100,	// 设置看守位
	PTZ_GOTO_GUARD_POINT	= 101,	// 转到看守位
	PTZ_SET_PRESET          = 8,   // 设置预置点
	PTZ_CLE_PRESET          = 9,   // 清除预置点
	PTZ_GOTO_PRESET         = 39,  // 转到预置点
	PTZ_LIGHT_PWRON         = 2,   /* 接通灯光电源 */
	PTZ_WIPER_PWRON         = 3,   /* 接通雨刷开关 */
	PTZ_FAN_PWRON           = 4,   /* 接通风扇开关 */
	PTZ_HEATER_PWRON        = 5,   /* 接通加热器开关 */
	PTZ_AUX_PWRON           = 6,   /* 接通辅助设备开关 */
	PTZ_ZOOM_IN             = 11,  /* 焦距以速度SS变大(倍率变大) */
	PTZ_ZOOM_OUT            = 12,  /* 焦距以速度SS变小(倍率变小) */
	PTZ_FOCUS_NEAR          = 13,  /* 焦点以速度SS前调 */
	PTZ_FOCUS_FAR           = 14,  /* 焦点以速度SS后调 */
	PTZ_IRIS_OPEN           = 15,  /* 光圈以速度SS扩大 */
	PTZ_IRIS_CLOSE          = 16,  /* 光圈以速度SS缩小 */
	PTZ_TILT_UP             = 21,  /* 云台以SS的速度上仰 */
	PTZ_TILT_DOWN           = 22,  /* 云台以SS的速度下俯 */
	PTZ_PAN_LEFT            = 23,  /* 云台以SS的速度左转 */
	PTZ_PAN_RIGHT           = 24,  /* 云台以SS的速度右转 */
	PTZ_UP_LEFT             = 25,  /* 云台以SS的速度上仰和左转 */
	PTZ_UP_RIGHT            = 26,  /* 云台以SS的速度上仰和右转 */
	PTZ_DOWN_LEFT           = 27,  /* 云台以SS的速度下俯和左转 */
	PTZ_DOWN_RIGHT          = 28,  /* 云台以SS的速度下俯和右转 */
	PTZ_PAN_AUTO            = 29,  /* 云台以SS的速度左右自动扫描 */
	PTZ_FILL_PRE_SEQ        = 30,  /* 将预置点加入巡航序列 */
	PTZ_SET_SEQ_DWELL       = 31,  /* 设置巡航点停顿时间 */
	PTZ_SET_SEQ_SPEED       = 32,  /* 设置巡航速度 */
	PTZ_CLE_PRE_SEQ         = 33,  /* 将预置点从巡航序列中删除 */
	PTZ_STA_MEM_CRUISE      = 34,  /* 开始记录轨迹 */
	PTZ_STO_MEM_CRUISE      = 35,  /* 停止记录轨迹 */
	PTZ_RUN_CRUISE          = 36,  /* 开始轨迹 */
	PTZ_RUN_SEQ             = 37,  /* 开始巡航 */
	PTZ_STOP_SEQ            = 38  /* 停止巡航 */
};

//! \~english Hasrdware type list \~chinese 支持的硬件设备列表
enum enumHardwareList
{
	HARDWARE_ZION_SD			= 20,
	HARDWARE_ZION_HD			= 21,
	HARDWARE_ZION_DEC			= 22,
	HARDWARE_CUSTOM_SD			= 30,
	HARDWARE_CUSTOM_HD			= 31,
	HARDWARE_UNICORN_ENC		= 40,
	HARDWARE_B1_ENC				= 50,
	HARDWARE_B1_DEC				= 60,
};

//! \~english Reseverd \~chinese 保留
enum enumReserved
{
	WORKSTATE_EXT_TYPE_1		=	1,
	WORKSTATE_EXT_TYPE_2		=	2,
	WORKSTATE_EXT_TYPE_3		=	4,
	WORKSTATE_EXT_TYPE_4		=	8
};

#pragma pack(push, 1)
    //! \~english Device information \~chinese 设备信息
	typedef struct  {
		BYTE sSerialNumber[SERIALNO_LEN];  //!< \~english Serial number \~chinese 序列号
		BYTE byAlarmInPortNum;      //!< \~english Number of alarm in port \~chinese 报警输入个数
		BYTE byAlarmOutPortNum;     //!< \~english Number of alarm out port \~chinese 报警输出个数
		BYTE byDiskNum;             //!< \~english Number of hard driver \~chinese 硬盘个数
		BYTE byDVRType;             //!< \~english Device type \~chinese 类型 \~ \see ::enumChannelFormat
		BYTE byChanNum;             //!< \~english Number of channel \~chinese 通道个数
		BYTE byStartChan;           //!< \~chinese 起始通道号,例如 \~english Start number of channel, like \~ DVS-1,DVR-1
	}NET_API_DEVICEINFO, *LPNET_API_DEVICEINFO;

	//! \~english Device parameters \~chinese 设备参数
	typedef struct{
		DWORD dwSize;                //!< \~english Size of this struct \~chinese 结构体大小
		BYTE sDVRName[NAME_LEN];     //!< \~english Name of device \~chinese 设备名称
		DWORD dwDVRID;              //!< \~english Device ID of IR remote controler \~chinese 设备 ID,用于遥控器 \~ (1-255)
		DWORD dwRecycleRecord;      //!< \~english Overwrite recording, 0 - N0, 1 - Yes \~chinese 是否循环录像,0:不是; 1:是
		DWORD dwRecFileTime;      //!< \~english Time limit for one record file (seconds) \~chinese 录像时长(秒)
		DWORD dwRecFileSize;      //!< \~english File size limit for one record file (bytes) \~chinese 录像大小
		DWORD dwPalNtsc;       //!< \~english  system type is PAL(0) or NTSC(1) \~chinese 系统制式选择0-PAL, 1-NTSC
		DWORD dwLanguage;       //!< \~english system language english(1) chinese(2) traditional chinese(3)  japness(4)   no use(0)\~chinese 保留, 1-英文 2-中文 3-繁体中文  4-日文   0-不使用这个字段
		DWORD dwAudioID;          //!<\~english Audio id of input, 0 - Not Support, 1 - SDI audio, 2 - USB audio \~chinese 输入的音频源 0 - 不支持，1 - SDI音频， 2 - USB音频
		
		BYTE sSerialNumber[SERIALNO_LEN];  //!< \~english Serial number \~chinese 序列号
		DWORD dwSoftwareVersion;            //!< \~english Software version \~chinese 软件版本号,高16位是主版本,低16位是次版本
		DWORD dwSoftwareBuildDate;          //!< \~english Software build date \~chinese 软件生成日期,0xYYYYMMDD
		DWORD dwDSPSoftwareVersion;         //!< \~english DSP version \~chinese DSP软件版本,高16位是主版本,低16位是次版本
		DWORD dwDSPSoftwareBuildDate;       //!< \~english DSP build date \~chinese DSP软件生成日期,0xYYYYMMDD
		DWORD dwPanelVersion;               //!< \~english Panel firmware version \~chinese 前面板版本,高16位是主版本,低16位是次版本
		DWORD dwHardwareVersion;    //!< \~english Hardware version,use enumHardwareList \~chinese 硬件版本,高16位是主版本,enumHardwareList类型,低16位是次版本
		BYTE byAlarmInPortNum;      //!< \~english Number of alarm in port \~chinese 设备报警输入个数
		BYTE byAlarmOutPortNum;     //!< \~english Number of alarm out port \~chinese 设备报警输出个数
		BYTE byRS232Num;            //!< \~english Number of RS232 \~chinese 设备 232串口个数
		BYTE byRS485Num;            //!< \~english Number of RS485 \~chinese 设备 485串口个数
		BYTE byNetworkPortNum;      //!< \~english Number of ethernet port \~chinese 网络口个数
		BYTE byDiskCtrlNum;         //!< \~english Number of hard driver controller \~chinese 设备 硬盘控制器个数
		BYTE byDiskNum;             //!< \~english Number of hard driver \~chinese 设备 硬盘个数
		BYTE byDVRType;             //!< \~english Device type \~chinese 类型 \~ \see ::enumChannelFormat
		BYTE byChanNum;             //!< \~english Number of channel \~chinese 通道个数
		BYTE byStartChan;           //!< \~chinese 起始通道号,例如 \~english Start number of channel, like \~ DVS-1,DVR-1
		BYTE byDecordChans;         //!< \~english Number of decode channel \~chinese 设备 解码路数
		BYTE byVGANum;              //!< \~english Number of VGA port \~chinese VGA口的个数
		BYTE byUSBNum;              //!< \~english Number of USB port \~chinese USB口的个数
		char reservedData[3];       //!< \~english Reserved \~chinese 保留
	}NET_API_DEVICECFG,*LPNET_API_DEVICECFG;

    //! \~english Ethernet config structure \~chinese 网卡设置
	typedef struct {
		char sDVRIP[16];            //!< \~english Deviece IP address \~chinese 设备 IP地址
		char sDVRIPMask[16];        //!< \~english Device netmask \~chinese 设备 IP地址掩码
		DWORD dwNetInterface;       //!< \~english Interface type: 0-Auto, 1-10M, 2-100M, 3-1000M \~chinese 网络接口 0-自动协商, 1-10M,  2-100M, 3-1000M
		BYTE byMACAddr[MACADDR_LEN];        //!< \~english Mac address \~chinese 服务器的物理地址
		char sMultiCastIP[16];      //!< \~english Multicast address \~chinese 多播组地址
		char sGatewayIP[16];        //!< \~english Gateway ip address \~chinese 网关地址
		DWORD dwConfigMode;         //!< \~english Config mode: 0=Fixed ip, 1=DHCP \~chinese 网络配置模式 0=固定IP地址,1=动态获取IP
		DWORD dwPPPOE;              //!< \~english Enable PPPoE, 0-Disable, 1-Enable \~chinese 是否启用PPPoE, 0-不启用,1-启用
		BYTE sPPPoEUser[NAME_LEN];  //!< \~english PPPoE username \~chinese PPPoE用户名
		char sPPPoEPassword[PASSWD_LEN];//!< \~english PPPoE password \~chinese PPPoE密码
		char sPPPoEIP[16];          //!< \~english PPPoE ip address (read only) \~chinese PPPoE IP地址(只读)
	}NET_API_ETHERNET;

	//! \~english Network config structure \~chinese 网络配置结构
	typedef struct{
		DWORD dwSize;               //!< \~english Size of structure \~chinese 结构体大小
		NET_API_ETHERNET struEtherNet[MAX_ETHERNET];        //!< \~english Ethernet config \~chinese 网卡配置
		char sManageHostIP[16];     //!< \~english Remote host ip address \~chinese 远程管理主机地址
		WORD wManageHostPort;       //!< \~english Remote host port \~chinese 远程管理主机端口号
		char sIPServerIP[16];       //!< \~english DNS ip address \~chinese DNS服务器地址
		char sNFSIP[16];            //!< \~english NFS ip address \~chinese NAS主机IP地址
		BYTE sNFSDirectory[PATHNAME_LEN];//!< \~english NFS directory \~chinese NAS目录
		WORD wHttpPort;             //!< \~english HTTP server port \~chinese HTTP服务端口号
		WORD wDVRPort;              //!< \~english Device server port \~chinese 设备服务端口号
	}NET_API_NETCFG,*LPNET_API_NETCFG;

	//! \~english Exception handle structure \~chinese 异常处理方式结构
	typedef struct
	{
        /*!
            \~ \li 0x00: \~english Don't handle \~chinese 无响应
            \~ \li 0x01: \~english Display on monitor \~chinese 监视器上警告
            \~ \li 0x02: \~english Beep \~chinese 声音警告
            \~ \li 0x04: \~english Report to manage host \~chinese 上传中心
            \~ \li 0x08: \~english Triger alarm out \~chinese 触发报警输出
            \~ \li 0x10: \~english Send mail with captured jpeg \~chinese 触发抓图并发邮件
            \~ \li 0x20: \~english Capture Jpeg \~chinese 触发抓图
            \~ \li 0x40: \~english Disable hide area \~chinese 去掉遮盖
        */
		DWORD   dwHandleType;//!< \~english Handle type, can be combined \~chinese 处理方式, 可以合并使用
		BYTE byRelAlarmOut[MAX_ALARMOUT];  //!< \~english Alarm out channel, set to 1 to enable \~chinese 报警触发的输出通道,报警触发的输出,为1表示触发该输出
	}NET_API_HANDLEEXCEPTION;

    //! \~english Date and time structure \~chinese 日期与时间结构
	typedef struct{
		DWORD dwYear;       //!< \~english Year \~chinese 年
		DWORD dwMonth;      //!< \~english Month \~chinese 月
		DWORD dwDay;        //!< \~english Day \~chinese 日
		DWORD dwHour;       //!< \~english Hour \~chinese 时
		DWORD dwMinute;     //!< \~english Minute \~chinese 分
		DWORD dwSecond;     //!< \~english Second \~chinese 秒
	}NET_API_TIME,*LPNET_API_TIME;

    //! \~english Time and time zone structure \~chinese 时间与时区结构
	typedef struct{
		NET_API_TIME strTime;       //!< \~english Date and time\~chinese 日期与时间
		BYTE byTimeZone;            //!< \~english Time zone, from 0-24, 12 means UTC+0 \~chinese 时区,0-24, 12 表示 UTC+0
	}NET_API_TIME_ZONE,*LPNET_API_TIME_ZONE;

    //! \~english Schedule time span \~chinese 时间段
	typedef struct{
		BYTE byStartHour;		//!< \~english Start hour \~chinese 开始小时
		BYTE byStartMin;        //!< \~english Start minute \~chinese 开始分钟
		BYTE byStopHour;        //!< \~english Stop hour \~chinese 结束小时
		BYTE byStopMin;         //!< \~english Stop minute \~chinese 结束分钟
	}NET_API_SCHEDTIME,*LPNET_API_SCHEDTIME;

	//! \~english Motion detect structure \~chinese 移动侦测结构
	typedef struct{
		BYTE byMotionScope[18][22]; //!< \~english Motion detect area, 18x22 macro block( 12x16 is available by now ), set to 1 to enable that block \~chinese 侦测区域,共有18x22个小宏块(目前仅12x16个有效),为1表示该宏块是移动侦测区域,0-表示不是
		BYTE byMotionSensitive;     //!< \~english Sensitive of motion detect, 0-5, 0 means most sensitive, 0xFF to disable \~chinese 移动侦测灵敏度, 0 - 5,越低越灵敏,0xff关闭
		BYTE byEnableHandleMotion;  //!< \~english Handle motion detect events \~chinese 是否处理移动侦测
		BYTE byShowGrid;  	        //!< \~english Display motion detect grid \~chinese 是否显示网格
		BYTE byShowMotion;          //!< \~english Show detection \~chinese 是否显示移动侦测
		NET_API_HANDLEEXCEPTION strMotionHandleType;    //!< \~english Handle type \~chinese 处理方式
		NET_API_SCHEDTIME struAlarmTime[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Handle time schedule \~chinese 布防时间
		BYTE byRelRecordChan[MAX_CHANNUM]; //!< \~english Triger record on which channel, set to 1 to enable \~chinese 报警触发的录象通道,为1表示触发该通道
	}NET_API_MOTION,*LPNET_API_MOTION;

	//! \~english Blind detect structure \~chinese 遮挡报警结构
	typedef struct{
		DWORD dwEnableHideAlarm;            //!< \~english Enable blind detect, 0-disable, 1-Low sensitive, 2-High sensitive \~chinese 是否启动遮挡报警 ,0-关闭,1-低灵敏度 2-高灵敏度
		WORD wHideAlarmAreaTopLeftX;        //!< \~english X position of blind detect area \~chinese 遮挡报警区域的x坐标
		WORD wHideAlarmAreaTopLeftY;        //!< \~english Y position of blind detect area\~chinese 遮挡报警区域的y坐标
		WORD wHideAlarmAreaWidth;           //!< \~english width of blind detect area\~chinese 遮挡报警区域的宽
		WORD wHideAlarmAreaHeight;          //!< \~english height of blind detect area\~chinese 遮挡报警区域的高
		NET_API_HANDLEEXCEPTION strHideAlarmHandleType; //!< \~english Handle type \~chinese 处理方式
		NET_API_SCHEDTIME struAlarmTime[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Handle time schedule \~chinese 布防时间
	}NET_API_HIDEALARM,*LPNET_API_HIDEALARM;

	//! \~english Video lost detect structure \~chinese 信号丢失报警结构
	typedef struct{
		BYTE byEnableHandleVILost;  //!< \~english Enable handle of video lost \~chinese 是否处理信号丢失报警
		NET_API_HANDLEEXCEPTION strVILostHandleType;    //!< \~english Handle type \~chinese 处理方式
		NET_API_SCHEDTIME struAlarmTime[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Handle time schedule \~chinese 布防时间
	}NET_API_VILOST,*LPNET_API_VILOST;

    //! \~english Hide area settings \~chinese 遮挡区域设置
	typedef struct{
		WORD wHideAreaTopLeftX;             //!< \~english X position of hide area \~chinese 遮挡区域的x坐标
		WORD wHideAreaTopLeftY;             //!< \~english Y position of hide area \~chinese 遮挡区域的y坐标
		WORD wHideAreaWidth;                //!< \~english Width of hide area \~chinese 遮挡区域的宽
		WORD wHideAreaHeight;               //!< \~english Height of hide area\~chinese 遮挡区域的高
	}NET_API_SHELTER,*LPNET_API_SHELTER;

	//! \~english Picture parameters structure \~chinese 图像参数结构
	typedef struct
	{
		DWORD dwSize;           //!< \~english Size of the structure \~chinese 结构体大小
		BYTE sChanName[NAME_LEN]; //!< \~english Channel name \~chinese 通道名称
		DWORD dwVideoFormat;    //!< \~english Video type(read only) \~chinese 只读：视频制式 \~ 1-NTSC 2-PAL
		BYTE byBrightness;      //!< \~english Brightness, \~chinese 亮度, \~ 0-255
		BYTE byContrast;        //!< \~english Contrast, \~chinese 对比度, \~ 0-255
		BYTE bySaturation;      //!< \~english Saturation, \~chinese 饱和度, \~ 0-255
		BYTE byHue;             //!< \~english Hue, \~chinese 色调, \~ 0-255
		DWORD dwShowChanName;   //!< \~english Show channel name on live video, 1-Enable, 0-Disable \~chinese 预览的图象上是否显示通道名称 0-不显示,1-显示
		WORD wShowNameTopLeftX;             //!< \~english X position of channel name \~chinese 通道名称显示位置的x坐标
		WORD wShowNameTopLeftY;             //!< \~english Y position of channel name \~chinese 通道名称显示位置的y坐标
		
        //! \~english Video lost alarm \~chinese 信号丢失报警
		NET_API_VILOST struVILost;
		//! \~english Motion detect alarm \~chinese 移动侦测
		NET_API_MOTION struMotion;
		//! \~english Blind detect alarm \~chinese 遮挡报警
		NET_API_HIDEALARM struHideAlarm;
		
		DWORD dwEnableHide;                 //!< \~english Enable hide area, 0-disable, 1-enable \~chinese 是否启动遮挡 ,0-否,1-是
        NET_API_SHELTER struShelter[MAX_SHELTERNUM];//!< \~english Hide area settings \~chinese 遮挡区域设置
		DWORD dwShowOsd;                //!< \~english Show OSD on live video, 0-disable, 1-enable \~chinese 预览的图象上是否显示OSD, 0-不显示,1-显示
		WORD wOSDTopLeftX;              //!< \~english X position of OSD \~chinese OSD的x坐标
		WORD wOSDTopLeftY;              //!< \~english Y position of OSD \~chinese OSD的y坐标
		/*! \~ \li 0: \~english YYYY-MM-DD \~chinese XXXX-XX-XX 年月日
            \~ \li 1: \~english MM-DD-YYYY \~chinese XX-XX-XXXX 月日年
            \~ \li 2: \~english YYYY-MM-DD \~chinese XXXX年XX月XX日 */
		BYTE byOSDType;                 //!< \~english OSD date format \~chinese OSD类型(主要是年月日格式)
		BYTE byDispWeek;                //!< \~english Show weekday or not \~chinese 是否显示星期
		BYTE byOSDAttrib;               //!< \~english OSD blinking(not used by now) \~chinese OSD属性:透明, 闪烁(暂时无效)
	}NET_API_PICCFG_EX,*LPNET_API_PICCFG_EX;

	//! \~english Picture parameters structure V2.0 \~chinese 图像参数结构 V2.0
	typedef struct
	{
		DWORD dwSize;           //!< \~english Size of the structure \~chinese 结构体大小
		BYTE sChanName[NAME_LEN_V20]; //!< \~english Channel name \~chinese 通道名称
		DWORD dwVideoFormat;    //!< \~english Video type(read only) \~chinese 只读：视频制式 \~ 1-NTSC 2-PAL
		BYTE byBrightness;      //!< \~english Brightness, \~chinese 亮度, \~ 0-255
		BYTE byContrast;        //!< \~english Contrast, \~chinese 对比度, \~ 0-255
		BYTE bySaturation;      //!< \~english Saturation, \~chinese 饱和度, \~ 0-255
		BYTE byHue;             //!< \~english Hue, \~chinese 色调, \~ 0-255
		DWORD dwShowChanName;   //!< \~english Show channel name on live video, 1-Enable, 0-Disable \~chinese 预览的图象上是否显示通道名称 0-不显示,1-显示 
		WORD wShowNameTopLeftX;             //!< \~english X position of channel name \~chinese 通道名称显示位置的x坐标
		WORD wShowNameTopLeftY;             //!< \~english Y position of channel name \~chinese 通道名称显示位置的y坐标
		
        //! \~english Video lost alarm \~chinese 信号丢失报警
		NET_API_VILOST struVILost;
		//! \~english Motion detect alarm \~chinese 移动侦测
		NET_API_MOTION struMotion;
		//! \~english Blind detect alarm \~chinese 遮挡报警
		NET_API_HIDEALARM struHideAlarm;
		
		DWORD dwEnableHide;                 //!< \~english Enable hide area, 0-disable, 1-enable \~chinese 是否启动遮挡 ,0-否,1-是
        NET_API_SHELTER struShelter[MAX_SHELTERNUM];//!< \~english Hide area settings \~chinese 遮挡区域设置
		DWORD dwShowOsd;                //!< \~english Show OSD on live video, 0-disable, 1-enable \~chinese 预览的图象上是否显示OSD,0-不显示,1-显示
		WORD wOSDTopLeftX;              //!< \~english X position of OSD \~chinese OSD的x坐标
		WORD wOSDTopLeftY;              //!< \~english Y position of OSD \~chinese OSD的y坐标
		/*! \~ \li 0: \~english YYYY-MM-DD \~chinese XXXX-XX-XX 年月日
            \~ \li 1: \~english MM-DD-YYYY \~chinese XX-XX-XXXX 月日年
            \~ \li 2: \~english YYYY-MM-DD \~chinese XXXX年XX月XX日 */
		BYTE byOSDType;                 //!< \~english OSD date format \~chinese OSD类型(主要是年月日格式)
		BYTE byDispWeek;                //!< \~english Show weekday or not \~chinese 是否显示星期
		BYTE byOSDAttrib;               //!< \~english OSD blinking(not used by now) \~chinese OSD属性:透明, 闪烁(暂时无效)
//		BYTE bySnapmode;            //!< \~english snap mode, \~chinese 抓图模式, \~ 0-2 不支持，自动，手动
	}NET_API_PICCFG_EX_V20,*LPNET_API_PICCFG_EX_V20;

	typedef struct
	{
		DWORD dwSize;           //!< \~english Size of the structure \~chinese 结构体大小
		BYTE sOsdUser[MAX_USER_OSD][NAME_LEN_V20]; //!< \~english Channel name \~chinese 通道名称
		DWORD dwShowOsdUser[MAX_USER_OSD];   //!< \~english Show channel name on live video, 1-Enable, 2-Disable \~chinese 预览的图象上是否显示通道名称 
		WORD wOSDUserTopLeftX[MAX_USER_OSD];              //!< \~english X position of OSD \~chinese OSD的x坐标
		WORD wOSDUserTopLeftY[MAX_USER_OSD];              //!< \~english Y position of OSD \~chinese OSD的y坐标
	}NET_API_PICCFG_USER_OSD,*LPNET_API_PICCFG_USER_OSD;

	typedef struct 
    {
        WORD wShowString;				// 预览的图象上是否显示字符,0-不显示,1-显示 区域大小704*576,单个字符的大小为32*32
        WORD wStringSize;				/* 该行字符的长度，不能大于44个字符 */
        WORD wShowStringTopLeftX;		/* 字符显示位置的x坐标 */
        WORD wShowStringTopLeftY;		/* 字符名称显示位置的y坐标 */
        char sString[44];				/* 要显示的字符内容 */
    }NET_API_SHOWSTRINGINFO, *LPNET_API_SHOWSTRINGINFO;

    //叠加字符扩展(8条字符)
    typedef struct 
    {
        DWORD dwSize;
        NET_API_SHOWSTRINGINFO struStringInfo[MAX_STRINGNUM_EX];				/* 要显示的字符内容 */
    }NET_API_SHOWSTRING_EX, *LPNET_API_SHOWSTRING_EX;

    //! \~english Extended encoder parameters structure \~chinese 扩展编码参数结构
	typedef struct{
		BYTE byStreamType;      //!< \~english Stream type: 0-Video, 1-Composite \~chinese 码流类型 0-视频流, 1-复合流
		BYTE byResolution;      //!< \~english Resolution \~chinese 分辨率 \~ ::enumChannelFormat
		BYTE byBitrateType;     //!< \~english Bitrate type: 0-VBR, 1-CBR \~chinese 码率类型 0:变码率, 1:定码率
		BYTE  byPicQuality;     //!< \~english Picture quality (1-100)% \~chinese 图象质量 (1-100)%
		DWORD dwVideoBitrate;   //!< \~english Video bitrate (kbps) \~chinese 视频码率 (kbps)
		DWORD dwVideoFrameRate; //!< \~english Frame rate (1-30 fps) \~chinese 帧率 (1-30 帧/秒)
		WORD  wIntervalFrameI;  //!< \~english I frame interval (not used by now) \~chinese I帧间隔 (暂时无效)
		BYTE  byReserved[2];
	}NET_API_COMPRESSION_INFO_EX,*LPNET_API_COMPRESSION_INFO_EX;

    //! \~english Extended encoder parameters for device \~chinese 设备扩展编码参数
	typedef struct{
		DWORD dwSize;
		NET_API_COMPRESSION_INFO_EX struRecordPara; //!< \~english Settings for recording \~chinese 录像设置
		NET_API_COMPRESSION_INFO_EX struNetPara;    //!< \~english Settings for network streaming \~chinese 网传设置
	}NET_API_COMPRESSIONCFG_EX,*LPNET_API_COMPRESSIONCFG_EX;

	//! \~english Recoding schedule \~chinese 录像时段设置
	typedef struct{
		NET_API_SCHEDTIME struRecordTime; //!< \~english Recording time schedule \~chinese 录像时间段
        //! \~english \li 0:Always on, \li 1:Motion detect(MD), \li 2:External alarm(EA), \li 3:MD or EA, \li 4: MD and EA
        //! \~chinese \li 0:定时录像, \li 1:移动侦测, \li 2:报警录像, \li 3:动测|报警, \li 4:动测&报警
		BYTE byRecordType;  //!< \~english Record type \~chinese 录像类型
        
		char reservedData[3];
	}NET_API_RECORDSCHED,*LPNET_API_RECORDSCHED;

    //! \~english Recording schedule for one day \~chinese 单日录像设置
	typedef struct {
		WORD wAllDayRecord;             //!< \~english All day recording \~chinese 是否全天录像
		//! \~english \li 0:Always on \li 1:Motion detect(MD) \li 2:External alarm(EA) \li 3:MD or EA \li 4: MD and EA
        //! \~chinese \li 0:定时录像 \li 1:移动侦测 \li 2:报警录像 \li 3:动测|报警 \li 4:动测&报警
		BYTE byRecordType;  //!< \~english Record type \~chinese 录像类型
        
		char reservedData;
	}NET_API_RECORDDAY;

    //! \~english Recording settings \~chinese 录像设置
	typedef struct {
		DWORD dwSize;   //!< \~english Size of structure \~chinese 结构体大小
		DWORD dwRecord;  //!< \~english Enable recording: 0-disable, 1-enable \~chinese 是否录像 0-否 1-是
		NET_API_RECORDDAY struRecAllDay[MAX_DAYS]; //!< \~english Allday recording schedule for days \~chinese 全天录像设置
		NET_API_RECORDSCHED struRecordSched[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Recording time span for days(Only effective when allday recording is not enabled for that day.) \~chinese 录像时段设置(仅在该日不是全天录像的情况下有效)
		//! \~english \li 4: 120s \li 5: 300s \li 6: 600s
        //! \~chinese \li 4: 120秒 \li 5: 300秒 \li 6: 600秒
        DWORD dwRecordTime; //!< \~english Recording time limit for one file \~chinese 录象时间长度
        
		//! \~english \li 0-disable \li 1-30s \li 2-60s \li 3-120s \li 4-300s \li 5-600s \li 6-900s
        //! \~chinese \li 0-不预录 \li 1-30秒 \li 2-1分钟 \li 3-2分钟 \li 4-5分钟 \li 5-10分钟 \li 6-15分钟
        DWORD dwPreRecordTime;  //!< \~english Pre-record length \~chinese 预录时间 
        
	}NET_API_RECORD,*LPNET_API_RECORD;

	//! \~english PTZ decoder setting \~chinese 解码器设置
	typedef struct{
		DWORD dwSize;//!< \~english Size of structure \~chinese 结构大小
		DWORD dwBaudRate;//!< \~english Baud rate \~chinese 波特率(bps) \~ : 0－50, 1－75, 2－110, 3－150, 4－300, 5－600, 6－1200, 7－2400, 8－4800, 9－9600, 10－19200,  11－38400, 12－57600, 13－76800, 14－115200;
		BYTE byDataBit;//!< \~english Data bit \~chinese 数据有几位 \~ :0－5, 1－6, 2－7, 3－8
		BYTE byStopBit;//!< \~english Stop bit: 0-1 bit, 1-2 bit \~chinese 停止位 0－1位, 1－2位;
		BYTE byParity;//!< \~english Parity: 0-none, 1-even, 2-odd \~chinese 校验 0－无校验, 1－奇校验, 2－偶校验;
		BYTE byFlowcontrol;//!< \~english Flow control: 0-none, 1-hardware, 2-software \~chinese 流控: 0－无, 1－硬流控,2-软流控
		WORD wDecoderType;//!< \~english Decoder type \~chinese 解码器类型 \~ :0－YouLi, 3－Pelco-p, 8－Pelco-d PICO
		WORD wDecoderAddress;   //!< \~english Decoder address \~chinese 解码器地址 \~ : 0 ~ 255
		BYTE byDefaultSpeed;	//!< \~english Default PTZ speed \~chinese 默认云台运动速度 \~: 1 ~ 64
		BYTE bySetPreset[MAX_PRESET];       //!< \~english Preset has been setup: 0-no, 1-yes \~chinese 预置点是否设置,0-没有设置,1-设置
		BYTE bySetCruise[MAX_PRESET];       //!< \~english Cruise has been setup: 0-no, 1-yes \~chinese 巡航是否设置: 0-没有设置,1-设置
		BYTE bySetTrack[MAX_PRESET];        //!< \~english Track has been setup: 0-no, 1-yes \~chinese 轨迹是否设置,0-没有设置,1-设置
	}NET_API_DECODERCFG,*LPNET_API_DECODERCFG;

    //! \~english Cruise point setting \~chinese 巡航路径点设置
	typedef struct
	{
		unsigned int preset_num;    //!< \~english Preset number \~chinese 预置点
		unsigned int time;          //!< \~english Duration \~chinese 时长
		unsigned int speed;         //!< \~english Speed \~chinese 移动速度 \~: 1 ~ 64
	}NET_API_PTZ_POINT;

    //! \~english Cruise settings \~chinese 巡航路径设置
	typedef struct
	{
		NET_API_PTZ_POINT  point[MAX_CRUISE_POINT]; //!< \~english Cruise points \~chinese 巡航路径点
	}NET_API_PTZ_CRUISE;

	//! \~english Alarm input settings \~chinese 报警输入设置
	typedef struct{
		DWORD dwSize;					//!< \~english Size of structure \~chinese 结构大小
		BYTE sAlarmInName[NAME_LEN];    //!< \~english Name of sensor \~chinese 名称
		BYTE byAlarmType;				//!< \~english Type of sensor: 0: Normal Open, 1: Normal Close \~chinese 报警器类型,0：常开,1：常闭
		BYTE byAlarmInHandle;			//!< \~english Response to alarm \~chinese 是否处理
		NET_API_HANDLEEXCEPTION struAlarmHandleType;    //!< \~english Response type \~chinese 处理方式
		NET_API_SCHEDTIME struAlarmTime[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Response schedule \~chinese 布防时间
		BYTE byRelRecordChan[MAX_CHANNUM];		//!< \~english Trigger recording channel, set to 1 to enable \~chinese 报警触发的录象通道,为1表示触发该通道
		BYTE byEnablePreset[MAX_CHANNUM];       //!< \~english Trigger PTZ to goto preset \~chinese 是否调用预置点
		BYTE byPresetNo[MAX_CHANNUM];           //!< \~english Triggered PTZ preset number \~chinese 调用的云台预置点序号
		BYTE byEnableCruise[MAX_CHANNUM];       //!< \~english Trigger PTZ to run cruise \~chinese 是否调用巡航
		BYTE byCruiseNo[MAX_CHANNUM];           //!< \~english Triggered PTZ cruise number \~chinese  巡航
		BYTE byEnablePtzTrack[MAX_CHANNUM];     //!< \~english Trigger PTZ to follow track \~chinese 是否调用轨迹
		BYTE byPTZTrack[MAX_CHANNUM];           //!< \~english Triggered PTZ track number \~chinese 调用的云台的轨迹序号
	}NET_API_ALARMINCFG,*LPNET_API_ALARMINCFG;

	//! \~english Alarm output settings \~chinese 报警输出
	typedef struct{
		DWORD dwSize;					//!< \~english Size of structure \~chinese 结构大小
		BYTE sAlarmOutName[NAME_LEN];   //!< \~english Name of alarm output \~chinese 名称
		BYTE byAlarmType;				//!< \~english Type of alarm output: 0: Normal Open, 1: Normal Close \~chinese 报警器类型,0：常开,1：常闭
		DWORD dwAlarmOutDelay;			//!< \~english Output duration (seconds) \~chinese 输出保持时间(秒) */
		NET_API_SCHEDTIME struAlarmOutTime[MAX_DAYS][MAX_TIMESEGMENT];//!< \~english Alarm output schedule \~chinese 报警输出激活时间段
	}NET_API_ALARMOUTCFG,*LPNET_API_ALARMOUTCFG;

	//! \~english User information \~chinese 用户参数
	typedef struct{
		BYTE sUserName[NAME_LEN];       //!< \~english User name \~chinese 用户名
		BYTE sPassword[PASSWD_LEN];     //!< \~english User password \~chinese 密码
		//! \~ \li \~english Array index 0: PTZ control \~chinese 数组0: 本地控制云台
		//! \~ \li \~english Array index 1: Record manually \~chinese 数组1: 本地手动录象
		//! \~ \li \~english Array index 2: Playback \~chinese 数组2: 本地回放
		//! \~ \li \~english Array index 3: Settings \~chinese 数组3: 本地设置参数
		//! \~ \li \~english Array index 4: View Log \~chinese 数组4: 本地查看状态、日志
		//! \~ \li \~english Array index 5: System operations (upgrading/shutdown, etc.) \~chinese 数组5: 本地高级操作(升级, 关机等)
		DWORD dwLocalRight[MAX_RIGHT];  //!< \~english Local user rights, set to 1 to enable \~chinese 本地权限(设置1为允许)
		DWORD dwLocalPlaybackRight;     //!< \~english Playback channel mask, bit 0 <-> channel 1 \~chinese 本地可以回放的通道掩码，最低位表示通道1
		//! \~ \li \~english Array index 0: PTZ control \~chinese 数组0: 远程控制云台
		//! \~ \li \~english Array index 1: Record manually \~chinese 数组1: 远程手动录象
		//! \~ \li \~english Array index 2: Playback \~chinese 数组2: 远程回放 
		//! \~ \li \~english Array index 3: Settings \~chinese 数组3: 远程设置参数
		//! \~ \li \~english Array index 4: View log \~chinese 数组4: 远程查看状态、日志
		//! \~ \li \~english Array index 5: System operations (upgrading/shutdown, etc.)  \~chinese 数组5: 远程高级操作(升级, 关机等)
		//! \~ \li \~english Array index 6: Voice communication \~chinese 数组6: 远程发起语音对讲
		//! \~ \li \~english Array index 7: Preview \~chinese 数组7: 远程预览
		//! \~ \li \~english Array index 8: External alarm control \~chinese 数组8: 远程请求报警上传、报警输出
		//! \~ \li \~english Array index 9: Alarm output control \~chinese 数组9: 远程控制, 本地输出
		//! \~ \li \~english Array index 10: PTZ control \~chinese 数组10: 远程控制串口
		DWORD dwRemoteRight[MAX_RIGHT]; //!< \~english Remote user rights, set to 1 to enable \~chinese 远程权限(设置1为允许)
		
		DWORD dwNetPreviewRight;        //!< \~english Preview channel mask, bit 0 <-> channel 1 \~chinese 远程可以预览的通道掩码，最低位表示通道1
		DWORD dwNetPlaybackRight;       //!< \~english Playback channel mask, bit 0 <-> channel 1 \~chinese 远程可以回放的通道掩码，最低位表示通道1
		char sUserIP[16];               //!< \~english Only allow user login from this IP address(Set to 0.0.0.0 to disable) \~chinese 用户IP地址(为0时表示允许任何地址)
		BYTE byMACAddr[MACADDR_LEN];    //!< \~english Only allow user login from this MAC address(Set to 0 to disable) \~chinese 物理地址(设置为0表示允许任何地址)
	}NET_API_USER_INFO_EX,*LPNET_API_USER_INFO_EX;

	//! \~english All user's information \~chinese 所有用户参数
	typedef struct{
		DWORD dwSize;					//!< \~english Size of structure \~chinese 结构大小
		NET_API_USER_INFO_EX struUser[MAX_USERNUM]; //!< \~english Array of user information \~chinese 用户信息数组
	}NET_API_USER_EX,*LPNET_API_USER_EX;


	//! \~english User information \~chinese 用户参数
	typedef struct{
		BYTE sUserName[NAME_LEN];       //!< \~english User name \~chinese 用户名
		BYTE sPassword[PASSWD_LEN];     //!< \~english User password \~chinese 密码
		DWORD	dwPlayback;	// 远程同步回放权限
		DWORD	dwBackup; // 备份(视频下载)
		DWORD	dwPTZ; // 云台控制权限
		DWORD	dwRemotePreview; // 远程实时视频
		DWORD	dwLocalPreview; // 设备监控
		DWORD	dwOperation;	// 操作权限
	}NET_API_USER_INFO_EX_V20,*LPNET_API_USER_INFO_EX_V20;

	//! \~english All user's information \~chinese 所有用户参数
	typedef struct{
		DWORD dwSize;					//!< \~english Size of structure \~chinese 结构大小
		NET_API_USER_INFO_EX_V20 struUser[MAX_USERNUM]; //!< \~english Array of user information \~chinese 用户信息数组
	}NET_API_USER_EX_V20,*LPNET_API_USER_EX_V20;

	//! \~english All exception's settings \~chinese 异常参数
	typedef struct{
		DWORD dwSize;//!< \~english Size of structure \~chinese 结构大小
		//! \~english Array index \li 0-Disk Full \li 1-Disk Error \li 2-Network Error \li 3-Ip conflict \li 4-Illegal access \li 5-Signal unmatched \li 6-Encoder error \li 7-High temperature \li 8-Fan error
		//! \~chinese 数组 \li 0-硬盘满 \li 1-硬盘出错 \li 2-网线断 \li 3-局域网内IP地址冲突 \li 4-非法访问 \li 5-输入/输出视频制式不匹配 \li 6-编码器错误 \li 7-温度异常 \li 8-风扇异常
		NET_API_HANDLEEXCEPTION struExceptionHandleType[MAX_EXCEPTIONNUM]; //!< \~english Exception handling \~chinese 异常处理方式
	}NET_API_EXCEPTION,*LPNET_API_EXCEPTION;

	//! \~english Other network settings \~chinese 额外网络参数
	typedef struct {
		DWORD   dwSize;				//!< \~english Size of structure \~chinese 结构大小
		char    sFirstDNSIP[16];	//!< \~english Primary DNS \~chinese 主要DNS地址
		char    sSecondDNSIP[16];	//!< \~english Secondry DNS \~chinese 备用DNS地址
		char    sRes[32];			//!< \~english Reserved \~chinese 备用信息
	}NET_API_NETCFG_OTHER,*LPNET_API_NETCFG_OTHER;

	//! \~english Log Information \~chinese 日志信息
	typedef struct {
		ULONG    log_time;		//!< \~english Timestamp from \~chinese 时间戳至 \~ 1970-1-1 00:00:00
		unsigned char    major_type;    //!< \~english Major type \~chinese 主类型 \~ \see ::enumLogMajorTypes
		unsigned char    minor_type;    //!< \~english Minor type \~chinese 次类型 \~ \see ::enumLogAlarmMinorTypes, ::enumLogExceptionMinorTypes, ::enumLogOperationMinorTypes
		char             sUser[NAME_LEN]; //!< \~english User name \~chinese 用户名
		ULONG    ip;            //!< \~english Remote ip address \~chinese 远程主机地址
		unsigned char    para_type;     //!< \~english Parameter type \~chinese 参数类型 \~ \see ::enumLogOperationConfigTypes
		unsigned char    channel;       //!< \~english Channel number, user 0xff to disable \~chinese 通道号 0xff 不显示
		unsigned char    disk_number;   //!< \~english Hard driver number \~chinese 硬盘号
		unsigned char    alarm_in_port; //!< \~english Alarm input port \~chinese 报警输入端口
		unsigned char    alarm_out_port;//!< \~english Alarm output port \~chinese 报警输出端口
	}NET_API_LOG,*LPNET_API_LOG;

	//! \~english Start preview parameters \~chinese 启动预览参数
	typedef struct{
		LONG lChannel;//!< \~english Channel number \~chinese 通道号
		LONG lLinkMode;//!< \~english Stream type, 0-TCP, 3-RTP, use 0x80000000 mask for secondary stream \~chinese 码流类型 0：TCP方式, 3 - RTP方式， 使用0x80000000掩码来请求子码流，默认为主码流
		HWND hPlayWnd;//!< \~english Player window handle, use NULL if no video display needed. \~chinese 播放窗口的句柄,为NULL表示不播放图象
		char* sMultiCastIP;//!< \~english Multicast address \~chinese 多播组地址
	}NET_API_CLIENTINFO,*LPNET_API_CLIENTINFO;

	//! \~english Record file information \~chinese 录像文件信息
	typedef struct
	{
		char sFileName[100];//!< \~english File name \~chinese 文件名
		NET_API_TIME struStartTime;//!< \~english Record begin time \~chinese 文件的开始时间
		NET_API_TIME struStopTime;//!< \~english Record end time \~chinese 文件的结束时间
		DWORD dwFileSize;//!< \~english Record file size \~chinese 文件的大小
	}NET_API_FIND_DATA,*LPNET_API_FIND_DATA;

	//! \~english Jpeg file information \~chinese 抓图文件信息
	typedef struct
	{
		char sFileName[100];//!< \~english File name \~chinese 文件名
		DWORD dwYear;       //!< \~english Year \~chinese 年
		DWORD dwMonth;      //!< \~english Month \~chinese 月
		DWORD dwDay;        //!< \~english Day \~chinese 日
		DWORD dwHour;       //!< \~english Hour \~chinese 时
		DWORD dwMinute;     //!< \~english Minute \~chinese 分
		DWORD dwSecond;     //!< \~english Second \~chinese 秒
		DWORD dwMillisecond;//!< \~english Millisecod \~chinese 毫秒
		DWORD dwFileSize;//!< \~english File size \~chinese 文件的大小
	}NET_API_FIND_JPG_DATA,*LPNET_API_FIND_JPG_DATA;

	//! \~english Alarm output status \~chinese 报警输出状态
	typedef struct{
		BYTE Output[MAX_ALARMOUT]; //!< \~english Enable or disable output \~chinese 是否输出
	}NET_API_ALARMOUTSTATUS, *LPNET_API_ALARMOUTSTATUS;

	//! \~english Channel state \~chinese 通道状态
	typedef struct {
		BYTE byRecordStatic; //!< \~english Recording? 0-No, 1-Yes \~chinese 通道是否在录像,0-不录像,1-录像
		BYTE bySignalStatic; //!< \~english Signal lost? 0-No, 1-Yes \~chinese 连接的信号状态,0-正常,1-信号丢失
		BYTE byHardwareStatic;//!< \~english Hardware error? 0-No, 1-Yes \~chinese 通道硬件状态,0-正常,1-异常
		char reservedData;//!< \~english Reserved \~chinese 保留
		DWORD dwBitRate;//!< \~english Current bitrate \~chinese 实际码率
		DWORD dwLinkNum;//!< \~english Number of clients \~chinese 客户端连接的个数
		DWORD dwClientIP[MAX_LINK];//!< \~english IP address of clients \~chinese 客户端的IP地址
	}NET_API_CHANNELSTATE,*LPNET_API_CHANNELSTATE;

	//! \~english Hard driver state \~chinese 硬盘状态
	typedef struct {
		DWORD dwVolume;//!< \~english Capacity of disk \~chinese 硬盘的容量
		DWORD dwFreeSpace;//!< \~english Free space \~chinese 硬盘的剩余空间
		//! \~english Disk state : \li Low 4 bit: (dwHardDiskStatic&0xF) = <0-Active, 1-Sleeping, 2-Standby Error> \li Other bytes: (dwHardDiskStatic>>4) = <0-No disk, 1-Formatting, 2-Normal, 3-Error, 4-Initializing, 5-No Disk>
		//! \~chinese 硬盘的状态: \li 低4位(dwHardDiskStatic&0xF): (0-活动,1-休眠,2-休眠硬盘出错) \li 31~4位(dwHardDiskStatic>>4):(0-未检测到,1-正在格式化,2-正常,3-读写错误,4-正在初始化)
		DWORD dwHardDiskStatic;
		BYTE  byDiskID;			//!< \~english Disk id (0~15), Special: 127-Not connected, 255-SATA port not available \~chinese 硬盘序号:(0~15表示硬盘号,127-硬件sata口存在,但该sata口没接硬盘,255-表示sata口不存在)
		char  sDiskInfo[100];	//!< \~english Disk information string \~chinese 硬盘信息字符串
	}NET_API_DISKSTATE,*LPNET_API_DISKSTATE;

	//! \~english Device working state \~chinese 设备状态
	typedef struct{
		DWORD dwDeviceStatic;   //!< \~english Device state: 0-Normal, 1-High CPU, 2-Error \~chinese 设备的状态,0-正常,1-CPU占用率太高,超过85%,2-硬件错误,例如串口死掉
		NET_API_DISKSTATE  struHardDiskStatic[MAX_DISKNUM]; //!< \~english Hard drivers' state \~chinese 硬盘组状态
		NET_API_CHANNELSTATE struChanStatic[MAX_CHANNUM];	//!< \~english Channel state \~chinese 通道的状态
		BYTE  byAlarmInStatic[MAX_ALARMIN];					//!< \~english Alarm input state \~chinese 报警端口的状态,0-没有报警,1-有报警
		BYTE  byAlarmOutStatic[MAX_ALARMOUT];				//!< \~english Alarm output state \~chinese 报警输出端口的状态,0-没有输出,1-有报警输出
		DWORD  dwLocalDisplay;//!< \~english Local display state, 0-Normal, 1-Abnormal \~chinese 本地显示状态,0-正常,1-不正常
		DWORD  dwFanSpeed;//!< \~english Fan speed \~chinese 风扇转速
		DWORD  dwTemperature;//!< \~english Temperature \~chinese 温度
	}NET_API_WORKSTATE,*LPNET_API_WORKSTATE;

	//! \~english Reserved working state structure \~chinese 保留状态结构
	typedef struct{
		DWORD dwType;
		DWORD dwReservedArray_1[MAX_CHANNUM]; 
		DWORD dwReservedArray_2[MAX_CHANNUM];  
		DWORD dwReservedArray_3[16];
		DWORD dwReservedArray_4[16];
		DWORD dwReservedValue[10];
	}NET_API_WORKSTATE_RESERVED,*LPNET_API_WORKSTATE_RESERVED;

	//! \~english Cluster parameters \~chinese 级联参数
	typedef struct{
		char 	sHostName[128];			//!< \~english Host name \~chinese 主机名
		DWORD 	dwPort;					//!< \~english Port \~chinese 端口号
		char 	sUserName[NAME_LEN];	//!< \~english User name of remote device\~chinese 远程设备用户名
		char 	sPassword[PASSWD_LEN];	//!< \~english Password of remote device \~chinese 远程设备密码
		DWORD 	dwRemoteChannel;		//!< \~english Remote channel number \~chinese 需要级联的远程通道号
		DWORD 	dwLocalChannel;			//!< \~english Local channel number \~chinese 本地播放通道号
		DWORD	dwStream;				//!< \~english Stream type \~chinese 码流类型
        DWORD   dwRate;                 //!< \~english Stream encode rate \~chinese 码流大小 (Kbps)
        DWORD   dwType;                 //!< \~english Session type 0-DVR; 1-IPC \~chinese 连接类型
	}NET_API_CLUSTER_CONFIG,*LPNET_API_CLUSTER_CONFIG;

	//! \~english JPEG parameters \~chinese JPEG抓图参数
	typedef struct
	{
		WORD    wPicSize;               //!< \~english Picture size: \~chinese 图片大小: \~ \li 0=CIF \li 1=QCIF \li 2=D1 \li 3=720P \li 4=1080P
		WORD    wPicQuality;            //!< \~english Picture quality: \li 0-Best \li 1-Good \li 2-Normal \~chinese 图片质量系数: \li 0-最好 \li 1-较好 \li 2-一般
	}NET_API_JPEGPARA, *LPNET_API_JPEGPARA;

	//! \~english Device preview parameters \~chinese 设备本地预览参数
	typedef struct{
		DWORD dwSize;					//!< \~english Size of structure \~chinese 结构大小
		//! \~english Split windows: \li 8Way device: 0-12way, 1-8way, 2-6way, 3-4way \li 16Way device: 0-16way, 1-12way, 2-8way, 3-4way
		//! \~chinese 预览画面数目：\li 8路设备：0-12画面, 1-8画面, 2-6画面, 3-4画面 \li 16路设备：0-16画面, 1-12画面, 2-8画面, 3-4画面
		BYTE byPreviewNumber;			
		BYTE byEnableAudio;				//!< \~english Enable audio preview? 0-Disable, 1-Enable \~chinese 是否声音预览,0-不预览,1-预览
		WORD wSwitchTime;				//!< \~english Switch view interval: 0-disable, \~chinese 切换时间: 0-不切换, \~ 1-5s, 2-10s, 3-20s, 4-30s, 5-60s, 6-120s, 7-300s
		BYTE bySwitchSeq[MAX_WINDOW];	//!< \~english Array of channel number for each view: (0~15)preview window, (16~19)playback window \~chinese 窗口对应的通道 0～15为预览通道 16～19为回放通道
	}NET_API_PREVIEWCFG,*LPNET_API_PREVIEWCFG;

	//! \~english Device primary video output parameters \~chinese 设备主要视频输出参数
	typedef struct {
		WORD wResolution;                           //!< \~english Resolution \~chinese 分辨率
		WORD wFreq;									//!< \~english Refresh frequence \~chinese 刷新频率
		DWORD dwBrightness;                         //!< \~english Brightness \~chinese 亮度
	}NET_API_VGAPARA;

	//! \~english Matrix parameters \~chinese 视频矩阵配置
	typedef struct{
		WORD wDisplayLogo;                      //!< \~english Display logo \~chinese 显示视频通道号
		WORD wDisplayOsd;                       //!< \~english Display OSD \~chinese 显示时间
	}NET_API_MATRIXPARA;

	//! \~english Device secondary video output parameters \~chinese 视频次要视频输出参数
	typedef struct{
		BYTE byVideoFormat;                     //!< \~english Video format: \~chinese 输出制式: \~ 0-PAL, 1-NTSC
		BYTE byMenuAlphaValue;                  //!< \~english Menu alpha value \~chinese 菜单透明度
		WORD wScreenSaveTime;                   //!< \~english Screensaver timeout \~chinese 屏幕保护时间
		WORD wVOffset;                          //!< \~english Video out offset \~chinese 视频输出偏移
		WORD wBrightness;                       //!< \~english Brightness \~chinese 视频输出亮度
		BYTE byStartMode;                       //!< \~english Start mode (0-Menu, 1-Preview) \~chinese 启动后视频输出模式(0:菜单,1:预览)
		char reservedData;						
	}NET_API_VOOUT;

	//! \~english Device video output parameters \~chinese 设备视频输出参数集
	typedef struct {
		DWORD dwSize;							//!< \~english Size of structure \~chinese 结构大小
		NET_API_VOOUT struVOOut[MAX_VIDEOOUT];	//!< \~english Device secondary video output parameters \~chinese 视频次要视频输出参数
		NET_API_VGAPARA struVGAPara[MAX_VGA];   //!< \~english Device primary video output parameters \~chinese 设备主要视频输出参数
		NET_API_MATRIXPARA struMatrixPara;      //!< \~english Matrix parameters \~chinese 视频矩阵配置
	}NET_API_VIDEOOUT,*LPNET_API_VIDEOOUT;
    
    //! \~english Device video input format parameters \~chinese 设备视频输入格式设置
	typedef struct {
		DWORD dwSize;							//!< \~english Size of structure \~chinese 结构大小
		char sFormatNames[MAX_FORMATCOUNT][NAME_LEN];	        //!< \~english Input video format names \~chinese 视频输入格式名称
		DWORD dwInputVideoFormat;               //!< \~english Current input video format \~chinese 当前视频输入格式
	}NET_API_VIN_FORMAT,*LPNET_API_VIN_FORMAT;
    
    //! \~english Device spot output parameters \~chinese 设备Spot视频输出设置
	typedef struct {
		DWORD dwOutputChannel;              //!< \~english Video channel \~chinese 视频输出对应通道
		DWORD dwOutputMode;                 //!< \~english Spot video output mode \~chinese Spot视频输出模式
	}NET_API_SPOT_OUT,*LPNET_API_SPOT_OUT;
    
	//! \~english Email configuration \~chinese 电子邮件配置
	typedef struct
	{
		DWORD   dwSize;						  //!< \~english Size of structure \~chinese 结构大小
		char    sUserName[NAME_LEN];          //!< \~english Username \~chinese 帐户名
		char    sPassWord[NAME_LEN];          //!< \~english Password \~chinese 帐户密码
		char    sFromName[NAME_LEN];          //!< \~english Sender name \~chinese 发送者显示名称
		char    sFromAddr[MAILADDR_LEN];      //!< \~english Sender EMail address \~chinese 发送者邮箱地址
		char    sToName1[NAME_LEN];           //!< \~english Receiver1 name \~chinese 接收者1名称
		char    sToName2[NAME_LEN];           //!< \~english Receiver2 name \~chinese 接收者2名称
		char    sToAddr1[MAILADDR_LEN];       //!< \~english Receiver1 EMail address \~chinese 接收者1地址
		char    sToAddr2[MAILADDR_LEN];       //!< \~english Receiver2 EMail address \~chinese 接收者2地址
		char    sEmailServer[NAME_LEN];       //!< \~english Email server address \~chinese 邮件服务器地址
		BYTE    byServerType;				  //!< \~english Email server type: \~chinese 邮件服务器类型： \~ 0-SMTP, 1-POP3
		BYTE    byUseAuthen;				  //!< \~english Email server need authentication: 0-no, 1-yes \~chinese 邮件服务器需要验证：0-否, 1-是
		BYTE    byAttachment;                 //!< \~english Enable JPEG attachment: 0-no, 1-yes \~6chinese 启用抓图功能，以附件发送：0-否, 1-是
		BYTE    byMailinterval;               //!< \~english EMail sending interval (2~5s) \~chinese 邮件发送间隔: (2~5秒)
	} NET_API_EMAILCFG, *LPNET_API_EMAILCFG;
	
	//! \~english Ntp configuration \~chinese Ntp 配置
	typedef struct tagNTPPARA{ 
		BYTE sNTPServer[64];    //!< \~english Domain Name or IP addr of NTP server  \~chinese Ntp服务器地址     
		WORD wInterval;        //!< \~english adjust time interval(hours) \~chinese  较时间隔 
		BYTE byEnableNTP;     //!< \~english enable NPT client 0-no，1-yes \~chinese  启用Ntp较时标志 0- 不启用,1 -启用 
		signed char cTimeDifferenceH;  //!< \~english later of UTC  -12 ... +13  \~chinese 与国际标准时间的时差  -12 ... +13   
		signed char cTimeDifferenceM;   
		BYTE res[11]; 
	}NET_API_NTPPARA, *LPNET_API_NTPPARA; 
	
	//! \~english Uploaded alarm information \~chinese 上传报警信息
	typedef struct{
		//! \~english \li 0-Alarm input \li 1-Hard disk full \li 2-Signal lost \li 3-Motion detected \li 4-Disk is not formatted \li 5-Disk I/O error \li 6-Block detected \li 7-Signal format unmatch \li 8-Unauthorized access \li 9-High Temperature \li 10-Fan error
		//! \~chinese \li 0-信号量报警 \li 1-硬盘满 \li 2-信号丢失 \li 3-移动侦测 \li 4-硬盘未格式化 \li 5-读写硬盘出错 \li 6-遮挡报警 \li 7-制式不匹配 \li 8-非法访问 \li 9-温度异常 \li 10-风扇异常
		DWORD dwAlarmType;							//!< \~english Alarm type \~chinese 报警类型
		DWORD dwAlarmInputNumber;					//!< \~english Alarm input port \~chinese 报警输入端口
		DWORD dwAlarmOutputNumber[MAX_ALARMOUT];	//!< \~english Array of related alarm output, 1 means enabled \~chinese 报警输入端口对应的输出端口, 哪一位为1表示对应哪一个输出
		DWORD dwAlarmRelateChannel[MAX_CHANNUM];	//!< \~english Array of related channel triggerred recording, 1 means triggered \~chinese 报警输入端口对应的录像通道, 哪一位为1表示对应哪一路录像,dwAlarmRelateChannel[0]对应第1个通道
		DWORD dwChannel[MAX_CHANNUM];				//!< \~english When alarm type is 2 or 3 or 6, indicates which channel triggers the alarm \~chinese dwAlarmType为2或3,6时, 表示哪个通道, dwChannel[0]位对应第0个通道
		DWORD dwDiskNumber[MAX_DISKNUM];			//!< \~english When alarm type is 4, indicates which disk is not formatted \~chinese dwAlarmType为4时,表示哪个硬盘
	}NET_API_ALARMINFO,*LPNET_API_ALARMINFO;
    
    //! \~english Uploaded file creation information \~chinese 上传录像文件创建信息
	typedef struct{
		char sFileName[256];//!< \~english File name \~chinese 文件名
		NET_API_TIME struStartTime;//!< \~english Record begin time \~chinese 文件的开始时间
		NET_API_TIME struStopTime;//!< \~english Record end time \~chinese 文件的结束时间
		DWORD dwFileDuration;//!< \~english Record file duration \~chinese 文件的时长
		DWORD dwChannelNumber;//!< \~english Record file channel \~chinese 文件的通道号
		DWORD dwFileSize;//!< \~english Record file size \~chinese 文件的大小
		DWORD dwFileType;//!< \~english Record file type, 0-alarm, 1-timer, 2-manual \~chinese 文件的类型 0-报警, 1-定时, 2-手动
	}NET_API_FILE_CREATION_INFO,*LPNET_API_FILE_CREATION_INFO;

	//! \~english captured jpeg information \~chinese 报警抓图信息
	typedef struct{
		DWORD dwAlarmType;//!< \~english Alarm type
		DWORD dwChannelNumber;//!< \~english Alarm channel 
		DWORD dwTimestamp;//!< \~english Time which when to captured picture 
		DWORD dwJpegdataLen;//!< \~english jpeg picture data length
	}NET_API_JPEG_CAPTURED_INFO,*LPNET_API_JPEG_CAPTURED_INFO;

	//! \~english sequential captured jpeg information \~chinese 连续抓图信息
	typedef struct {
		DWORD dwSize;
		BYTE bySnapmode;            //!< \~english snap mode, \~chinese 抓图模式, \~ 0-2 不支持，自动，手动
		BYTE byReserved;            //!< \~english snap mode, \~chinese 抓图模式, \~ 0-2 不支持，自动，手动
		DWORD dwReserved1;
		DWORD dwReserved2;
		DWORD dwReserved3;
		DWORD dwReserved4;
	}NET_API_SNAP_INFO,*LPNET_API_SNAP_INFO;




	//------------------------cls4000 add struct begin-------------------------------

	//! \~english Image Enhance parameter \~chinese 图像增强参数
	typedef struct
	{
		BYTE channel;		//!< \~english Channel \~chinese 通道
		BYTE ieEn;		//!< \~english enable image enhance 0-no，1-yes \~chinese 启用图像增强标志 0- 不启用,1 -启用 
		BYTE ieStrength;	//!< \~english IE strength (0--255, Default 32) \~chinese 强度 (0--255, 默认32)
		BYTE ieSharp;		//!< \~english IE sharpness(0--7, Default 7) \~chinese 锐度 (0--7,默认7)
	}NET_API_IMAGE_ENHANCE;

	//! \~english Noise reduce paramters \~chinese 去噪参数
	typedef struct
	{
		BYTE channel;		//!< \~english Channel \~chinese 通道
		BYTE nrEn;		//!< \~english Enable or disable \~chinese 使能
		BYTE sfStrength;	//!< \~english Space filter strength(0--16, default 16) \~chinese 空域滤波强度(0--16, 默认16)
		BYTE tfStrength;	//!< \~english Time filter strength(0--16, default 8) \~chinese 时域滤波强度(0--16, 默认8)
	}NET_API_NOISE_REDUCE;


	//! \~english Image sharpen parameters \~chinese 图像锐化参数
	typedef struct
	{
		BYTE channel;		//!< \~english Channel \~chinese 通道
		BYTE spEn;		//!< \~english Enable or disable \~chinese 使能
		BYTE LumaGain;	//!< \~english Gain(0--255, default 40) \~chinese 强度(0--255, 默认40)
		BYTE rsv;
	}NET_API_SHARPEN;

	//! \~english Image settings \~chinese 视频图像参数
	typedef struct
	{
		BYTE channel;			//!< \~english Channel \~chinese 通道
		BYTE luminance;			//!< \~english Brightness (0--48, default 32) \~chinese 亮度(0--48, 默认32)
		BYTE contrast;			//!< \~english Contrast (0--48, default 8) \~chinese 对比度(0--48, 默认8)
		BYTE chromaRange;		//!< \~english Saturation (0--48, default 16) \~chinese 色度(0--48, 默认16)
		BYTE darkEnhance;		//!< \~english Dark enhance (0--48, 默认16) \~chinese 暗区增强(0--48, 默认16)
		BYTE brightEnhance;		//!< \~english Bright enhance0--48, 默认16) \~chinese 亮区增强(0--48, 默认16)
		BYTE rsv[2];
	}NET_API_IMAGE_INFO;

	//! \~english Restore Image settings \~chinese 视频图像参数恢复默认
	typedef struct
	{
		BYTE channel; //!< \~english Channel \~chinese 通道
		BYTE rsv[3];
	}NET_API_RESTORE_DEF;


	//! \~english Voice encoder switcher \~chinese 音频编码开关
	typedef struct
	{
		BYTE enable; //!< \~english Enable or disable \~chinese 使能
		BYTE reserve[3];
	}NET_API_SPEAKER_SWITCH;

	//! \~english OSD Types \~chinese OSD类型
	typedef enum
	{
		OSD_TYP_TIME = 0,
		OSD_TYP_NAME,
		OSD_TYP_USER,
		OSD_TYP_PTZ,
		OSD_TYP_ENCODE,
		OSD_TYP_NUM,
	}enumOsdType_E;
	#define MAX_OSD_LEN			255
	#define OSD_NUM				OSD_TYP_NUM
	
	//! \~english OSD Paramters \~chinese OSD 参数
	typedef struct
	{
		BYTE channel; //!< \~english Channel \~chinese 通道
		BYTE enable;				//!< \~english Enable or Disable \~chinese 使能
		BYTE osdType;				//!< \~english OSD Types  \~chinese OSD类型 \~ \see ::enumOsdType_E
		BYTE format;				//!< \~english When OSD Type is OSD_TYP_TIME, Time and Date format \~chinese 当OSD类型为OSD_TYP_TIME时，时间显示格式 \~ \see TimeFormat_E
		WORD x;					//!< \~english X position \~chinese X坐标 \~ (0--1920)
		WORD y;					//!< \~english Y position \~chinese Y坐标 \~ (0--1080)
		BYTE name[MAX_OSD_LEN];		//!< \~english When OSD Type is OSD_TYP_USER, User OSD \~chinese 当OSD类型为OSD_TYP_USER时:用户信息
		//OSD_TYP_NAME:通道名
	}NET_API_VIDEO_OSD;
	
	//! \~english Channel Video resolution \~chinese分辨率
/*	typedef enum
	{
		VIDEO_RESOLUTION_TYPE_1080P = 1,
		VIDEO_RESOLUTION_TYPE_720P,
		VIDEO_RESOLUTION_TYPE_D1,
		VIDEO_RESOLUTION_TYPE_CIF,
		VIDEO_RESOLUTION_TYPE_VGA,
		VIDEO_RESOLUTION_TYPE_QVGA,
	}enumVIDEO_RESOLUTION_TYPE;
	//! \~english Video Formats \~chinese视频输入制式
	typedef enum
	{
		NET_API_ENUM_VIDEO_IN_AUTO = 0,
		NET_API_ENUM_VIDEO_IN_1080P30,
		NET_API_ENUM_VIDEO_IN_1080P25,
		NET_API_ENUM_VIDEO_IN_1080I60,
		NET_API_ENUM_VIDEO_IN_1080I50,
		NET_API_ENUM_VIDEO_IN_720P60,
		NET_API_ENUM_VIDEO_IN_720P50,
		NET_API_ENUM_VIDEO_IN_720P30,
		NET_API_ENUM_VIDEO_IN_720P25,
		NET_API_ENUM_VIDEO_IN_NUM,
	}enumVideoInFormats;*/

	//! \~english Channel Video Format \~chinese 通道制式参数
/*	typedef struct
	{
		BYTE channel; //!< \~english Channel \~chinese 通道
		BYTE format; //!< \~english video formats \~chinese 制式 \~ \see ::enumVideoInFormats
		BYTE rsv[2];
	}NET_API_VIDEO_FORMAT_EX;*/

	//! \~english Daylight Saving Time Span \~chinese 夏令时时间段
	typedef struct
	{
		BYTE num;	 //!< \~english Week number \~chinese 第几个星期 \~ (1~5)
		BYTE month;	 //!< \~english Month \~chinese 月 \~ (1~12)
		BYTE week;	 //!< \~english Weekday (Sunday ~ Satuaday) \~chinese 星期（周日～周六） \~ (0~6)
		BYTE hour;	 //!< \~english Hour \~chinese 小时 \~ (0~23)
		BYTE minute;	 //!< \~english Minute \~chinese 分 \~ (0~59)
		BYTE second;	 //!< \~english Second \~chinese 秒 \~ (0~59)
		BYTE rsv[2];
	}NET_API_DST_SPAN;
	
	//! \~english Daylight saving time setting  \~chinese 夏令时设置
	typedef struct
	{
		BYTE enable;	 //!< \~english Enable or disable \~chinese 启用
		BYTE time;	 //!< \~english Offset(Hour) \~chinese 时间偏移(小时)
		BYTE rsv[2];
		NET_API_DST_SPAN start;	 //!< \~english DST start time \~chinese 夏令时开始
		NET_API_DST_SPAN end;	 //!< \~english DST end time \~chinese 夏令时结束
	}NET_API_DST_PARAM;
	
	//! \~english DDNS Setting \~chinese DDNS设置
	typedef struct
	{
		BYTE enable;	 //!< \~english Enable or disable \~chinese 启用
		BYTE provider;	 //!< \~english DDNS Provider \~chinese DDNS提供商 0-3322
		WORD updateTime;	 //!< \~english Update interval(10-1440 min) \~chinese 更新时间(10-1440分钟)
		char domain[NAME_LEN];	 //!< \~english DDNS Domain \~chinese 域名
		char user[NAME_LEN];	 //!< \~english DDNS Server username \~chinese 用户名
		char passwd[PASSWD_LEN];	 //!< \~english DDNS Server password \~chinese 密码
	}NET_API_DDNSCFG_EX;

	//! \~english Lane information \~chinese 车道统计设置信息
	typedef struct
	{
		BYTE enable;	 //使能
		BYTE direction;	 //方向:0X00--由下向上,0X01--由上向下
		BYTE rsv[2];
		WORD x[4];	 //x坐标
		WORD y[4];	 //y坐标
	}NET_API_LANE_INFO;

	//! \~english Lane config \~chinese 车道设置
	typedef struct
	{
		BYTE channel;	 //通道
		BYTE rsv[3];
		NET_API_LANE_INFO lane[MAX_LANE_NUM];	//车道信息
	}NET_API_LANE_CFG;

	//EVENT_ENCODE_VEHICLESTATISTICSRESULT
	typedef struct
	{
		BYTE channel;	 //通道
		BYTE rsv[3];
		DWORD result[MAX_LANE_NUM];	 //车辆统计结果	
	}NET_API_LANE_RESULT;

	//EVENT_ENCODE_VEHICLESTATISTICSRESET
	typedef struct
	{
		BYTE channel;	 //通道
		BYTE lane;	 //车道:0--车道1, 1--车道2, 3--全部
		BYTE rsv[2];
	}NET_API_LANE_RESET_COUNT;
	
	typedef struct
	{
		BYTE enable;
		BYTE streamType; //0 主码流,1 副码流 
		BYTE channel;
		char hostIp[16]; //接收端IP
		WORD wVideoPort;  //video port
		WORD wAudioPort;  //audio port
	}NET_API_AUTO_SENDRTP_CFG;

	typedef struct
	{
		BYTE dev;        //	PTZ_DEV_485	= 0,PTZ_DEV_422 = 1
		BYTE transmit_enable;
		BYTE rsv[2];
	}NET_API_PTZ_DEV_CFG;
	
	typedef struct
	{
		BYTE enable;
		BYTE source;	 //0 SPI,1 Line in
		BYTE rsv[2];
	}NET_API_AUDIO_INFO_CFG;

	//NET_API_GET_RTMP_CFG   and  NET_API_SET_RTMP_CFG struct
	typedef struct
	{
		BYTE enable;
		BYTE enable_param;
		WORD rtmpPort;
		BYTE rtmpIP[16];
		BYTE media_route[128];
		BYTE stream_name[128];
		BYTE param_ext[1024];
	}NET_API_RTMP_CFG;

	//NET_API_GET_PORT_MAP_CFG and NET_API_SET_PORT_MAP_CFG
	typedef struct
	{
		BYTE enable;
		BYTE rsv[3];
		DWORD port1;
		DWORD port2;
	}NET_API_PORT_MAP_CFG;

	//------------------------cls4000 add struct end---------------------------------

	//---------------cls440x decoder add begin---------------------
	//NET_API_GET_DECDEV_DEC_CFG and NET_API_SET_DECDEV_DEC_CFG
	//前端设备或流媒体服务器信息
	typedef struct
	{
		BYTE ip[MAX_IPADDRESS_LEN];			//设备或流媒体服务器IP地址
		WORD port;							//设备或流媒体服务器端口号
		BYTE channel;						//设备或流媒体服务器通道号
		BYTE streamType;					//传输码流模式: 0-主码流, 1-子码流 
		BYTE transProtocol;					//传输协议类型: 0-TCP, 1-UDP, 2-多播方式, 3-RTP 
		BYTE rsv[3];
		BYTE user[NAME_LEN];				//设备或流媒体服务器登陆帐号
		BYTE passwd[MAX_PASSWORD_LEN];		//设备或流媒体服务器密码
	}NET_API_DEC_CHANN_INFO_CFG;
	//RTSP信息
	typedef struct
	{
		BYTE transProtocol;					//传输协议类型: 0-TCP, 1-UDP, 2-多播方式, 3-RTP 
		BYTE rsv[3];
		BYTE user[NAME_LEN];				//设备或流媒体服务器登陆帐号
		BYTE passwd[MAX_PASSWORD_LEN];		//设备或流媒体服务器密码
		BYTE url[256];						//RTSP URL
	}NET_API_DEC_RTSP_INFO_CFG;
	//被动解码参数
	typedef struct
	{
		BYTE transProtocol;					//传输协议: 0-TCP, 1-UDP
		BYTE rsv[3];
		WORD port_v;							//视频端口号(不同的解码通道UDP端口号需设置为不同的值)
		WORD port_a;							//音频段口号(不同的解码通道UDP端口号需设置为不同的值)
	}NET_API_DEC_PASSIVE_CFG;
	//音频设置
	typedef enum
	{
		AUDIO_COMPRESS_TYPE_RAW			= 0,
		AUDIO_COMPRESS_TYPE_G711U		= 1,
		AUDIO_COMPRESS_TYPE_G711A		= 2,
		AUDIO_COMPRESS_TYPE_G723		= 3,
		AUDIO_COMPRESS_TYPE_G729		= 4,
	}AUDIO_COMPRESS_TYPE;
	//通道音频或对讲音频设置
	//NET_API_GET_DECDEV_AUDIO_CFG and NET_API_SET_DECDEV_AUDIO_CFG
	typedef struct
	{
		BYTE channel;				//0xff-对讲通道，其他-解码或编码设备通道
		BYTE enable;				//音频使能
		BYTE stereo;				//0-单声道， 1-双声道
		BYTE compress_type;			//编码类型, AUDIO_COMPRESS_TYPE
		WORD sample_rate;			//采样率
		WORD sample_size;			//数据长度
	}NET_API_AUDIO_CFG;
	//解码通道参数设置
	typedef struct
	{
		BYTE channel;				//通道
		BYTE valid;					//是否启用
		BYTE mode;					//模式: 0-主动, 1-被动
		BYTE streamType;			//流类型: 0-流媒体服务器, 1-前端设备
		NET_API_DEC_CHANN_INFO_CFG devInfo;		//前端设备信息
		NET_API_DEC_CHANN_INFO_CFG serverInfo;		//流媒体服务器信息
		NET_API_DEC_RTSP_INFO_CFG rtspInfo;			//rtsp信息
		NET_API_DEC_PASSIVE_CFG passive;			//被动解码参数
		BYTE outFormat;					//视频输出制式(制式列表由NET_API_GET_CHANNEL_VIDEO_OUTPUT_FORMAT，结构NET_API_VIN_FORMAT获取)
		BYTE scaleStatus;				//缩放控制:0-真实显示, 1-缩放显示
		BYTE decDelay;				//0- 默认, 1- 实时性好, 2- 实时性较好, 3- 实时性中, 流畅性中, 4- 流畅性较好, 5- 流畅性好, 0xff- 自动调整 
		BYTE rsv;
	}NET_API_DECDEV_DEC_CFG;

	//NET_API_GET_DECDEV_STATUS
	//解码通道状态
	typedef struct
	{
		BYTE channel;		//通道
		BYTE status;		//解码通道状态: 0-休眠, 1-正在连接, 2-已连接, 3-未收到数据, 4-正在解码
		BYTE decType;		//解码类型 :VideoEncType_E: 0-H.264, 1-MJPEG, 2-JPEG
		BYTE resolution;	//分辨率:VideoResolutionType_E: 0-1080p, 1-720p, 2-d1, 3-cif, 4-vga, 5-qvga
		BYTE frameRate;		//帧率
		BYTE rsv[3];
		DWORD streamRate;	//码流传输速率,单位kb/s 
	}NET_API_DECDEV_STATUS;

	//NET_API_GET_DECDEV_DISPLAY_CFG and NET_API_SET_DECDEV_DISPLAY_CFG


	//NET_API_GET_DECDEV_TRANS_CFG and NET_API_SET_DECDEV_TRANS_CFG
	//解码器透明通道设置
	typedef struct{
		DWORD dwBaudRate;//!< \~english Baud rate \~chinese 波特率(bps) \~ : 0－50, 1－75, 2－110, 3－150, 4－300, 5－600, 6－1200, 7－2400, 8－4800, 9－9600, 10－19200,  11－38400, 12－57600, 13－76800, 14－115200;
		BYTE byDataBit;//!< \~english Data bit \~chinese 数据有几位 \~ :0－5, 1－6, 2－7, 3－8
		BYTE byStopBit;//!< \~english Stop bit: 0-1 bit, 1-2 bit \~chinese 停止位 0－1位, 1－2位;
		BYTE byParity;//!< \~english Parity: 0-none, 1-odd, 2-even \~chinese 校验 0－无校验, 1－奇校验, 2－偶校验;
		BYTE byFlowcontrol;//!< \~english Flow control: 0-none, 1-hardware, 2-software \~chinese 流控: 0－无, 1－硬流控,2-软流控
		WORD wDecoderType;//!< \~english Decoder type \~chinese 解码器类型 \~ :0－YouLi, 3－Pelco-p, 8－Pelco-d PICO
		WORD wDecoderAddress;   //!< \~english Decoder address \~chinese 解码器地址 \~ : 0 ~ 255
		BYTE byDefaultSpeed;	//!< \~english Default PTZ speed \~chinese 默认云台运动速度 \~: 1 ~ 64
		BYTE rsv[3];
	}NET_API_PTZ_CFG;
	typedef struct
	{
		BYTE enable;								//是否启用透明通道
		BYTE localDevId;							//本地串口设备Id, 0-RS485, 1-RS422
		BYTE remoteDevId;							//远程串口设备Id, 0-RS485, 1-RS422
		BYTE rsv;
		BYTE remoteDevIP[MAX_IPADDRESS_LEN];		//远程设备IP地址
		DWORD remoteDevPort;						//远程设备端口 
		BYTE isEstablished;							//透明通道建立成功标志: 0-没有成功, 1-建立成功 
		BYTE rsv1[3];
		NET_API_PTZ_CFG remotePtz;					//远程设备云台配置信息，只读
		BYTE user[NAME_LEN];						//用户名 
		BYTE passwd[MAX_PASSWORD_LEN];				//密码
	}NET_API_DEC_TRANS_CFG;

	//---------------cls440x decoder add end---------------------
#pragma pack(pop)

	//! \~english Voice communication callback \~chinese 语言对讲回调函数
	typedef void (CALLBACK *VoiceDataCallBack)(LONG lVoiceComHandle,char *pRecvDataBuffer,DWORD dwBufSize,BYTE byAudioFlag,DWORD dwUser);
	//! \~english Serial data callback function \~chinese 串口数据回调函数
	typedef void (CALLBACK *SerialDataCallBack)(LONG lSerialHandle,const char *pRecvDataBuffer,DWORD dwBufSize,DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Device message callback.
     *  \param[out]  lCommand	    	    Message type 
                                            \li COMM_ALARM = 0x1100					- Alarm information
											\li COMM_WORKSTATE = 0x1200				- Device work state
											\li COMM_FILE_CREATION_INFO = 0x1700    - File creation information
											\see ::enumCommandMessages
     *  \param[out]  sDVRIP		            Device IP address
     *  \param[out]  pBuf		            Message data buffer
                                            \li Pointer to a ::NET_API_ALARMINFO, when type is COMM_ALARM
                                            \li Pointer to a ::NET_API_WORKSTATE, when type is COMM_WORKSTATE
											\li Pointer to a ::NET_API_FILE_CREATION_INFO, when type is COMM_FILE_CREATION_INFO
     *  \param[out]  dwBufLen		        Message data length
     *  \param[out]  dwUser		            User parameter set by ::NET_API_SetDVRMessageCallBack
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设备消息回调函数
	 *  \param[out]  lCommand	    	    消息类型
                                            \li COMM_ALARM = 0x1100					- 报警信息
                                            \li COMM_WORKSTATE = 0x1200				- 设备工作状态
											\li COMM_FILE_CREATION_INFO = 0x1700    - 录像文件创建信息
											\see ::enumCommandMessages
     *  \param[out]  sDVRIP		            设备IP地址
     *  \param[out]  pBuf		            消息数据缓冲区
                                            \li 指向一个 ::NET_API_ALARMINFO, 当消息类型是 COMM_ALARM
											\li 指向一个 ::NET_API_WORKSTATE, 当消息类型是 COMM_WORKSTATE
											\li 指向一个 ::NET_API_FILE_CREATION_INFO, 当消息类型是 COMM_FILE_CREATION_INFO
     *  \param[out]  dwBufLen		        消息数据长度
     *  \param[out]  dwUser		            ::NET_API_SetDVRMessageCallBack 设置的用户参数
     *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
     *  \see        NET_API_SetDVRMessageCallBack, NET_API_StartListen
	 */
    typedef BOOL (CALLBACK *MessageCallBack)(LONG lCommand,const char *sDVRIP,const char *pBuf,DWORD dwBufLen, DWORD dwUser);
	//! \~ \brief \~english Preview data callback \~chinese 预览数据回调函数 
	/**	\eng
	*	\param[out]		lRealHandle			Handle returned by ::NET_API_Realplay
	*	\param[out]		dwDataType	    	Type of callback data, please see ::enumFrameType
	*	\param[out]		pBuffer	    		Callback data
	*	\param[out]		dwBufSize    		Size of callback data
	*	\param[out]		dwUser		        User data set by ::NET_API_SetRealDataCallBack
	*   \else                                                               
	*	\param[out]		lRealHandle			::NET_API_Realplay 返回的播放句柄
	*	\param[out]		dwDataType	    	回调数据的类型, 请参考 ::enumFrameType
	*	\param[out]		pBuffer	    		回调数据
	*	\param[out]		dwBufSize    		回调数据大小
	*	\param[out]		dwUser		        ::NET_API_SetRealDataCallBack 设置的用户参数
	*   \endif                           
	*	\see NET_API_SetRealDataCallBack                                    
	*/
	typedef void (CALLBACK *RealDataCallBack)(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer,DWORD dwBufSize,DWORD dwUser);
	//! \~english Preview data callback reserved \~chinese 预览数据保留回调函数 \~ \see NET_API_SetRealDataCallBackEx
	typedef void (CALLBACK *RealDataCallBack_Ex)(BYTE* nal, unsigned int length, char end_nal, char end_frame, char start_nal, ULONG timestamp, char frame_type, char video_format, DWORD dwUser);
	//! \~english Preview data callback extended \~chinese 预览数据扩展回调函数 \~ \see NET_API_SetRealDataCallBackEx2
	typedef void (CALLBACK *RealDataCallBack_Ex2)(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer,DWORD dwBufSize,DWORD dwTimestamp, DWORD dwUser, DWORD dwReserve1, DWORD dwReserved2);
	//! \~english Playback data callback \~chinese 回放文件回调函数 \~ \see NET_API_SetExceptionCallBack, RealDataCallBack
	typedef void (CALLBACK *PlayDataCallBack)(LONG lPlayHandle, DWORD dwDataType, BYTE *pBuffer,DWORD dwBufSize,DWORD dwUser);
	//! \~english Search file callback \~chinese 检索文件回调函数 \~ \see NET_API_FindFileWithCallback
	typedef BOOL (CALLBACK *FindFileCallback)(LONG lFindHandle, LONG nResult, LPNET_API_FIND_DATA lpFindData,DWORD dwUser);

	/**
	 *  \eng
	 *  \brief 	Callback function will be called after one picture has been draw on hDC
	 *  \param[in] lRealHandle	Player handle
	 *  \param[in] hDc			Handle of device context
	 *  \param[in] dwWidth		Width of the picture
	 *  \param[in] dwHeight		Height of the picture
	 *  \param[in] dwUser		User data passed from call of h264_player_register_draw_function
	 *	\else
	 *  \brief 播放器图像绘制后调用的回调函数
	 *  \param[in] lRealHandle	播放句柄
	 *  \param[in] hDc			绘制DC句柄
	 *  \param[in] dwWidth		图像宽度
	 *  \param[in] dwHeight		图像高度
	 *  \param[in] dwUser		h264_player_register_draw_function传入的用户自定的数据
	 *	\endif
	 *	\see NET_API_RegisterDrawFun
	 */
	typedef void (CALLBACK *DrawFunction)(LONG lRealHandle, HDC hDc, DWORD dwWidth, DWORD dwHeight,DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Exception callback.
     *  \param[in]  dwType	    	    Exception type \see ::enumExceptionMessages                                        
     *  \param[in]  lUserID		        Exception related user id
     *  \param[in]  lHandle		        Exception related handle
     *  \param[in]  dwUser		        User parameter set by NET_API_SetExceptionCallBack
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		异常发生回调函数
	 *  \param[in]  dwType	    	    异常类型 \see ::enumExceptionMessages
     *  \param[in]  lUserID		        产生异常的用户ID
     *  \param[in]  lHandle		        产生异常的句柄
     *  \param[in]  dwUser		        NET_API_SetExceptionCallBack 设置的用户参数
     *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
     *  \see        NET_API_SetExceptionCallBack
	 */
    typedef void (CALLBACK* ExceptionCallBack)(DWORD dwType, LONG lUserID, LONG lHandle, DWORD dwUser); 
    /** 
	 *	\eng
	 *  \defgroup sdk SDK general functions
	 *  SDK general interfaces. 
	 *	\else
	 *  \defgroup sdk SDK相关
	 *  初始化以及清理SDK相关的接口。
	 *	\endif
	 */
	//@{
	/**
	 *	\eng
	 *  \brief 		Initialize SDK.
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		初始化SDK
	 *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Init();
	/**
	 *	\eng
	 *  \brief 		Release resources used by SDK.
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放SDK使用的资源
	 *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Cleanup();
	/**
	 *	\eng
	 *  \brief 		Get SDK version number.
	 *  \return 	High word is major version number, and low word is minor version number.
	 *	\else
	 *  \brief		获取SDK版本号
	 *  \return		2个高字节表示主版本, 2个低字节表示次版本
	 *	\endif
	 */
	NET_API DWORD CAL_CALL NET_API_GetSDKVersion();
    /**
	 *	\eng
	 *  \brief 		Get last error code.
	 *  \return 	Error code, see ::enumErrorCodes
	 *	\else
	 *  \brief		获取最后一次出现的错误的代码
	 *  \return		错误代码, 查看所有错误代码 ::enumErrorCodes
	 *	\endif
     *  \see        enumErrorCodes
	 */
    NET_API DWORD CAL_CALL NET_API_GetLastError();
    /**
	 *	\eng
	 *  \brief 		Set callback function for device messages
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  fMessageCallBack	Message callback function pointer
	 *  \param[in]  dwUser	            User parameter that will be passed to callback
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置设备消息回调函数
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  fMessageCallBack	设备消息回调函数指针
	 *  \param[in]  dwUser	            传给回调函数的用户参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        MessageCallBack
	 */
	NET_API BOOL CAL_CALL NET_API_SetDVRMessageCallBack(LONG lUserID, MessageCallBack fMessageCallBack, DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Set callback function for exceptions
	 *  \param[in]  fExceptionCallBack	Exception callback function pointer
	 *  \param[in]  dwUser	            User parameter that will be passed to callback
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置异常发生回调函数
	 *  \param[in]  fExceptionCallBack	异常发生回调函数指针
	 *  \param[in]  dwUser	            传给回调函数的用户参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        ExceptionCallBack
	 */
	NET_API BOOL CAL_CALL NET_API_SetExceptionCallBack(ExceptionCallBack fExceptionCallBack, DWORD dwUser);
    //@}
	/** 
	 *	\eng
	 *  \defgroup login User registration
	 *  Login and logout related interface. 
	 *	\else
	 *  \defgroup login 用户登录
	 *  用户登录注销相关的接口函数. 
	 *	\endif
	 */
	//@{
	/**
	 *	\eng
	 *  \brief 		Login a user to device.
	 *  \param[in]		sDVRIP			IP address of device
	 *  \param[in]		wDVRPort		Port number of device	
	 *  \param[in]		sUserName		User name
	 *  \param[in]		sPassword		Password
	 *  \param[out]		lpDeviceInfo	Pointer to device info structure
	 *  \return 	Success: None zero User Hash used for other APIs,			Fail: 0
	 *	\else
	 *  \brief 		用户登录到设备.
	 *  \param[in]		sDVRIP			设备的 IP 地址
	 *  \param[in]		wDVRPort		设备的端口号	
	 *  \param[in]		sUserName		登录的用户名
	 *  \param[in]		sPassword		用户密码
	 *  \param[out]		lpDeviceInfo	用于接收描述设备信息的数据结构的指针
	 *  \return 	成功: 非0表示用户 ID 值,用于其他API的输入参数				失败: 0
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_Login(const char *sDVRIP,WORD wDVRPort,const char *sUserName,const char *sPassword,LPNET_API_DEVICEINFO lpDeviceInfo);
	/**
	 *	\eng
	 *  \brief 		Logout a user from device.
	 *  \param[in]  lUserID		User ID returned by ::NET_API_Login 
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		从设备上注销用户
	 *  \param[in]  lUserID		::NET_API_Login 函数返回的用户ID值
	 *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Logout(LONG lUserID);
	//@}
	/** 
	 *	\eng
	 *  \defgroup config Device Configuration
	 *  Get or set device configuration. 
	 *	\else
	 *  \defgroup config 设备配置
	 *  获取以及设置设备参数相关的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Get Device configuration.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  dwCommand			Parameter type:
     <table>
     <tr><td>Parameter type</td><td>Data structure for lpOutBuffer/lpInBuffer</td><td>Description</td></tr>
     <tr><td>NET_API_GET_DEVICECFG</td>     <td>NET_API_DEVICECFG</td>  <td>Get device parameters</td></tr>
     <tr><td>NET_API_GET_NETCFG</td>        <td>NET_API_NETCFG</td>     <td>Get network parameters</td></tr>
	 <tr><td>NET_API_GET_DDNSCFG_EX</td>    <td>NET_API_DDNSCFG_EX</td>     <td>Get ddns parameters</td></tr>
	 <tr><td>NET_API_GET_PICCFG_EX</td>     <td>NET_API_PICCFG_EX</td><td>Get extra Image Parameters</td></tr>
     <tr><td>NET_API_GET_COMPRESSCFG_EX</td><td>NET_API_COMPRESSIONCFG_EX</td><td>Get extra Compression Parameters</td></tr>
     <tr><td>NET_API_GET_RECORDCFG</td>     <td>NET_API_RECORD</td><td>Get Record Parameters</td></tr>
     <tr><td>NET_API_GET_DECODERCFG</td>    <td>NET_API_DECODERCFG</td><td>Get PTZ Parameters</td></tr>
     <tr><td>NET_API_GET_ALARMINCFG</td>    <td>NET_API_ALARMINCFG</td><td>Get Alarm In Parameters</td></tr>
     <tr><td>NET_API_GET_ALARMOUTCFG</td>   <td>NET_API_ALARMOUTCFG</td><td>Get Alarm Out Parameters</td></tr>
     <tr><td>NET_API_GET_USERCFG_EX</td>    <td>NET_API_USER_EX</td><td>Get User Parameters</td></tr>
     <tr><td>NET_API_GET_EXCEPTIONCFG</td>  <td>NET_API_EXCEPTION</td><td>Get Exception Parameters</td></tr>
     <tr><td>NET_API_GET_EVENTCOMPCFG</td>  <td>NET_API_COMPRESSIONCFG_EX</td><td>Get Event Record Parameters</td></tr>
     <tr><td>NET_API_GET_NETCFG_OTHER</td>  <td>NET_API_NETCFG_OTHER</td><td>Get other network parameters</td></tr>
     <tr><td>NET_API_GET_EMAILPARACFG</td>  <td>NET_API_EMAILCFG</td><td>Get E-Mail Parameters</td></tr>
     <tr><td>NET_API_GET_PTZ_CRUISE</td>    <td>NET_API_PTZ_CRUISE</td><td>Get PTZ Cruise Parameters</td></tr>
	 <tr><td>NET_API_GET_TIME_ZONE</td>    <td>NET_API_TIME_ZONE</td><td>Get time and timezone</td></tr>
	 <tr><td>NET_API_GET_DST_PARAM</td>    <td>NET_API_DST_PARAM</td><td>Get DST parameters</td></tr>
     </table>
	 *  \param[in]  lChannel			Channel number, set to 0 if this is not used
	 *  \param[out] lpOutBuffer		    Output data buffer point to device configuration structure
	 *  \param[in]  dwOutBufferSize		Data buffer size
	 *  \param[out] lpBytesReturned	    Number of bytes returned
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		获取硬盘录像机的参数
	 *  \param[in]  lUserID				::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  dwCommand			参数类型:
     <table>
     <tr><td>参数类型</td><td>lpOutBuffer/lpInBuffer指向的结构体类型</td><td>描述</td></tr>
     <tr><td>NET_API_GET_DEVICECFG</td>     <td>NET_API_DEVICECFG</td>  <td>获取设备信息参数</td></tr>
     <tr><td>NET_API_GET_NETCFG</td>        <td>NET_API_NETCFG</td>     <td>获取网络参数</td></tr>
	 <tr><td>NET_API_GET_DDNSCFG_EX</td>    <td>NET_API_DDNSCFG_EX</td>     <td>获取DDNS参数</td></tr>
     <tr><td>NET_API_GET_PICCFG_EX</td>     <td>NET_API_PICCFG_EX</td><td>获取额外图像参数</td></tr>
     <tr><td>NET_API_GET_COMPRESSCFG_EX</td><td>NET_API_COMPRESSIONCFG_EX</td><td>获取额外编码参数</td></tr>
     <tr><td>NET_API_GET_RECORDCFG</td>     <td>NET_API_RECORD</td><td>获取录像参数</td></tr>
     <tr><td>NET_API_GET_DECODERCFG</td>    <td>NET_API_DECODERCFG</td><td>获取云台接口参数</td></tr>
     <tr><td>NET_API_GET_ALARMINCFG</td>    <td>NET_API_ALARMINCFG</td><td>获取报警输入参数</td></tr>
     <tr><td>NET_API_GET_ALARMOUTCFG</td>   <td>NET_API_ALARMOUTCFG</td><td>获取报警输出参数</td></tr>
     <tr><td>NET_API_GET_USERCFG_EX</td>    <td>NET_API_USER_EX</td><td>获取用户配置</td></tr>
     <tr><td>NET_API_GET_EXCEPTIONCFG</td>  <td>NET_API_EXCEPTION</td><td>获取异常处理参数</td></tr>
     <tr><td>NET_API_GET_EVENTCOMPCFG</td>  <td>NET_API_COMPRESSIONCFG_EX</td><td>获取触发录像参数</td></tr>
     <tr><td>NET_API_GET_NETCFG_OTHER</td>  <td>NET_API_NETCFG_OTHER</td><td>获取额外网络参数</td></tr>
     <tr><td>NET_API_GET_EMAILPARACFG</td>  <td>NET_API_EMAILCFG</td><td>获取邮件报警参数</td></tr>
     <tr><td>NET_API_GET_PTZ_CRUISE</td>    <td>NET_API_PTZ_CRUISE</td><td>获取云台巡航轨迹配置</td></tr>
     <tr><td>NET_API_GET_TIME_ZONE</td>    <td>NET_API_TIME_ZONE</td><td>获取设备时间时区</td></tr>
	 <tr><td>NET_API_GET_DST_PARAM</td>    <td>NET_API_DST_PARAM</td><td>获取设备夏令时设置</td></tr>
     </table>
	 *  \param[in]  lChannel			通道号,如果不是通道参数,lChannel 不用,置为0 即可
	 *  \param[out] lpOutBuffer		    存放输出参数的缓冲区
	 *  \param[in]  dwOutBufferSize		缓冲区的大小
	 *  \param[out] lpBytesReturned	    实际返回的缓冲区大小
	 *  \return		成功: TRUE,				失败: FALSE     
	 *	\endif
	 *	\see ::enumConfigTypes
	 */	
	NET_API BOOL CAL_CALL NET_API_GetDVRConfig(LONG lUserID, DWORD dwCommand,LONG lChannel, LPVOID lpOutBuffer,DWORD dwOutBufferSize,LPDWORD lpBytesReturned);
	/**
	 *	\eng
	 *  \brief 		Set device configuration.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  dwCommand			Parameter type:
	 <table>
	 <tr><td>Parameter type</td><td>Data structure for lpOutBuffer/lpInBuffer</td><td>Description</td></tr>
	 <tr><td>NET_API_SET_DEVICECFG</td>     <td>NET_API_DEVICECFG</td>  <td>Set device parameters</td></tr>
	 <tr><td>NET_API_SET_NETCFG</td>        <td>NET_API_NETCFG</td>     <td>Set network parameters</td></tr>
	 <tr><td>NET_API_SET_PICCFG_EX</td>     <td>NET_API_PICCFG_EX</td><td>Set extra Image Parameters</td></tr>
	 <tr><td>NET_API_SET_COMPRESSCFG_EX</td><td>NET_API_COMPRESSIONCFG_EX</td><td>Set extra Compression Parameters</td></tr>
	 <tr><td>NET_API_SET_RECORDCFG</td>     <td>NET_API_RECORD</td><td>Set Record Parameters</td></tr>
	 <tr><td>NET_API_SET_DECODERCFG</td>    <td>NET_API_DECODERCFG</td><td>Set PTZ Parameters</td></tr>
	 <tr><td>NET_API_SET_ALARMINCFG</td>    <td>NET_API_ALARMINCFG</td><td>Set Alarm In Parameters</td></tr>
	 <tr><td>NET_API_SET_ALARMOUTCFG</td>   <td>NET_API_ALARMOUTCFG</td><td>Set Alarm Out Parameters</td></tr>
	 <tr><td>NET_API_SET_USERCFG_EX</td>    <td>NET_API_USER_EX</td><td>Set User Parameters</td></tr>
	 <tr><td>NET_API_SET_EXCEPTIONCFG</td>  <td>NET_API_EXCEPTION</td><td>Set Exception Parameters</td></tr>
	 <tr><td>NET_API_SET_EVENTCOMPCFG</td>  <td>NET_API_COMPRESSIONCFG_EX</td><td>Set Event Record Parameters</td></tr>
	 <tr><td>NET_API_SET_NETCFG_OTHER</td>  <td>NET_API_NETCFG_OTHER</td><td>Set other network parameters</td></tr>
	 <tr><td>NET_API_SET_EMAILPARACFG</td>  <td>NET_API_EMAILCFG</td><td>Set E-Mail Parameters</td></tr>
	 <tr><td>NET_API_SET_TIME_ZONE</td>    <td>NET_API_TIME_ZONE</td><td>Set time and timezone</td></tr>
	 <tr><td>NET_API_SET_DST_PARAM</td>    <td>NET_API_DST_PARAM</td><td>Set DST parameters</td></tr>
	 </table>
	 *  \param[in]  lChannel			Channel number, set to 0 if this is not used
	 *  \param[in]  lpInBuffer			Input data buffer point to device configuration structure
	 *  \param[in]  dwInBufferSize		Data buffer size
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设置硬盘录像机的参数
	 *  \param[in]  lUserID				::NET_API_Login  函数返回的用户ID值
	 *  \param[in]  dwCommand			参数类型:
	 <table>
	 <tr><td>参数类型</td><td>lpOutBuffer/lpInBuffer指向的结构体类型</td><td>描述</td></tr>
	 <tr><td>NET_API_SET_DEVICECFG</td>     <td>NET_API_DEVICECFG</td>  <td>设置设备信息参数</td></tr>
	 <tr><td>NET_API_SET_NETCFG</td>        <td>NET_API_NETCFG</td>     <td>设置网络参数</td></tr>
	 <tr><td>NET_API_SET_DDNSCFG_EX</td>    <td>NET_API_DDNSCFG_EX</td>     <td>设置DDNS参数</td></tr>
	 <tr><td>NET_API_SET_PICCFG_EX</td>     <td>NET_API_PICCFG_EX</td><td>设置额外图像参数</td></tr>
	 <tr><td>NET_API_SET_COMPRESSCFG_EX</td><td>NET_API_COMPRESSIONCFG_EX</td><td>设置额外编码参数</td></tr>
	 <tr><td>NET_API_SET_RECORDCFG</td>     <td>NET_API_RECORD</td><td>设置录像参数</td></tr>
	 <tr><td>NET_API_SET_DECODERCFG</td>    <td>NET_API_DECODERCFG</td><td>设置云台接口参数</td></tr>
	 <tr><td>NET_API_SET_ALARMINCFG</td>    <td>NET_API_ALARMINCFG</td><td>设置报警输入参数</td></tr>
	 <tr><td>NET_API_SET_ALARMOUTCFG</td>   <td>NET_API_ALARMOUTCFG</td><td>设置报警输出参数</td></tr>
	 <tr><td>NET_API_SET_USERCFG_EX</td>    <td>NET_API_USER_EX</td><td>设置用户配置</td></tr>
	 <tr><td>NET_API_SET_EXCEPTIONCFG</td>  <td>NET_API_EXCEPTION</td><td>设置异常处理参数</td></tr>
	 <tr><td>NET_API_SET_EVENTCOMPCFG</td>  <td>NET_API_COMPRESSIONCFG_EX</td><td>设置触发录像参数</td></tr>
	 <tr><td>NET_API_SET_NETCFG_OTHER</td>  <td>NET_API_NETCFG_OTHER</td><td>设置额外网络参数</td></tr>
	 <tr><td>NET_API_SET_EMAILPARACFG</td>  <td>NET_API_EMAILCFG</td><td>设置邮件报警参数</td></tr>
	 <tr><td>NET_API_SET_TIME_ZONE</td>    <td>NET_API_TIME_ZONE</td><td>设置设备时间时区</td></tr>
	 <tr><td>NET_API_SET_DST_PARAM</td>    <td>NET_API_DST_PARAM</td><td>设置设备夏令时参数</td></tr>
	 </table>
	 *  \param[in]  lChannel			通道号,如果不是通道参数,lChannel 不用,置为0 即可
	 *  \param[in]  lpInBuffer			存放输入参数的缓冲区
	 *  \param[in]  dwInBufferSize		缓冲区的大小
	 *  \return		成功: TRUE,				失败: FALSE
	 *	\endif
     *  \see        NET_API_GetDVRConfig
	 *	\see ::enumConfigTypes
	 */	
	NET_API BOOL CAL_CALL NET_API_SetDVRConfig(LONG lUserID, DWORD dwCommand,LONG lChannel, LPVOID lpInBuffer,DWORD dwInBufferSize);

	/**
	 *	\eng
	 *  \brief 		Get device configuration node by name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sName			    Name of the configuration node, less than 1024 bytes.
	 *  \param[out] sOutValue	        Output buffer of the value of configuration node, must larger than 255 bytes
	 *  \param[in]  dwBufferSize		Output buffer size, must larger than 255 bytes.
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		以节点名称方式获取设备配置
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  sName			    配置节点名称, 小于1024字节
	 *  \param[out] sOutValue	        用于保存配置节点对应值的字符串数组，必须大于255个字节
	 *  \param[in]  dwBufferSize		输出缓冲区大小，必须大于255字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_GetDVRXMLConfig(LONG lUserID, const char* sName, char* sOutValue, DWORD dwBufferSize);
	/**
	 *	\eng
	 *  \brief 		Set device configuration node by name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sName			    Name of the configuration node, less than 1024 bytes.
	 *  \param[in]  sValue				The value of configuration node, less than 255 bytes
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		以节点名称方式设置设备配置
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  sName			    配置节点名称, 小于1024字节
	 *  \param[in]  sValue			    配置节点对应值的字符串数组，字符串最大255个字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_SetDVRXMLConfig(LONG lUserID, const char* sName, const char* sValue);
	/**
	 *	\eng
	 *  \brief 		Get device status node by name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sName			    Name of the status node, less than 1024 bytes.
	 *  \param[out] sOutValue	        Output buffer of the value of status node, must larger than 255 bytes
	 *  \param[in]  dwBufferSize		Output buffer size, must larger than 255 bytes.
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		以节点名称方式获取设备状态
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  sName			    状态节点名称, 小于1024字节
	 *  \param[out] sOutValue	        用于保存状态节点对应值的字符串数组，必须大于255个字节
	 *  \param[in]  dwBufferSize		输出缓冲区大小，必须大于255字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_GetDVRXMLStatus(LONG lUserID, const char* sName, char* sOutValue, DWORD dwBufferSize);
	/**
	 *	\eng
	 *  \brief 		Set device status node by name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sName			    Name of the status node, less than 1024 bytes.
	 *  \param[in]  sValue				The value of status node, less than 255 bytes
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		以节点名称方式设置设备状态
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  sName			    状态节点名称, 小于1024字节
	 *  \param[in]  sValue			    状态节点对应值的字符串数组，字符串最大255个字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_SetDVRXMLStatus(LONG lUserID, const char* sName, const char* sValue);
	/**
	 *	\eng
	 *  \brief 		Get device video effect settings
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \param[out] dwBrightness		Brightness of the picture, from 0~255
	 *  \param[out] dwContrast			Contrast of the picture, from 0~255
	 *  \param[out] dwSaturation		Saturation of the picture, from 0~255
	 *  \param[out] dwHue				Hue of the picture, from 0~255
	 *  \param[out] bAutoAdjust			Auto adjust flag
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		获取设备视频效果设置
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \param[out] dwBrightness		画面亮度 0~255
	 *  \param[out] dwContrast			画面对比度 0~255
	 *  \param[out] dwSaturation		画面色度 0~255
	 *  \param[out] dwHue				画面色调 0~255
	 *  \param[out] bAutoAdjust			是否自动调整
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_GetVideoEffect(LONG lUserID, LONG lChannel, DWORD* dwBrightness, DWORD* dwContrast, 
		DWORD* dwSaturation, DWORD* dwHue, BOOL* bAutoAdjust);
	/**
	 *	\eng
	 *  \brief 		Set device video effect
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \param[in]  dwBrightness		Brightness of the picture, from 0~255
	 *  \param[in]  dwContrast			Contrast of the picture, from 0~255
	 *  \param[in]  dwSaturation		Saturation of the picture, from 0~255
	 *  \param[in]  dwHue				Hue of the picture, from 0~255
	 *  \param[in]  bAutoAdjust			Auto adjust flag
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置设备视频效果
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \param[in]  dwBrightness		画面亮度 0~255
	 *  \param[in]  dwContrast			画面对比度 0~255
	 *  \param[in]  dwSaturation		画面色度 0~255
	 *  \param[in]  dwHue				画面色调 0~255
	 *  \param[in]  bAutoAdjust			是否自动调整
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_SetVideoEffect(LONG lUserID, LONG lChannel, DWORD dwBrightness, DWORD dwContrast, 
		DWORD dwSaturation, DWORD dwHue, BOOL bAutoAdjust);

	/**
	 *	\eng
	 *  \brief 		Get device work state
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[out] lpWorkState	        Device work state structure
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		获取设备工作状态
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[out] lpWorkState	        设备工作状态结构体
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_GetDVRWorkState(LONG lUserID,LPNET_API_WORKSTATE lpWorkState);
    	
	/**
	 *	\eng
	 *  \brief 		Get cruise information for specified channel and cruise. 
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  wChannel		Channel number
	 *  \param[in]  wCruiseID		Cruise id
	 *  \param[in]  lpOutBuffer		Pointer to NET_API_PTZ_CRUISE structure for received data
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		获取指定通道号和巡航路径编号的巡航路径信息
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  wChannel		通道号
	 *  \param[in]  wCruiseID		巡航路径编号
	 *  \param[in]  lpOutBuffer		保存返回结果的巡航路径结构体 NET_API_PTZ_CRUISE 的指针
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetPtzCruise(LONG lUserID, WORD wChannel, WORD wCruiseID, NET_API_PTZ_CRUISE* lpOutBuffer);
	
	/**
	 *	\eng
	 *  \brief 		Get RS485 configuration information for specified channel and device. 
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  wChannel		Channel number
	 *  \param[in]  wDevice			Device id
	 *  \param[in]  lpOutBuffer		Pointer to NET_API_DECODERCFG structure for received data
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		获取指定通道号和设备号的RS485串口参数
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  wChannel		通道号
	 *  \param[in]  wDevice			设备编号
	 *  \param[in]  lpOutBuffer		保存返回结果的串口参数结构体NET_API_DECODERCFG的指针
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Get485Config(LONG lUserID, WORD wChannel, WORD wDevice, LPNET_API_DECODERCFG lpOutBuffer);
	/**
	 *	\eng
	 *  \brief 		Set RS485 configuration information for specified channel and device. 
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  wChannel		Channel number
	 *  \param[in]  wDevice			Device id
	 *  \param[in]  lpInBuffer		Pointer to NET_API_DECODERCFG structure for input data
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		设置指定通道号和设备号的RS485串口参数
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  wChannel		通道号
	 *  \param[in]  wDevice			设备编号
	 *  \param[in]  lpInBuffer		串口参数结构体NET_API_DECODERCFG的指针
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Set485Config(LONG lUserID, WORD wChannel, WORD wDevice, LPNET_API_DECODERCFG lpInBuffer);

    /**
	 *	\eng
	 *  \brief 		Get device power schedule setting
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[out] lpPowerSchedule		Power schedule structure pointer
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		获取设备开机时间段设置
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[out] lpPowerSchedule		开机时间段结构体指针
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetPowerSchedule(LONG lUserID, NET_API_RECORD* lpPowerSchedule);
    /**
	 *	\eng
	 *  \brief 		Set device power schedule setting
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lpPowerSchedule		Power schedule structure pointer
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置设备开机时间段
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lpPowerSchedule		开机时间段结构体指针
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_SetPowerSchedule(LONG lUserID, NET_API_RECORD* lpPowerSchedule);
    /**
	 *	\eng
	 *  \brief 		Get devie config file
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sFileName		the name of local deviceconfig file
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		获取设备配置文件
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sFileName		本地的设备配置文件名
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetConfigFile(LONG lUserID,const char *sFileName);
	/**
	 *	\eng
	 *  \brief 		Set device config file
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sFileName		the name of local device config file
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		设置设备配置文件
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sFileName		本地的设备配置文件名
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SetConfigFile(LONG lUserID,const char *sFileName);
	/**
	 *	\eng
	 *  \brief 		Restore device config to default
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		恢复设备配置到默认值
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_RestoreConfig(LONG lUserID);
	/**
	 *	\eng
	 *  \brief 		Save device config to flash
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		保存设备配置到闪存
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SaveConfig(LONG lUserID);	
    //@}
	/** 
	 *	\eng
	 *  \defgroup log Log
	 *  Get device log information. 
	 *	\else
	 *  \defgroup log 日志
	 *  获取设备日志相关的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Search for device log.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  lSelectMode			Search type
	 *  								\li	 0 - all 
	 *  								\li	 1 - by type 
	 *  								\li	 2 - by time
	 *  								\li	 3 - by time and type
	 *  \param[in]  dwMajorType			Major type of log (see MAJOR_ALARM_* defines)
	 *  \param[in]  dwMinorType			Minor type of log (see MINOR_* defines)
	 *  \param[in]  lpStartTime			Start time for search
	 *  \param[in]  lpStopTime			Stop time for search
	 *  \return 	Success: Handle for device log search,			Fail: -1
	 *	\else
	 *  \brief		查询设备日志
	 *  \param[in]  lUserID				::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lSelectMode			查询方式
	 *									\li	0－全部
	 *									\li	1－按类型
	 *									\li	2－按时间
	 *									\li	3－按时间和类型
	 *  \param[in]  dwMajorType			需查询的日志主类型 (参见 MAJOR_ALARM_* 宏定义)
	 *  \param[in]  dwMinorType			需查询的日志副类型 (参见 MINOR_* 宏定义)
	 *  \param[in]  lpStartTime			需查询日志的起始时间
	 *  \param[in]  lpStopTime			需查询日志的终止时间
	 *  \return		成功: 日志查询句柄,				失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_FindDVRLog(LONG lUserID,LONG lSelectMode, DWORD dwMajorType,DWORD dwMinorType, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime);
	/**
	 *	\eng
	 *  \brief 		Get device log information.
	 *  \param[in]  lLogHandle			Handle for device log, returned by ::NET_API_FindDVRLog
	 *  \param[out] lpLogData			Output log data
	 *  \return 	\li NET_API_FILE_SUCCESS 	--	Get log info successful
					\li NET_API_FILE_NOFIND		--	No log is found
					\li NET_API_ISFINDING		--	Searching
					\li NET_API_NOMORE_FILE		--	No more log
					\li NET_API_FILE_EXCEPTION	--	Exception in searching
	 *	\else
	 *  \brief		获取日志信息
	 *  \param[in]  lLogHandle			::NET_API_FindDVRLog 函数返回的日志查询句柄
	 *  \param[out] lpLogData			返回的日志信息
	 *  \return		
					\li NET_API_FILE_SUCCESS    -- 获得日志信息
					\li NET_API_FILE_NOFIND     -- 没有日志
					\li NET_API_ISFINDING       -- 正在查找日志
					\li NET_API_NOMORE_FILE     -- 查找日志时没有更多的日志
					\li NET_API_FILE_EXCEPTION  -- 查找日志时异常
	 *	\endif
	 *	\see enumReturnValuesForFinding
	 */
	NET_API LONG CAL_CALL NET_API_FindNextLog(LONG lLogHandle,LPNET_API_LOG lpLogData);
	/**
	 *	\eng
	 *  \brief 		Release resources for log search.
	 *  \param[in]  lLogHandle			Handle for device log, returned by ::NET_API_FindDVRLog
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放查找日志的资源
	 *  \param[in]  lLogHandle			::NET_API_FindDVRLog 函数返回的日志查询句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_FindLogClose(LONG lLogHandle);
    //@}
	/** 
	 *	\eng
	 *  \defgroup preview Preview
	 *  Preview video stream from device. 
	 *	\else
	 *  \defgroup preview 预览
	 *  预览设备视频流的相关接口. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Start real time preview. 
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lpClientInfo	Preview configuration parameter
	 *  \return 	Success: Handle for real time preview, 			Fail: -1
	 *	\else
	 *  \brief		启动实时图像码流
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lpClientInfo	图像预览参数
	 *  \return		成功: 实时播放句柄,								失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_RealPlay(LONG lUserID, const LPNET_API_CLIENTINFO lpClientInfo);
	/**
	 *	\eng
	 *  \brief 		Stop real time preview. 
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		停止实时图像码流
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_StopRealPlay(LONG lRealHandle);
	/**
	*	\eng
	*  \brief 		Enable or Disable water mark validation
	*  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	*  \param[in]  bEnable				Enable or Disable validation
	*  \return 	Success: TRUE, 			Fail: FALSE
	*	\else
	*  \brief		启用或禁用码流水印校验
	*  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	*  \param[in]  bEnable				启用或禁用校验
	*  \return 	成功: TRUE, 			失败: FALSE
	*	\endif
	*/
	NET_API BOOL CAL_CALL NET_API_EnableWaterMarkValidation(LONG lRealHandle, BOOL bEnable);
	/**
	*	\eng
	*  \brief 		Get water mark state of stream. 
	*  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	*  \return 	0-Unknown, 1-Valid, 2-Invalid, 3-WaterMark Not supported, 4-WaterMark Validation Not enabled
	*	\else
	*  \brief		获取码流水印状态
	*  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	*  \return 	0-未知, 1-有效, 2-无效, 3-不支持水印, 4-未启用校验
	*	\endif
	*/
	NET_API LONG CAL_CALL NET_API_GetWaterMarkState(LONG lRealHandle);
	/**
	 *	\eng
	 *  \brief 		Start writing video & audio stream to movie file. 
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  sFileName			Destination file name
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		开始保存实时图像码流到视频文件
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  sFileName			存盘文件名
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SaveRealData(LONG lRealHandle,const char *sFileName) ;
	/**
	 *	\eng
	 *  \brief 		Stop writing video stream to file. 
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		停止保存实时图像码流
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_StopSaveRealData(LONG lRealHandle) ;
	/**
	 *	\eng
	 *  \brief 		Set callback function for stream data. 
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  fRealDataCallBack	Callback function pointer
	 *  \param[in]  dwUser				User data that will be passed to callback
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设置实时图像码流数据回调函数
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  fRealDataCallBack	回调函数
	 *  \param[in]  dwUser				将传给回调函数的用户自定义数据
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        RealDataCallBack
	 */
	NET_API BOOL CAL_CALL NET_API_SetRealDataCallBack(LONG lRealHandle,RealDataCallBack fRealDataCallBack,DWORD dwUser) ;

	/**
	 *	\eng
	 *  \brief 		Set callback function for stream data, with extra information about stream data. 
	 *  \param[in]  lRealHandle			Handle for real time preview returned by:: NET_API_RealPlay
	 *  \param[in]  fRealDataCallBackEx	Callback function pointer
	 *  \param[in]  dwUser				User data that will be passed to callback
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设置实时图像码流数据回调函数, 回调函数将获得更多的码流信息
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  fRealDataCallBackEx	回调函数
	 *  \param[in]  dwUser				将传给回调函数的用户自定义数据
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        RealDataCallBack_Ex
	 */	
    NET_API BOOL CAL_CALL NET_API_SetRealDataCallBackEx(LONG lRealHandle, RealDataCallBack_Ex fRealDataCallBackEx, DWORD dwUser) ;

	/**
	 *	\eng
	 *  \brief 		Set callback function for stream data, with extra information about stream data. 
	 *  \param[in]  lRealHandle				Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]  fRealDataCallBackEx2	Callback function pointer
	 *  \param[in]  dwUser				    User data that will be passed to callback
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设置实时图像码流数据回调函数, 回调函数将获得更多的码流信息
	 *  \param[in]  lRealHandle				::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]  fRealDataCallBackEx2    回调函数
	 *  \param[in]  dwUser				    将传给回调函数的用户自定义数据
	 *  \return 	成功: TRUE, 			    失败: FALSE
	 *	\endif
     *  \see        RealDataCallBack_Ex2
	 */	
    NET_API BOOL CAL_CALL NET_API_SetRealDataCallBackEx2(LONG lRealHandle, RealDataCallBack_Ex2 fRealDataCallBackEx2, DWORD dwUser) ;
	/**
	 *	\eng
	 *  \brief 		Capture picture from video stream, and save to BMP file. 
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]  sPicFileName		Destination file name
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		抓取码流数据图像到BMP
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]  sPicFileName		存盘文件名
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CapturePicture(LONG lRealHandle,const char *sPicFileName);
	/**
	 *	\eng
	 *  \brief 		Capture picture from video stream, and save to Jpeg file. 
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]  sPicFileName		Destination file name
	 *  \param[in]  nQuality			JPEG quality
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		抓取码流数据图像到Jpeg文件
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]  sPicFileName		存盘文件名
	 *  \param[in]  nQuality			JPEG 质量
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CapturePictureToJpeg(LONG lRealHandle,const char *sPicFileName, int nQuality);
	/**
	 *	\eng
	 *  \brief 		Generate a key frame in real time preview. 
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		网络预览时动态产生一个关键帧
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		通道号
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_MakeKeyFrame(LONG lUserID,LONG lChannel);
	/**
	 *	\eng
	 *  \brief 		Generate a key frame for CIF stream.
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		子码流生成一个关键帧
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		通道号
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_MakeKeyFrameSub(LONG lUserID,LONG lChannel);

	/**
	 *	\eng
	 *  \brief 		Open sound for preview/playback, this must be called after preview/playback is started.
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		启动回放声音, 必须在启动预览/回放之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName /::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_OpenSound(LONG lRealHandle);

	/**
	 *	\eng
	 *  \brief 		Close sound for preview/playback, this must be called after preview/playback is started.
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		停止回放声音, 必须在启动预览/回放之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CloseSound(LONG lRealHandle);

	/**
	 *	\eng
	 *  \brief 		Change sound volume for preview/playback, this must be called after preview/playback is started.
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]  wVolume				New volume value, between 0 to 100
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		更改回放声音音量, 必须在启动预览/回放之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]  wVolume				新的音量值, 在0-100之间
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API	BOOL CAL_CALL NET_API_Volume(LONG  lRealHandle,WORD  wVolume);
	
	/**
	 *	\eng
	 *  \brief		Adjust player's video smooth level. Lower level cause playback more real time. Higher level cause playback more smooth.
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]	dwLevel				Desire smooth level, from 1 to 10. (default is 6)
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		调节播放器的流畅程度，数值越低表示实时性越高，越高表示越流畅
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]	dwLevel				从1-10的流畅度数值(默认为6)
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API	BOOL CAL_CALL NET_API_SetSmoothLevel(LONG  lRealHandle, DWORD dwLevel);

	/**
	 *	\eng
	 *  \brief		Register draw callback to player.
	 *  \param[in]  lRealHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]	fDrawFun			Callback function pointer
	 *  \param[in]	dwUser				User defined data will be passed to callback function
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		注册绘制回调函数
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]	fDrawFun			回调函数
	 *  \param[in]	dwUser 				回调函数用户参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API	BOOL CAL_CALL NET_API_RegisterDrawFun(LONG  lRealHandle, DrawFunction fDrawFun,DWORD  dwUser);

	/**
	 *	\eng
	 *  \brief		Change the window handle that player uses
	 *  \param[in]  lPlayHandle			Handle for preview/playback returned by ::NET_API_RealPlay or ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime
	 *  \param[in]	hWnd				New window handle on which video will be draw.
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		更改播放器绘制窗口
	 *  \param[in]  lPlayHandle			::NET_API_RealPlay 或者 ::NET_API_PlayBackByName / ::NET_API_PlayBackByTime 函数返回的播放句柄
	 *  \param[in]	hWnd				新窗口句柄.
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_ChangeDisplayWnd(LONG lPlayHandle,HWND hWnd);
    //@}
	/** 
	 *	\eng
	 *  \defgroup ptz PTZ control
	 *  Control PTZ from device. 
	 *	\else
	 *  \defgroup ptz 云台控制
	 *  控制设备连接的云台设备. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Get control permission to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwCmd				\li 1 - get control permission  \li 2 - release control permission
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		查看云台控制权, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwCmd				\li 1 - 获得云台控制权 \li 2 - 释放云台控制权
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetPTZCtrl(LONG lRealHandle,DWORD dwCmd);

	 //@}
	/** 
	 *	\eng
	 *  \defgroup ptz PTZ control
	 *  Control PTZ from device. 
	 *	\else
	 *  \defgroup ptz 云台控制
	 *  控制设备连接的云台设备. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Get control permission to PTZ, don't need preview.
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwCmd				\li 1 - get control permission  \li 2 - release control permission
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		查看云台控制权, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwCmd				\li 1 - 获得云台控制权 \li 2 - 释放云台控制权
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */

	NET_API BOOL CAL_CALL NET_API_GetPTZCtrl_WithoutPreview(LONG lUserID,LONG lChannel,DWORD dwCmd);
	/**
	 *	\eng
	 *  \brief 		Send Control command to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送云台控制命令, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZControl(LONG lRealHandle,DWORD dwPTZCommand,DWORD dwStop);
	/**
	 *	\eng
	 *  \brief 		Send Control command to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送云台控制命令, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */

	NET_API BOOL CAL_CALL NET_API_PTZControl_WithoutPreview(LONG lUserID,LONG lChannel,DWORD dwPTZCommand,DWORD dwStop);
    /**
	 *	\eng
	 *  \brief 		Send Control command to PTZ with speed, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
     *  \param[in]  dwSpeed				PTZ speed: 1 ~ 64
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送带速度的云台控制命令, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \param[in]  dwSpeed				云台速度: 1 ~ 64
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZControlWithSpeed(LONG lRealHandle, DWORD dwPTZCommand, DWORD dwStop, DWORD dwSpeed);
	
	 /**
	 *	\eng
	 *  \brief 		Send Control command to PTZ with speed, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
     *  \param[in]  dwSpeed				PTZ speed: 1 ~ 64
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送带速度的云台控制命令, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \param[in]  dwSpeed				云台速度: 1 ~ 64
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZControlWithSpeed_WithoutPreview(LONG lUserID,LONG lChannel , DWORD dwPTZCommand, DWORD dwStop, DWORD dwSpeed);

	/**
	 *	\eng
	 *  \brief 		Send Control command to selected PTZ device, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwDeviceID			PTZ device ID
     *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送云台控制命令到指定设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwDeviceID			云台设备编号
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZControl_Ex(LONG lRealHandle, DWORD dwDeviceID, DWORD dwPTZCommand,DWORD dwStop);

	/**
	 *	\eng
	 *  \brief 		Send Control command to selected PTZ device, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwDeviceID			PTZ device ID
     *  \param[in]  dwPTZCommand		PTZ Commands 
	 *						\li PTZ_LIGHT_PWRON         2   power on light
	 *						\li PTZ_WIPER_PWRON         3   power on wiper
	 *						\li PTZ_FAN_PWRON           4   power on fan
	 *						\li PTZ_HEATER_PWRON        5   power on heater
	 *						\li PTZ_AUX_PWRON           6   power on aux 
	 *						\li PTZ_ZOOM_IN             11  zoom in
	 *						\li PTZ_ZOOM_OUT            12  zoom out
	 *						\li PTZ_FOCUS_NEAR          13  focus near
	 *						\li PTZ_FOCUS_FAR           14  focus far
	 *						\li PTZ_IRIS_OPEN           15  iris open
	 *						\li PTZ_IRIS_CLOSE          16  iris close
	 *						\li PTZ_TILT_UP             21  tilt up
	 *						\li PTZ_TILT_DOWN           22  tilt down
	 *						\li PTZ_PAN_LEFT            23  pan left
	 *						\li PTZ_PAN_RIGHT           24  pan right
	 *						\li PTZ_UP_LEFT             25  move up left
	 *						\li PTZ_UP_RIGHT            26  move up right
	 *						\li PTZ_DOWN_LEFT           27  move down left
	 *						\li PTZ_DOWN_RIGHT          28  move down right
	 *						\li PTZ_PAN_AUTO            29  pan automaticly
	 *  \param[in]  dwStop				Start or stop Action, 0-Start, 1－Stop
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送云台控制命令到指定设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwDeviceID			云台设备编号
	 *  \param[in]  dwPTZCommand		PTZ 命令： 
	 *						\li PTZ_LIGHT_PWRON         2   接通灯光电源 
	 *						\li PTZ_WIPER_PWRON         3   接通雨刷开关 
	 *						\li PTZ_FAN_PWRON           4   接通风扇开关 
	 *						\li PTZ_HEATER_PWRON        5   接通加热器开关 
	 *						\li PTZ_AUX_PWRON           6   接通辅助设备开关 
	 *						\li PTZ_ZOOM_IN             11  焦距以速度SS变大(倍率变大) 
	 *						\li PTZ_ZOOM_OUT            12  焦距以速度SS变小(倍率变小) 
	 *						\li PTZ_FOCUS_NEAR          13  焦点以速度SS前调 
	 *						\li PTZ_FOCUS_FAR           14  焦点以速度SS后调 
	 *						\li PTZ_IRIS_OPEN           15  光圈以速度SS扩大 
	 *						\li PTZ_IRIS_CLOSE          16  光圈以速度SS缩小 
	 *						\li PTZ_TILT_UP             21  云台以SS的速度上仰 
	 *						\li PTZ_TILT_DOWN           22  云台以SS的速度下俯 
	 *						\li PTZ_PAN_LEFT            23  云台以SS的速度左转 
	 *						\li PTZ_PAN_RIGHT           24  云台以SS的速度右转 
	 *						\li PTZ_UP_LEFT             25  云台以SS的速度上仰和左转 
	 *						\li PTZ_UP_RIGHT            26  云台以SS的速度上仰和右转
	 *						\li PTZ_DOWN_LEFT           27  云台以SS的速度下俯和左转 
	 *						\li PTZ_DOWN_RIGHT          28  云台以SS的速度下俯和右转 
	 *						\li PTZ_PAN_AUTO            29  云台以SS的速度左右自动扫描 
	 *  \param[in]  dwStop				让云台停止动作还是开始动作, 0-开始, 1－停止
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZControl_Ex_WithoutPreview(LONG lUserID,LONG lChannel, DWORD dwDeviceID, DWORD dwPTZCommand,DWORD dwStop);

	/**
	 *	\eng
	 *  \brief 		Send transparent data to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  pPTZCodeBuf         Transplant data buffer contains PTZ data
	 *  \param[in]  dwBufSize			Buffer size
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送透明通道数据到串口设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  pPTZCodeBuf         待发送的透明通道数据缓冲区
	 *  \param[in]  dwBufSize			缓冲区大小
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_TransPTZ(LONG lRealHandle,const char *pPTZCodeBuf,DWORD dwBufSize);

	/**
	 *	\eng
	 *  \brief 		Send transparent data to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  pPTZCodeBuf         Transplant data buffer contains PTZ data
	 *  \param[in]  dwBufSize			Buffer size
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送透明通道数据到串口设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  pPTZCodeBuf         待发送的透明通道数据缓冲区
	 *  \param[in]  dwBufSize			缓冲区大小
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_TransPTZ_WithoutPreview(LONG lUserID,LONG lChannel,const char *pPTZCodeBuf,DWORD dwBufSize);

	/**
	 *	\eng
	 *  \brief 		Send preset command to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwPTZPresetCmd      PTZ preset command
                                        \li PTZ_SET_PRESET = 8 - Set preset
                                        \li PTZ_CLE_PRESET = 9 - Clear preset
                                        \li PTZ_GOTO_PRESET = 39 - Goto preset
	 *  \param[in]  dwPresetIndex		PTZ preset index
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送预置点操作命令到PTZ设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwPTZPresetCmd      PTZ预置点命令
                                        \li PTZ_SET_PRESET = 8 - 设置预置点
                                        \li PTZ_CLE_PRESET = 9 - 清除预置点
                                        \li PTZ_GOTO_PRESET = 39 - 转到预置点
	 *  \param[in]  dwPresetIndex		PTZ预置点索引
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZPreset(LONG lRealHandle,DWORD dwPTZPresetCmd,DWORD dwPresetIndex);

	/**
	 *	\eng
	 *  \brief 		Send preset command to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwPTZPresetCmd      PTZ preset command
                                        \li PTZ_SET_PRESET = 8 - Set preset
                                        \li PTZ_CLE_PRESET = 9 - Clear preset
                                        \li PTZ_GOTO_PRESET = 39 - Goto preset
	 *  \param[in]  dwPresetIndex		PTZ preset index
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送预置点操作命令到PTZ设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwPTZPresetCmd      PTZ预置点命令
                                        \li PTZ_SET_PRESET = 8 - 设置预置点
                                        \li PTZ_CLE_PRESET = 9 - 清除预置点
                                        \li PTZ_GOTO_PRESET = 39 - 转到预置点
	 *  \param[in]  dwPresetIndex		PTZ预置点索引
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZPreset_WithoutPreview(LONG lUserID,LONG lChannel ,DWORD dwPTZPresetCmd,DWORD dwPresetIndex);
    
	/**
	 *	\eng
	 *  \brief 		Send guard point command to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwGuardPointCmd     PTZ guard point command
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送看守位操作命令到PTZ设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwGuardPointCmd     PTZ看守位命令
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZGuardPoint(LONG lRealHandle,DWORD dwGuardPointCmd);
	
	/**
	 *	\eng
	 *  \brief 		Send guard point command to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwGuardPointCmd     PTZ guard point command
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送看守位操作命令到PTZ设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwGuardPointCmd     PTZ看守位命令
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZGuardPoint_WithoutPreview(LONG lUserID,LONG lChannel ,DWORD dwGuardPointCmd);
	
	/**
	 *	\eng
	 *  \brief 		Send cruise command to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwPTZCruiseCmd      PTZ preset command
                                        \li PTZ_FILL_PRE_SEQ = 30   - Add preset to cruise
                                        \li PTZ_SET_SEQ_DWELL = 31  - Set preset dwell time
                                        \li PTZ_SET_SEQ_SPEED = 32  - Set cruise speed
                                        \li PTZ_CLE_PRE_SEQ = 33    - Remove preset from cruise
                                        \li RUN_SEQ = 37            - Start cruise
                                        \li STOP_SEQ = 38           - Stop cruise
	 *  \param[in]  byCruiseRoute		PTZ cruise route index
	 *  \param[in]  byCruisePoint		PTZ cruise point index
	 *  \param[in]  wInput      		PTZ cruise input parameter
                                        \li When command is FILL_PRE_SEQ, indicates preset index(max is 128)
                                        \li When command is CLE_PRE_SEQ, indicates preset index(max is 128)
                                        \li When command is SET_SEQ_SPEED, indicates speed(max is 15)
                                        \li When command is SET_SEQ_DWELL, indicates time in seconds(max is 255)
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送巡航操作命令到PTZ设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwPTZCruiseCmd      PTZ预置点命令
                                        \li PTZ_FILL_PRE_SEQ = 30   - 将预置点加入巡航序列
                                        \li PTZ_SET_SEQ_DWELL = 31  - 设置巡航点停顿时间
                                        \li PTZ_SET_SEQ_SPEED = 32  - 设置巡航速度
                                        \li PTZ_CLE_PRE_SEQ = 33    - 将预置点从巡航序列中删除
                                        \li RUN_SEQ = 37            - 开始巡航 
                                        \li STOP_SEQ = 38           - 停止巡航
	 *  \param[in]  byCruiseRoute		PTZ巡航路径索引
	 *  \param[in]  byCruisePoint		PTZ巡航点索引
	 *  \param[in]  wInput      		PTZ巡航输入参数
                                        \li 命令为 FILL_PRE_SEQ 时,表示预置点(最大128)
                                        \li 命令为 CLE_PRE_SEQ 时,表示预置点(最大128)
                                        \li 命令为 SET_SEQ_SPEED 时,表示速度(最大15)
                                        \li 命令为 SET_SEQ_DWELL 时,表示时间(最大255秒)
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZCruise(LONG lRealHandle,DWORD dwPTZCruiseCmd,BYTE byCruiseRoute, BYTE byCruisePoint, WORD wInput);
	
	/**
	 *	\eng
	 *  \brief 		Send cruise command to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwPTZCruiseCmd      PTZ preset command
                                        \li PTZ_FILL_PRE_SEQ = 30   - Add preset to cruise
                                        \li PTZ_SET_SEQ_DWELL = 31  - Set preset dwell time
                                        \li PTZ_SET_SEQ_SPEED = 32  - Set cruise speed
                                        \li PTZ_CLE_PRE_SEQ = 33    - Remove preset from cruise
                                        \li RUN_SEQ = 37            - Start cruise
                                        \li STOP_SEQ = 38           - Stop cruise
	 *  \param[in]  byCruiseRoute		PTZ cruise route index
	 *  \param[in]  byCruisePoint		PTZ cruise point index
	 *  \param[in]  wInput      		PTZ cruise input parameter
                                        \li When command is FILL_PRE_SEQ, indicates preset index(max is 128)
                                        \li When command is CLE_PRE_SEQ, indicates preset index(max is 128)
                                        \li When command is SET_SEQ_SPEED, indicates speed(max is 15)
                                        \li When command is SET_SEQ_DWELL, indicates time in seconds(max is 255)
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送巡航操作命令到PTZ设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwPTZCruiseCmd      PTZ预置点命令
                                        \li PTZ_FILL_PRE_SEQ = 30   - 将预置点加入巡航序列
                                        \li PTZ_SET_SEQ_DWELL = 31  - 设置巡航点停顿时间
                                        \li PTZ_SET_SEQ_SPEED = 32  - 设置巡航速度
                                        \li PTZ_CLE_PRE_SEQ = 33    - 将预置点从巡航序列中删除
                                        \li RUN_SEQ = 37            - 开始巡航 
                                        \li STOP_SEQ = 38           - 停止巡航
	 *  \param[in]  byCruiseRoute		PTZ巡航路径索引
	 *  \param[in]  byCruisePoint		PTZ巡航点索引
	 *  \param[in]  wInput      		PTZ巡航输入参数
                                        \li 命令为 FILL_PRE_SEQ 时,表示预置点(最大128)
                                        \li 命令为 CLE_PRE_SEQ 时,表示预置点(最大128)
                                        \li 命令为 SET_SEQ_SPEED 时,表示速度(最大15)
                                        \li 命令为 SET_SEQ_DWELL 时,表示时间(最大255秒)
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZCruise_WithoutPreview(LONG lUserID,LONG lChannel ,DWORD dwPTZCruiseCmd,BYTE byCruiseRoute, BYTE byCruisePoint, WORD wInput);
	
	/**
	 *	\eng
	 *  \brief 		Send track command to PTZ, this must be called after preview is started.
	 *  \param[in]  lRealHandle			Handle for real time preview returned by ::NET_API_RealPlay
	 *  \param[in]  dwPTZTrackCmd       PTZ track command
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送轨迹操作命令到PTZ设备, 必须在启动预览之后调用
	 *  \param[in]  lRealHandle			::NET_API_RealPlay 函数返回的实时播放句柄
	 *  \param[in]  dwPTZTrackCmd       PTZ轨迹命令
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PTZTrack(LONG lRealHandle,DWORD dwPTZTrackCmd);
	
	/**
	 *	\eng
	 *  \brief 		Send track command to PTZ, don't need preview..
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel		Channel number
	 *  \param[in]  dwPTZTrackCmd       PTZ track command
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送轨迹操作命令到PTZ设备, 不需要预览图像
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel		 通道号
	 *  \param[in]  dwPTZTrackCmd       PTZ轨迹命令
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 *  \see enumPTZCommands
	 */

	NET_API BOOL CAL_CALL NET_API_PTZTrack_WithoutPreview(LONG lUserID,LONG lChannel ,DWORD dwPTZTrackCmd);
    //@}
	/** 
	 *	\eng
	 *  \defgroup playback Search / Play files
	 *  Search or play record file from device. 
	 *	\else
	 *  \defgroup playback 录像检索/回放
	 *  检索/回放设备录像文件的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Search for record file on device.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number to be searched
	 *  \param[in]  dwFileType			Search type , see ::enumRecordTypes
	 *  \param[in]  lpStartTime			Start time for search
	 *  \param[in]  lpStopTime			Stop time for search
	 *  \return 	Success: Handle for device record file search,			Fail: -1
	 *	\else
	 *  \brief		查找设备上的录像文件
	 *  \param[in]  lUserID				::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel			等待查询的通道号
	 *  \param[in]  dwFileType			查询类型, 见 ::enumRecordTypes
	 *  \param[in]  lpStartTime			需查询的起始时间
	 *  \param[in]  lpStopTime			需查询的终止时间
	 *  \return		成功: 录像文件查询句柄,				失败: -1
	 *	\endif
	 *	\see enumRecordTypes
	 */
	NET_API LONG CAL_CALL NET_API_FindFile(LONG lUserID,LONG lChannel,DWORD dwFileType, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime);
	/**
	 *	\eng
	 *  \brief 		Search for record file on device with callback.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number to be searched
	 *  \param[in]  dwFileType			Search type , see ::enumRecordTypes
	 *  \param[in]  lpStartTime			Start time for search
	 *  \param[in]  lpStopTime			Stop time for search
	 *  \param[in]  fCallback			Callback for search
	 *  \param[in]  dwUser				User data for callback
	 *  \return 	Success: Handle for device record file search,			Fail: -1
	 *	\else
	 *  \brief		回调方式查找设备上的录像文件
	 *  \param[in]  lUserID				::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel			等待查询的通道号
	 *  \param[in]  dwFileType			查询类型, 见 ::enumRecordTypes
	 *  \param[in]  lpStartTime			需查询的起始时间
	 *  \param[in]  lpStopTime			需查询的终止时间
	 *  \param[in]  fCallback			查询录像的回调函数
	 *  \param[in]  dwUser				回调函数中的用户参数
	 *  \return		成功: 录像文件查询句柄,				失败: -1
	 *	\endif
	 *	\see enumRecordTypes
	 */
	NET_API LONG CAL_CALL NET_API_FindFileWithCallback(LONG lUserID,LONG lChannel,DWORD dwFileType, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime, FindFileCallback fCallback, DWORD dwUser);

	/**
	 *	\eng
	 *  \brief 		Get next file information.
	 *  \param[in]  lFindHandle			Handle for device record file search, returned by ::NET_API_FindFile
	 *  \param[out] lpFindData			Output file information
	 *  \return 	\li NET_API_FILE_SUCCESS 	--	Get file info successful
					\li NET_API_FILE_NOFIND 	--	No file is found
					\li NET_API_ISFINDING		--	Searching
					\li NET_API_NOMOREFILE		--	No more file
					\li NET_API_FILE_EXCEPTION	--	Exception in searching
	 *	\else
	 *  \brief		获取下一个文件信息
	 *  \param[in]  lFindHandle			::NET_API_FindFile 函数返回的录像文件查询句柄
	 *  \param[out] lpFindData			返回的文件信息
	 *  \return		
					\li NET_API_FILE_SUCCESS    -- 获得文件信息
					\li NET_API_FILE_NOFIND     -- 没有文件
					\li NET_API_ISFINDING       -- 正在查找
					\li NET_API_NOMOREFILE      -- 查找文件时没有更多的文件
					\li NET_API_FILE_EXCEPTION  -- 查找文件时异常
	 *	\endif
	 *	\see enumReturnValuesForFinding
	 */
    NET_API LONG CAL_CALL NET_API_FindNextFile(LONG lFindHandle,LPNET_API_FIND_DATA lpFindData);
	/**
	 *	\eng
	 *  \brief 		Release resources for search.
	 *  \param[in]  lFindHandle			Handle for device record file search, returned by ::NET_API_FindFile
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放查找文件时使用的资源
	 *  \param[in]  lFindHandle			::NET_API_FindFile 函数返回的录像文件查询句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_FindClose(LONG lFindHandle);
	/**
	 *	\eng
	 *  \brief 		Release resources for search by callback.
	 *  \param[in]  lFindHandle			Handle for device record file search, returned by ::NET_API_FindFileWithCallback
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放回调方式查找文件时使用的资源
	 *  \param[in]  lFindHandle			::NET_API_FindFileWithCallback 函数返回的录像文件查询句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_FindCloseWithCallback(LONG lFindHandle);
	/**
	 *	\eng
	 *  \brief 		Play record file by file name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sPlayBackFileName	The name of remote record file, returned by ::NET_API_FindNextFile
	 *  \param[in]  hWnd		        The handle of playback window, can be NULL
	 *  \return 	Success: Playback handle,		Fail: -1
	 *	\else
	 *  \brief		按名称播放设备上的录像
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sPlayBackFileName	::NET_API_FindNextFile 返回的远程录像文件名称
	 *  \param[in]  hWnd		        播放录像文件的窗口句柄, 可以为NULL
	 *  \return 	成功: 播放句柄, 		            失败: -1
	 *	\endif
     *  \see        NET_API_SetPlayDataCallBack
	 */
	NET_API LONG CAL_CALL NET_API_PlayBackByName(LONG lUserID,const char *sPlayBackFileName, HWND hWnd);
    /**
	 *	\eng
	 *  \brief 		Play record file by time and channel
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel	        Channel number
	 *  \param[in]  lpStartTime	        Start time of record file
	 *  \param[in]  lpStopTime	        Stop time of record file
	 *  \param[in]  hWnd		        The handle of playback window, can be NULL
	 *  \return 	Success: Playback handle,		Fail: -1
	 *	\else
	 *  \brief		按时间和通道播放设备上的录像
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel	        通道编号
	 *  \param[in]  lpStartTime	        录像起始时间
	 *  \param[in]  lpStopTime	        录像结束时间
	 *  \param[in]  hWnd		        播放录像文件的窗口句柄, 可以为NULL
	 *  \return 	成功: 播放句柄, 		            失败: -1
	 *	\endif
     *  \see        NET_API_SetPlayDataCallBack
	 */
	NET_API LONG CAL_CALL NET_API_PlayBackByTime(LONG lUserID,LONG lChannel, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime, HWND hWnd);
	/**
	 *	\eng
	 *  \brief 		Control the playback progress
	 *  \param[in]  lPlayHandle			Playback handle returned by NET_API_PlayBackBy* functions
	 *  \param[in]  dwControlCode	    Playback control commands
                                        \li NET_API_PLAYSTART = 1       - Start playback
                                        \li NET_API_PLAYSTOP = 2        - Stop playback
                                        \li NET_API_PLAYPAUSE = 3       - Pause
                                        \li NET_API_PLAYRESTART = 4     - Resume
                                        \li NET_API_PLAYFAST = 5        - Fast
                                        \li NET_API_PLAYSLOW = 6        - Slow
                                        \li NET_API_PLAYNORMAL = 7      - Normal
                                        \li NET_API_PLAYFRAME = 8       - Single Frame
										\li NET_API_PLAYSTARTAUDIO = 9	- Start audio
										\li NET_API_PLAYSTOPAUDIO = 10	- Stop audio
										\li NET_API_PLAYAUDIOVOLUME = 11 - Set audio volume (not working by now)
                                        \li NET_API_PLAYSETPOS = 12     - Set playback position(0-100)
                                        \li NET_API_PLAYGETPOS = 13     - Get playback position(0-100)
                                        \li NET_API_PLAYGETTIME = 14    - Get played time(s)
                                        \li NET_API_PLAYGETFRAME = 15   - Get played frame count
                                        \li NET_API_GETTOTALFRAMES = 16 - Get total frame count
                                        \li NET_API_GETTOTALTIME = 17   - Get total time(s)
										\li NET_API_PLAYGETSPEED = 18	- Get current play speed
										\li NET_API_LASTIFRAME   = 21	- View Last I Frame
										\li NET_API_NEXTIFRAME    = 22	- View Next I Frame
										\li NET_API_GET_AUDIO_STATUS = 23 - Check if audio enabled
	 *  \param[in]  dwInValue	        Input value for SET commands
	 *  \param[out] lpOutValue	        Output value for GET commands
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		控制回放过程
	 *  \param[in]  lPlayHandle			NET_API_PlayBackBy* 函数返回的播放句柄
	 *  \param[in]  dwControlCode	    回放控制命令
                                        \li NET_API_PLAYSTART = 1 - 开始播放
                                        \li NET_API_PLAYSTOP = 2 - 停止播放
                                        \li NET_API_PLAYPAUSE = 3 - 暂停播放
                                        \li NET_API_PLAYRESTART = 4 - 恢复播放
                                        \li NET_API_PLAYFAST = 5 - 快放
                                        \li NET_API_PLAYSLOW = 6 - 慢放
                                        \li NET_API_PLAYNORMAL = 7 - 正常速度
                                        \li NET_API_PLAYFRAME = 8 - 单帧放
										\li NET_API_PLAYSTARTAUDIO = 9	- 启动音频
										\li NET_API_PLAYSTOPAUDIO = 10	- 停止音频
										\li NET_API_PLAYAUDIOVOLUME = 11 - 设置音频音量(目前无效)
                                        \li NET_API_PLAYSETPOS = 12 - 改变文件回放的进度(0-100)
                                        \li NET_API_PLAYGETPOS = 13 - 获取文件回放的进度(0-100)
                                        \li NET_API_PLAYGETTIME = 14 - 获取当前已经播放的时间(s)
                                        \li NET_API_PLAYGETFRAME = 15 - 获取当前已经播放的帧数
                                        \li NET_API_GETTOTALFRAMES = 16 - 获取当前播放文件总帧数
										\li NET_API_GETTOTALTIME = 17 - 获取当前播放文件总的时间(s)
										\li NET_API_PLAYGETSPEED = 18	- 获取当前播放速度
										\li NET_API_LASTIFRAME   = 21	- 查看上一个I帧
										\li NET_API_NEXTIFRAME    = 22	- 查看下一个I帧
										\li NET_API_GET_AUDIO_STATUS = 23 - 返回音频播放状态
	 *  \param[in]  dwInValue	        以上SET命令的参数
	 *  \param[out] lpOutValue	        以上GET命令返回值
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        NET_API_PlayBackByName, NET_API_PlayBackByTime
	 *  \see		enumPlaybackControlCommands
	 */
	NET_API BOOL CAL_CALL NET_API_PlayBackControl(LONG lPlayHandle,DWORD dwControlCode,DWORD dwInValue,DWORD *lpOutValue);
	/**
	 *	\eng
	 *  \brief 		Set playback data callback function
	 *  \param[in]  lPlayHandle			Playback handle returned by NET_API_PlayBackBy* functions
	 *  \param[in]  PlayDataCallBack	Playback data callback function pointer
	 *  \param[in]  dwUser	            User parameter that will be passed to callback
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置文件回放数据回调函数
	 *  \param[in]  lPlayHandle			NET_API_PlayBackBy* 函数返回的播放句柄
	 *  \param[in]  PlayDataCallBack	回放数据回调函数指针
	 *  \param[in]  dwUser	            传给回调函数的用户参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        NET_API_PlayBackByName, NET_API_PlayBackByTime
     *  \see        PlayDataCallBack
	 */
	NET_API BOOL CAL_CALL NET_API_SetPlayDataCallBack(LONG lPlayHandle, PlayDataCallBack fPlayDataCallBack,DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Stop playback progress
	 *  \param[in]  lPlayHandle			Playback handle returned by NET_API_PlayBackBy* functions
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		停止回放过程
	 *  \param[in]  lPlayHandle			NET_API_PlayBackBy* 函数返回的播放句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     *  \see        NET_API_PlayBackByName, NET_API_PlayBackByTime
	 */
	NET_API BOOL CAL_CALL NET_API_StopPlayBack(LONG lPlayHandle);
    //@}
	/** 
	 *	\eng
	 *  \defgroup download Download record file
	 *  Download record file from device. 
	 *	\else
	 *  \defgroup download 录像下载
	 *  下载设备录像文件的接口函数. 
	 *	\endif
	 */
    //@{
    /**
	 *	\eng
	 *  \brief 		Start download recorded file from device by file name
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sRemoteFile		    Remoter file name, returned by NET_API_FindNextFile
	 *  \param[in]  sLocalFile		    Locale file name to be saved
	 *  \return 	Success: Download file handle, 		Fail: -1
	 *	\else
	 *  \brief		按名称下载设备上录制的文件
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sRemoteFile		    NET_API_FindNextFile返回的录像文件名
	 *  \param[in]  sLocalFile		    本地保存的文件名称
	 *  \return 	成功: 文件下载句柄, 			失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_GetFileByName(LONG lUserID, const char *sRemoteFile, const char* sLocalFile);
	
	/**
	 *	\eng
	 *  \brief 		Get download file progress
	 *  \param[in]  lHandle	    Download file handle returned by ::NET_API_GetFileByName
	 *  \return 	Success: Percentage(0-100),	Fail: -1
	 *	\else
	 *  \brief		获取设备文件下载进度
	 *  \param[in]  lHandle	    ::NET_API_GetFileByName 函数返回的文件下载句柄
	 *  \return 	成功:     百分比(0-100), 		失败: -1
	 *	\endif
	 */
	NET_API int CAL_CALL NET_API_GetDownloadPos(LONG lHandle);
	/**
	 *	\eng
	 *  \brief 		Close download file handle
	 *  \param[in]  lHandle	    Download file handle returned by ::NET_API_GetFileByName
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		关闭设备文件下载句柄
	 *  \param[in]  lHandle	    ::NET_API_GetFileByName 函数返回的文件下载句柄
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_StopGetFile(LONG lHandle);
	//@}
	/** 
	 *	\eng
	 *  \defgroup alarm Alarm
	 *  Alarm related interface. 
	 *	\else
	 *  \defgroup alarm 报警
	 *  报警相关的接口函数. 
	 *	\endif
	 */
    //@{
    /**
	 *	\eng
	 *  \brief 		Set device alarm out state
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lAlarmOutPort	    Alarm out port number
	 *  \param[in]  lAlarmOutStatic	    Alarm out state, 1 or 0
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		设置设备报警输出状态
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lAlarmOutPort	    报警输出端口编号
	 *  \param[in]  lAlarmOutStatic	    报警输出状态, 1 或 0
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
     
	NET_API BOOL CAL_CALL NET_API_SetAlarmOut(LONG lUserID,LONG lAlarmOutPort,LONG lAlarmOutStatic);
	/**
	 *	\eng
	 *  \brief 		Get device alarm out state
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[out] lpAlarmOutState	    Alarm out state stucture
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		获取设备报警输出状态
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[out] lpAlarmOutState	    报警输出状态结构体
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_GetAlarmOut(LONG lUserID,LPNET_API_ALARMOUTSTATUS lpAlarmOutState);
    
	/**
	 *	\eng
	 *  \brief 		Start listen alarm information from device at local port
	 *  \param[in]  sLocalIP			On which ip should we listen
	 *  \param[in]  wLocalPort			On which port should we listen
	 *  \param[in]  fMessageCallBack	Message callback function will be called when there is incomming alarm information
	 *  \param[in]  dwUser				User parameter will be passed when calling message callback function
	 *  \return 	Success: listing handle,		Fail: -1
	 *	\else
	 *  \brief		开始监听设备报警
	 *  \param[in]  sLocalIP			监听使用的IP
	 *  \param[in]  wLocalPort			监听使用的端口
	 *  \param[in]  fMessageCallBack	消息回调函数, 在有报警信息的时候, 会被调用
	 *  \param[in]  dwUser				传给消息回调函数的用户参数
	 *  \return 	成功: 监听句柄,	 			失败: -1
	 *	\endif
     *  \see        NET_API_SetDVRMessageCallBack, MessageCallBack
	 */
	NET_API LONG CAL_CALL NET_API_StartListen(const char  *sLocalIP, WORD  wLocalPort, MessageCallBack fMessageCallBack, DWORD dwUser);
	
	/**
	 *	\eng
	 *  \brief 		Stop listen alarm information from device at local port
	 *  \param[in]  lListenHandle	Listen handle returned by ::NET_API_StartListen
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		停止监听设备报警
	 *  \param[in]  lListenHandle	::NET_API_StartListen 函数返回的监听句柄
	 *  \return 	成功: TRUE,			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_StopListen(LONG  lListenHandle);

	/**
	 *	\eng
	 *  \brief 		Setup alarm channel for device, incoming alarm information will be passed to ::MessageCallBack set by ::NET_API_SetDVRMessageCallBack
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \return 	Success: alarm channel handle,		Fail: -1
	 *	\else
	 *  \brief		建立设备报警通道, 来自设备的报警将会传到 ::NET_API_SetDVRMessageCallBack 设置的 ::MessageCallBack 回调函数中
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \return 	成功: 报警通道句柄, 			失败: -1
	 *	\endif
     *  \see        NET_API_SetDVRMessageCallBack, MessageCallBack
	 */
	NET_API LONG CAL_CALL NET_API_SetupAlarmChan(LONG  lUserID);
	/**
	 *	\eng
	 *  \brief 		Close a exist alarm channel for device
	 *  \param[in]  lAlarmHandle	Alarm channel handle returned by ::NET_API_SetupAlarmChan
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		关闭设备报警通道
	 *  \param[in]  lAlarmHandle	::NET_API_SetupAlarmChan 函数返回的报警通道句柄
	 *  \return 	成功: TRUE,			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CloseAlarmChan(LONG  lAlarmHandle);
	
	//@}
	/** 
	 *	\eng
	 *  \defgroup voice Audio/Video communication
	 *  Audio/Video communication related interface. 
	 *	\else
	 *  \defgroup voice 音视频对讲
	 *  音频视频对讲相关的接口函数. 
	 *	\endif
	 */
    //@{
    /**
	 *	\eng
	 *  \brief 		Start a voice communication channel from SDK to device, 
                    using local default sound input device to capture and send voice data
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  fVoiceDataCallBack	Voice data callback function will be called when there is incoming sound data (not working by now)
	 *  \param[in]  dwUser				User parameter will be passed when calling callback function
	 *  \return 	Success: voice communication handle,		Fail: -1
	 *	\else
	 *  \brief		开始SDK至设备的语音对讲, 使用默认本地音频采集设备采集并发送数据
     *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  fVoiceDataCallBack	音频数据回调函数, 在有音频数据的时候, 会被调用(暂时无效)
	 *  \param[in]  dwUser				传给回调函数的用户参数
	 *  \return 	成功: 对讲句柄,	 			失败: -1
	 *	\endif
     *  \see        VoiceDataCallBack
	 */
	NET_API LONG CAL_CALL NET_API_StartVoiceCom(LONG lUserID, VoiceDataCallBack fVoiceDataCallBack, DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Set voice communication volume.
	 *  \param[in]  lVoiceComHandle			Voice communication handle, returned by ::NET_API_StartVoiceCom /::NET_API_StartVoiceCom_MR
	 *  \param[in]  wVolume			        Voice communication volume value
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		设置对讲音量
	 *  \param[in]  lVoiceComHandle			::NET_API_StartVoiceCom /::NET_API_StartVoiceCom_MR 函数返回的对讲句柄
	 *  \param[in]  wVolume			        对讲音量数值
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SetVoiceComClientVolume(LONG lVoiceComHandle,WORD wVolume);
	/**
	 *	\eng
	 *  \brief 		Release resources for voice communication.
	 *  \param[in]  lVoiceComHandle			Voice communication handle, returned by ::NET_API_StartVoiceCom /::NET_API_StartVoiceCom_MR
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放对讲时使用的资源
	 *  \param[in]  lVoiceComHandle			::NET_API_StartVoiceCom /::NET_API_StartVoiceCom_MR 函数返回的对讲句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_StopVoiceCom(LONG lVoiceComHandle);
	
    /**
	 *	\eng
	 *  \brief 		Start a audio/video communication channel from SDK to device.
                    User have to capture and send data by using ::NET_API_VoiceComSendData /::NET_API_VoiceComSendDataEx
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  fVoiceDataCallBack	Voice data callback function will be called when there is incomming sound data
	 *  \param[in]  dwUser				User parameter will be passed when calling callback function
	 *  \return 	Success: voice communication handle,		Fail: -1
	 *	\else
	 *  \brief		开始SDK至设备的音/视频对讲, 用户需要采集并使用 ::NET_API_VoiceComSendData/ ::NET_API_VoiceComSendDataEx 发送数据
     *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  fVoiceDataCallBack	音频数据回调函数, 在有音频数据的时候, 会被调用
	 *  \param[in]  dwUser				传给回调函数的用户参数
	 *  \return 	成功: 对讲句柄,	 			失败: -1
	 *	\endif
     *  \see        VoiceDataCallBack
	 */
	NET_API LONG CAL_CALL NET_API_StartVoiceCom_MR(LONG lUserID, VoiceDataCallBack fVoiceDataCallBack, DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Send voice communication data.
	 *  \param[in]  lVoiceComHandle			Voice communication handle, returned by ::NET_API_StartVoiceCom_MR
	 *  \param[in]  pSendBuf			    G.711u encoded voice data
	 *  \param[in]  dwBufSize			    Voice data size
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送音频数据
	 *  \param[in]  lVoiceComHandle			::NET_API_StartVoiceCom_MR 函数返回的对讲句柄
	 *  \param[in]  pSendBuf			    G.711u 编码的音频数据
	 *  \param[in]  dwBufSize			    音频数据大小
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_VoiceComSendData(LONG lVoiceComHandle, char *pSendBuf, DWORD dwBufSize);
	/**
	 *	\eng
	 *  \brief 		Send audio/video communication data.
	 *  \param[in]  lVoiceComHandle			Voice communication handle, returned by ::NET_API_StartVoiceCom_MR
	 *  \param[in]  pSendBuf			    Encoded audio/video data
	 *  \param[in]  dwBufSize			    Data size
	 *  \param[in]  wFrameType			    Data type
                                            \li CA_I_FRAME = 0x65, H.264 I Frame
                                            \li CA_P_FRAME = 0x41, H.264 P Frame
                                            \li CA_SPS_FRAME = 0x67, H.264 SPS Information
                                            \li CA_PPS_FRAME = 0x68, H.264 PPS Information
                                            \li CA_AUDIO_FRAME = 0x10, G.711 ulaw audio frame
                                            \li CA_END_OF_FRAME	= 0x8000, End of frame flag, can be combined with other flags
	 *  \param[in]  wCodecType			    Data encoder type
                                            \li CA_CODEC_H264 = 0x00, H.264 baseline video encoder
                                            \li CA_CODEC_G711 = 0x10, G.711 ulaw audio encoder
	 *  \param[in]  wWidth			        Video frame width
	 *  \param[in]  wHeight			        Video frame height
	 *  \param[in]  dwTimestamp			    Data timestamp (ms)
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送音/视频数据
	 *  \param[in]  lVoiceComHandle			::NET_API_StartVoiceCom_MR 函数返回的对讲句柄
	 *  \param[in]  pSendBuf			    编码后的音频视频数据
	 *  \param[in]  dwBufSize			    数据大小
	 *  \param[in]  wFrameType			    数据类型
                                            \li CA_I_FRAME = 0x65, H.264 I 帧
                                            \li CA_P_FRAME = 0x41, H.264 P 帧
                                            \li CA_SPS_FRAME = 0x67, H.264 SPS 信息
                                            \li CA_PPS_FRAME = 0x68, H.264 PPS 信息
                                            \li CA_AUDIO_FRAME = 0x10, G.711 ulaw 音频帧
                                            \li CA_END_OF_FRAME	= 0x8000, 帧结尾标志, 可以与上述标志组合使用
	 *  \param[in]  wCodecType			    数据编码格式
                                            \li CA_CODEC_H264 = 0x00, H.264 baseline 视频编码
                                            \li CA_CODEC_G711 = 0x10, G.711 ulaw 音频编码
	 *  \param[in]  wWidth			        视频帧宽度
	 *  \param[in]  wHeight			        视频帧高度
	 *  \param[in]  dwTimestamp			    时间戳 (毫秒)
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_VoiceComSendDataEx(LONG lVoiceComHandle, char *pSendBuf, DWORD dwBufSize, 
        WORD wFrameType, WORD wCodecType, WORD wWidth, WORD wHeight, DWORD dwTimestamp);

	 /**
	 *	\eng
	 *  \brief 		Get decode capacity of the device.
	 *  \param[in]  lUserID						User ID returned by ::NET_API_Login 
	 *  \param[out] lCount						Number of decode channel supported by device 
	 *  \return 	Success: TRUE,				Fail: FALSE
	 *	\else
	 *  \brief		获取设备解码通道数量
     *  \param[in]  lUserID						::NET_API_Login  函数返回的用户ID
	 *  \param[out] lCount						返回设备支持的解码通道数
	 *  \return 	成功: TRUE					失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Decoder_GetCapacity(LONG lUserID, LONG* lCount);
	 /**
	 *	\eng
	 *  \brief 		Start a decode channel on device.
                    User have to send data by using ::NET_API_Decoder_SendData
	 *  \param[in]  lUserID						User ID returned by ::NET_API_Login 
	 *  \return 	Success: decoder handle,	Fail: -1
	 *	\else
	 *  \brief		打开设备解码通道, 使用 ::NET_API_Decoder_SendData 发送数据
     *  \param[in]  lUserID						::NET_API_Login  函数返回的用户ID
	 *  \return 	成功: 解码句柄,	 			失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_Decoder_Start(LONG lUserID, DWORD dwChannel);
	/**
	 *	\eng
	 *  \brief 		Release resources for decoder channel
	 *  \param[in]  lDecodeHandle			Decoder handle, returned by ::NET_API_Decoder_Start
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放设备解码时使用的资源
	 *  \param[in]  lDecodeHandle			::NET_API_Decoder_Start 函数返回的解码句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_Decoder_Stop(LONG lDecodeHandle);
	/**
	 *	\eng
	 *  \brief 		Send audio/video data to decoder.
	 *  \param[in]  lDecodeHandle			Decoder handle, returned by ::NET_API_Decoder_Start
	 *  \param[in]  pSendBuf			    Encoded audio/video data
	 *  \param[in]  dwBufSize			    Data size
	 *  \param[in]  wFrameType			    Data type
                                            \li CA_I_FRAME = 0x65, H.264 I Frame
                                            \li CA_P_FRAME = 0x41, H.264 P Frame
                                            \li CA_SPS_FRAME = 0x67, H.264 SPS Information
                                            \li CA_PPS_FRAME = 0x68, H.264 PPS Information
                                            \li CA_AUDIO_FRAME = 0x10, G.711 ulaw audio frame
                                            \li CA_END_OF_FRAME	= 0x8000, End of frame flag, can be combined with other flags
	 *  \param[in]  wCodecType			    Data encoder type
                                            \li CA_CODEC_H264 = 0x00, H.264 baseline video encoder
                                            \li CA_CODEC_G711 = 0x10, G.711 ulaw audio encoder
	 *  \param[in]  dwTimestamp			    Data timestamp (ms)
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送音/视频数据到解码设备
	 *  \param[in]  lDecodeHandle			::NET_API_Decoder_Start 函数返回的解码句柄
	 *  \param[in]  pSendBuf			    音频视频数据
	 *  \param[in]  dwBufSize			    数据大小
	 *  \param[in]  wFrameType			    数据类型
                                            \li CA_I_FRAME = 0x65, H.264 I 帧
                                            \li CA_P_FRAME = 0x41, H.264 P 帧
                                            \li CA_SPS_FRAME = 0x67, H.264 SPS 信息
                                            \li CA_PPS_FRAME = 0x68, H.264 PPS 信息
                                            \li CA_AUDIO_FRAME = 0x10, G.711 ulaw 音频帧
                                            \li CA_END_OF_FRAME	= 0x8000, 帧结尾标志, 可以与上述标志组合使用
	 *  \param[in]  wCodecType			    数据编码格式
                                            \li CA_CODEC_H264 = 0x00, H.264 baseline 视频编码
                                            \li CA_CODEC_G711 = 0x10, G.711 ulaw 音频编码
	 *  \param[in]  dwTimestamp			    时间戳 (毫秒)
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_Decoder_SendData(LONG lDecodeHandle, char *pSendBuf, DWORD dwBufSize, 
		WORD wFrameType, WORD wCodecType, DWORD dwTimestamp);
    //@}
	/** 
	 *	\eng
	 *  \defgroup serial Transparent channel
	 *  Transparent channel related interface. 
	 *	\else
	 *  \defgroup serial 透明通道
	 *  串口透明通道相关的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Start a transparent serial data channel from SDK to device
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lSerialPort			Serial port number on device
                                        \li 1 - RS232
                                        \li 2 - RS485
	 *  \param[in]  fSerialDataCallBack	Serial data callback function will be called when there is incomming data
	 *  \param[in]  dwUser				User parameter will be passed when calling callback function
	 *  \return 	Success: serial data handle,		Fail: -1
	 *	\else
	 *  \brief		开始SDK至设备的串口数据透明传输
     *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
     *  \param[in]  lSerialPort			设备上的串口编号
                                        \li 1 - 232串口
                                        \li 2 - 485串口
	 *  \param[in]  fSerialDataCallBack	串口数据透明传输回调函数, 在有串口数据的时候, 会被调用
	 *  \param[in]  dwUser				传给回调函数的用户参数
	 *  \return 	成功: 串口数据透明传输句柄,	 			失败: -1
	 *	\endif
     *  \see        SerialDataCallBack
	 */    
	NET_API LONG CAL_CALL NET_API_SerialStart(LONG lUserID,LONG lSerialPort,SerialDataCallBack fSerialDataCallBack,DWORD dwUser);
	/**
	 *	\eng
	 *  \brief 		Send transparent serial data to device.
	 *  \param[in]  lSerialHandle			Serial data handle, returned by ::NET_API_SerialStart
	 *  \param[in]  lChannel			    Channel number
	 *  \param[in]  pSendBuf			    Serial data
	 *  \param[in]  dwBufSize			    Data size
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		发送串口数据至设备
	 *  \param[in]  lSerialHandle			::NET_API_SerialStart 函数返回的串口数据透明传输句柄
	 *  \param[in]  lChannel			    通道号
	 *  \param[in]  pSendBuf			    串口数据
	 *  \param[in]  dwBufSize			    数据大小
	 *  \return 	成功: TRUE, 			    失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SerialSend(LONG lSerialHandle, LONG lChannel, const char *pSendBuf,DWORD dwBufSize);
	/**
	 *	\eng
	 *  \brief 		Release resources for transparent serial communication.
	 *  \param[in]  lSerialHandle			Serial data handle, returned by ::NET_API_SerialStart
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放串口数据透明传输时使用的资源
	 *  \param[in]  lSerialHandle			::NET_API_SerialStart 函数返回的串口数据透明传输句柄
	 *  \return 	成功: TRUE, 			    失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_SerialStop(LONG lSerialHandle);
    //@}
	/** 
	 *	\eng
	 *  \defgroup record Manual Recording
	 *  Manual recording on device related interface. 
	 *	\else
	 *  \defgroup record 手动录像
	 *  手动启动设备录像相关的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Start record for one channel on device manually
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \param[in]  lRecordType			Reserved
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		手动启动设备上某个通道的录像
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \param[in]  lRecordType			保留参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_StartDVRRecord(LONG lUserID,LONG lChannel,LONG lRecordType);
	/**
	 *	\eng
	 *  \brief 		Stop record for one channel on device manually
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		手动停止设备上某个通道的录像
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_StopDVRRecord(LONG lUserID,LONG lChannel);
    //@}
	/** 
	 *	\eng
	 *  \defgroup resuming Resuming Feature
	 *  Resuming related interface. 
	 *	\else
	 *  \defgroup resuming 续传功能
	 *  续传相关的接口函数. 
	 *	\endif
	 */
    //@{
    /**
	 *	\eng
	 *  \brief 	Enable Resuming Feature on one channel
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		启用断点续传功能
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_EnableResumingFeature(LONG lUserID,LONG lChannel);
    
	/**
	 *	\eng
	 *  \brief 	Disable Resuming Feature on one channel
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		关闭断点续传功能
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_DisableResumingFeature(LONG lUserID,LONG lChannel);

    /**
	 *	\eng
	 *  \brief 		Reset record file resuming flag by time
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel            Channel
	 *  \param[in]  lpStartTime         Start time
	 *  \param[in]  lpStopTime  		Stop time
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		按时间段重置录像文件续传标记
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel            通道号
	 *  \param[in]  lpStartTime         起始时间
	 *  \param[in]  lpStopTime          结束时间
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */	 
	NET_API BOOL CAL_CALL NET_API_ResetFileResumingFlagByTime(LONG lUserID, LONG lChannel, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime);
	//@}
	/** 
	 *	\eng
	 *  \defgroup capture Capture picture
	 *  Jpeg capture related interface. 
	 *	\else
	 *  \defgroup capture 抓图
	 *  抓图相关的接口函数. 
	 *	\endif
	 */
    //@{
    /**
	 *	\eng
	 *  \brief 		Capture jpeg picture from device to local file
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \param[in]  lpJpegPara			Jpeg capture parameter
	 *  \param[in]  sPicFileName		Local captured file name
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		从设备抓取JPEG图片到本地文件
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \param[in]  lpJpegPara			Jpeg抓图参数
	 *  \param[in]  sPicFileName		本地保存的文件名
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_CaptureJPEGPicture(LONG lUserID, LONG lChannel, LPNET_API_JPEGPARA lpJpegPara, const char *sPicFileName);
	 /**
	 *	\eng
	 *  \brief 		Capture jpeg picture from device to buffer
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number
	 *  \param[in]  lpJpegPara			Jpeg capture parameter
	 *  \param[in]  dwBufferSize		Output buffer size
	 *  \param[out] pdwJpegSize		    Pointer to a DWORD which will be used to store actual writen size
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		从设备抓取JPEG图片到缓冲区
	 *  \param[in]  lUserID			    ::NET_API_Login  函数返回的用户ID
	 *  \param[in]  lChannel			通道号
	 *  \param[in]  lpJpegPara			Jpeg抓图参数
	 *  \param[in]  dwBufferSize		上述缓冲区大小
	 *  \param[out] pdwJpegSize		    指向用于保存实际写入JPEG数据大小的DWORD的指针
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_CaptureJPEGPictureToBuffer(LONG lUserID, LONG lChannel, LPNET_API_JPEGPARA lpJpegPara, char* pOutBuffer, DWORD dwBufferSize, DWORD* pJpegSize);
	/**
	 *	\eng
	 *  \brief 		Search for captured jpeg file on device.
	 *  \param[in]  lUserID				User ID returned by ::NET_API_Login 
	 *  \param[in]  lChannel			Channel number to be searched
	 *  \param[in]  dwFileType			Search type 
     *                                   \li 0 - recorded by timer
     *                                   \li 1 - recorded by motion dectect
     *                                   \li 2 - recorded by sensor
     *                                   \li 3 - recorded by sensor or motion
     *                                   \li 4 - recorded by sensor and motion
     *                                   \li 5 - recorded by command
     *                                   \li 6 - recorded by manual
     *                                   \li 0xff - all type
	 *  \param[in]  lpStartTime			Start time for search
	 *  \param[in]  lpStopTime			Stop time for search
	 *  \return 	Success: Handle for device jpeg file search,			Fail: -1
	 *	\else
	 *  \brief		查找设备上的JPEG图片
	 *  \param[in]  lUserID				::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lChannel			等待查询的通道号
	 *  \param[in]  dwFileType			查询类型 
     *                                   \li 0 - 定时触发
     *                                   \li 1 - 移动侦测触发
     *                                   \li 2 - 外接传感器触发
     *                                   \li 3 - 移动侦测或者外接传感器触发
     *                                   \li 4 - 移动侦测和外接传感器同时触发
     *                                   \li 5 - 命令触发
     *                                   \li 6 - 手动触发
     *                                   \li 0xff - 所有类型
	 *  \param[in]  lpStartTime			需查询的起始时间
	 *  \param[in]  lpStopTime			需查询的终止时间
	 *  \return		成功: JPEG查询句柄,				失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_FindJpeg(LONG lUserID,LONG lChannel,DWORD dwFileType, LPNET_API_TIME lpStartTime, LPNET_API_TIME lpStopTime);
    
    /**
	 *	\eng
	 *  \brief 		Get next jpeg file information.
	 *  \param[in]  lFindHandle			Handle for device jpeg file search, returned by ::NET_API_FindJpeg
	 *  \param[out] lpFindData			Output file information
	 *  \return 	\li NET_API_FILE_SUCCESS 	--	Get file info successful
					\li NET_API_FILE_NOFIND 	--	No file is found
					\li NET_API_ISFINDING		--	Searching
					\li NET_API_NOMOREFILE		--	No more file
					\li NET_API_FILE_EXCEPTION	--	Exception in searching
	 *	\else
	 *  \brief		获取下一个文件信息
	 *  \param[in]  lFindHandle			::NET_API_FindJpeg 函数返回的JPEG查询句柄
	 *  \param[out] lpFindData			返回的文件信息
	 *  \return		
					\li NET_API_FILE_SUCCESS    -- 获得文件信息
					\li NET_API_FILE_NOFIND     -- 没有文件
					\li NET_API_ISFINDING       -- 正在查找
					\li NET_API_NOMOREFILE      -- 查找文件时没有更多的文件
					\li NET_API_FILE_EXCEPTION  -- 查找文件时异常
	 *	\endif
	 *	\see enumReturnValuesForFinding
	 */
    NET_API LONG CAL_CALL NET_API_FindNextJpeg(LONG lFindHandle,LPNET_API_FIND_JPG_DATA lpFindData);
    
    /**
	 *	\eng
	 *  \brief 		Release resources for search.
	 *  \param[in]  lFindHandle			Handle for device jpeg file search, returned by ::NET_API_FindJpeg
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		释放查找文件时使用的资源
	 *  \param[in]  lFindHandle			::NET_API_FindJpeg 函数返回的JPEG查询句柄
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_FindJpegClose(LONG lFindHandle);
    
    /**
	 *	\eng
	 *  \brief 		Get device jpeg file to buffer
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sRemoteFileName		The name of remote jpeg file, returned by ::NET_API_FindNextJpeg
	 *  \param[out] pOutBuffer		    Output buffer pointer
	 *  \param[in]  dwBufferSize		Output buffer size
	 *  \param[out] pdwJpegSize		    Pointer to a DWORD which will be used to store actual writen size
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		获取设备 JPEG文件到缓冲区
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sRemoteFileName		::NET_API_FindNextJpeg 返回的远程JPEG文件名称
	 *  \param[out] pOutBuffer		    用于保存JPEG的缓冲区
	 *  \param[in]  dwBufferSize		上述缓冲区大小
	 *  \param[out] pdwJpegSize		    指向用于保存实际写入JPEG数据大小的DWORD的指针
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_GetJpegToBuffer(LONG lUserID,const char *sRemoteFileName, char* pOutBuffer, DWORD dwBufferSize, DWORD* pdwJpegSize);
    /**
	 *	\eng
	 *  \brief 		Get Device jpeg file to local file
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sRemoteFileName		The name of remote jpeg file, returned by ::NET_API_FindNextJpeg
	 *  \param[in]  sLocalFileName		The name of local file
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		获取设备 JPEG文件到本地文件
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sRemoteFileName		::NET_API_FindNextJpeg 返回的远程JPEG文件名称
	 *  \param[in]  sLocalFileName		保存到本地的JPEG文件名
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
    NET_API BOOL CAL_CALL NET_API_GetJpegToFile(LONG lUserID,const char *sRemoteFileName, const char *sLocalFileName);
	/**
	 *	\eng
	 *  \brief 		Clear jpeg file downloaded flag
	 *  \param[in]  lUserID			    User ID returned by ::NET_API_Login 
	 *  \param[in]  sRemoteFileName		The name of remote jpeg file, returned by ::NET_API_FindNextJpeg
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		清除设备JPEG文件下载标记
	 *  \param[in]  lUserID			    ::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sRemoteFileName		::NET_API_FindNextJpeg 返回的远程JPEG文件名称
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_ClearJpegDownloadFlag(LONG lUserID, const char *sRemoteFileName);
    //@}
	/** 
	 *	\eng
	 *  \defgroup system System Operation
	 *  System operation related interface. 
	 *	\else
	 *  \defgroup system 系统操作
	 *  系统相关的接口函数. 
	 *	\endif
	 */
    //@{
	/**
	 *	\eng
	 *  \brief 		Reboot Device
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		重新启动设备
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_RebootDVR(LONG lUserID);
	/**
	 *	\eng
	 *  \brief 		Shut down Device
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		关闭设备电源
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_ShutDownDVR(LONG lUserID);
	/**
	 *	\eng
	 *  \brief 		Upgrade Device MCU Firmware
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sFileName		the name of local Firmware upgrade file
	 *  \return 	Success: TRUE,	Fail: FALSE
	 *	\else
	 *  \brief		更新设备MCU固件
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sFileName		本地的更新文件名
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_UpgradeMCU(LONG lUserID,const char *sFileName);
	/**
	 *	\eng
	 *  \brief 		Upgrade Device Firmware
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sFileName		the name of local Firmware upgrade file
	 *  \return 	Success: Upgrade handle,	Fail: -1
	 *	\else
	 *  \brief		更新设备固件
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sFileName		本地的更新文件名
	 *  \return 	成功: 更新句柄, 			失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_Upgrade(LONG lUserID,const char *sFileName);
	/**
	 *	\eng
	 *  \brief 		Get firmware upgrade state
	 *  \param[in]  lUpgradeHandle	    Upgrade handle returned by ::NET_API_Upgrade
	 *  \return 	Success: percentage<<16 | state,	Fail: -1
	 *  				\li -1：Error
	 *  				\li 1：Upgrade succeed
	 *  				\li 2：Upgrading
	 *  				\li 3：Upgrade failed
	 *  				\li 4：Unknown state
	 *	\else
	 *  \brief		获取更新固件的状态
	 *  \param[in]  lUpgradeHandle	    ::NET_API_Upgrade 函数返回的升级句柄
	 *  \return 	成功: 更新进度<<16 | 状态值, 		失败: -1
	 *  				\li -1：出现错误 
	 *  				\li 1：升级成功
	 *  				\li 2：正在升级
	 *  				\li 3：升级失败
	 *  				\li 4：网络断开,状态未知
	 *	\endif
	 */
	NET_API int CAL_CALL NET_API_GetUpgradeState(LONG lUpgradeHandle);
	/**
	 *	\eng
	 *  \brief 		Close Firmware upgrade handle
	 *  \param[in]  lUpgradeHandle		Upgrade handle returned by ::NET_API_Upgrade
	 *  \return 	Success: TRUE,		Fail: FALSE
	 *	\else
	 *  \brief		关闭固件升级句柄
	 *  \param[in]  lUpgradeHandle	    ::NET_API_Upgrade 函数返回的升级句柄
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CloseUpgradeHandle(LONG lUpgradeHandle);

	/**
	 *	\eng
	 *  \brief 		Format disk on device
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  lDiskNumber		Disk number on device, NET_API_WORKSTATE.NET_API_DISKSTATE.byDiskID
	 *  \return 	Success: Format handle,	Fail: -1
	 *	\else
	 *  \brief		格式化设备上的硬盘
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  lDiskNumber		设备上硬盘的序号, NET_API_WORKSTATE.NET_API_DISKSTATE.byDiskID
	 *  \return 	成功: 格式化句柄, 			失败: -1
	 *	\endif
	 */
	NET_API LONG CAL_CALL NET_API_FormatDisk(LONG lUserID,LONG lDiskNumber);
	/**
	 *	\eng
	 *  \brief 		Get format progress on device
	 *  \param[in]  lFormatHandle			Format handle returned by ::NET_API_FormatDisk
	 *  \param[out] pCurrentFormatDisk		Disk number current is formating
	 *  \param[out] pCurrentDiskPos		    Current format percentage
	 *  \param[out] pFormatStatic		    Disk fomate state
                                            \li 0 Formating
                                            \li 1 Format complete
                                            \li 2 Format current disk error
                                            \li 3 Unknow state, offline
	 *  \return 	Success: TRUE,	        Fail: FALSE
	 *	\else
	 *  \brief		获取设备上硬盘格式化的进度
	 *  \param[in]  lFormatHandle			::NET_API_FormatDisk 函数返回的格式化句柄
	 *  \param[out] pCurrentFormatDisk		当前格式化的硬盘编号
	 *  \param[out] pCurrentDiskPos		    当前格式化的进度
	 *  \param[out] pFormatStatic		    格式化状态
                                            \li 0 表示正在格式化
                                            \li 1 表示硬盘全部格式化完成
                                            \li 2 表示格式化当前硬盘出错 
                                            \li 3 表示由于网络异常, 状态未知
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetFormatProgress(LONG lFormatHandle,LONG *pCurrentFormatDisk,LONG *pCurrentDiskPos,LONG *pFormatStatic);
	/**
	 *	\eng
	 *  \brief 		Close format session
	 *  \param[in]  lFormatHandle			Format handle returned by ::NET_API_FormatDisk
	 *  \return 	Success: TRUE,	        Fail: FALSE
	 *	\else
	 *  \brief		关闭格式化会话
	 *  \param[in]  lFormatHandle			::NET_API_FormatDisk 函数返回的格式化句柄
	 *  \return 	成功: TRUE, 		失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_CloseFormatHandle(LONG lFormatHandle);

	/**
	 *	\eng
	 *  \brief 		Upload device license file
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sFileName		license file name
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		上传设备授权文件
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sFileName		授权文件名
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_UploadDVRLicenseFile(LONG lUserID,const char *sFileName);

	/**
	 *	\eng
	 *  \brief 		Get device license information
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sInfoBuffer		buffer will contain license information, must be larger than 1024
	 *  \param[in]  dwBufferSize	buffer size, must be larger than 1024
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		获取设备授权信息
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sInfoBuffer		用于保存授权信息的缓冲区,必须大于1024字节
	 *  \param[in]  dwBufferSize	授权信息缓冲区大小, 必须大于1024字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetDVRLicenseInformation(LONG lUserID, char *sInfoBuffer, DWORD dwBufferSize);

	/**
	 *	\eng
	 *  \brief 		Get device Version string
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  sInfoBuffer		buffer will contain DVR Version string, must be larger than 256
	 *  \param[in]  dwBufferSize	buffer size, must be larger than 256
	 *  \return 	Success: TRUE, 			Fail: FALSE
	 *	\else
	 *  \brief		获取设备版本信息的字符串
	 *  \param[in]  lUserID			::NET_API_Login 函数返回的用户ID值
	 *  \param[in]  sInfoBuffer		用于保存版本信息的字符串缓冲区,必须大于256字节
	 *  \param[in]  dwBufferSize	版本信息的字符串缓冲区大小, 必须大于256字节
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
	 */
	NET_API BOOL CAL_CALL NET_API_GetDVRVersionString(LONG lUserID, char *sInfoBuffer, DWORD dwBufferSize);
    
	enum enumDISCOVERY_CMDs {
		DC_get_dev_info_req = 101,
		DC_get_dev_info_resp = 102,
		
		DC_sync_time_req = 103,
		DC_sync_time_resp = 104,

		DC_get_dev_info_resp_v2 = 106,

		DC_set_dev_ip_req = 107,
		DC_set_dev_ip_resp = 108
	};
    #pragma pack(push, 1)
	struct device_cmd{
        unsigned char cmd;
        NET_API_DEVICECFG device_info;
        NET_API_TIME_ZONE time_zone;
        char version_string[64];
        char ip[16];
        unsigned short port;
    };
	struct device_cmd_v2{
		unsigned char cmd;
		NET_API_DEVICECFG device_info;
		NET_API_TIME_ZONE time_zone;
		char version_string[64];
		char ip[16];
		unsigned short port;
		ULONG dev_id;
	};
	#pragma pack(pop)
	
    typedef void (CALLBACK *device_enum_callback)(unsigned char cmd, void* out, ULONG len, DWORD user);
    
    NET_API LONG CAL_CALL NET_API_FindAvailableDeviceStart(device_enum_callback cb, DWORD dwUser);
    NET_API BOOL CAL_CALL NET_API_SynchronizeTime(LONG lFindHandle, DWORD dev_id, NET_API_TIME_ZONE* time_zone);
	NET_API BOOL CAL_CALL NET_API_SetAvailableDeviceIP(LONG lFindHandle, DWORD dev_id, const char* dev_ip, const char* password, NET_API_ETHERNET* eth, WORD port);
    NET_API BOOL CAL_CALL NET_API_FindAvailableDeviceStop(LONG lFindHandle);

	NET_API BOOL CAL_CALL NET_API_JsonCommand(LONG lUserID, const char* sInput, DWORD dwInputSize, char* sOutValue, DWORD dwBufferSize);
	NET_API BOOL CAL_CALL NET_API_ValidateJpegWatermark(const char* filename);
	NET_API BOOL CAL_CALL NET_API_ValidateFLVWatermark(const char* filename);

	/**
	 *	\eng
	 *  \brief 		Test API
	 *  \param[in]  lUserID			User ID returned by ::NET_API_Login 
	 *  \param[in]  nParam			Input param < 1024
	 *  \param[out] pOut			Output param
	 *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		测试API
	 *  \param[in]  lUserID			::NET_API_Login  函数返回的用户ID
	 *  \param[in]  nParam			输入参数 < 1024
	 *  \param[out] pOut			输出参数
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_Test( LONG lUserID, int nParam, int* pOut );

	/**
	 *	\eng
	 *  \brief 		Convert mov to avi
	 *  \param[in]  source_file			source file
	 *  \param[in]  des_file			Destination file
	 *  \param[in]  dwBitrate			1 <= bitrate <= 20480 (kbps)
     *  \return 	Success: TRUE, 		Fail: FALSE
	 *	\else
	 *  \brief		转换 MOV到 AVI，AVC将转码为WMV编码，以便于Mediaplayer直接播放
	 *  \param[in]  source_file			源文件
	 *  \param[in]  des_file			目标文件
	 *  \param[in]  dwBitrate			1 <= 视频比特率 <= 20480 （单位Kbps）
	 *  \return 	成功: TRUE, 			失败: FALSE
	 *	\endif
     */
	NET_API BOOL CAL_CALL NET_API_ConvertMovToAVI(const char* source_file, const char* des_file, DWORD dwBitrate);
	//@}
#ifdef   __cplusplus
}
#endif

#endif

/*! 
\eng
\mainpage Programming guides

This manual is divided in the following sections:
- \subpage callseq
- \subpage faq
- \subpage player_callseq
- \subpage player_faq

API reference by group:
- \ref sdk
- \ref login
- \ref config
- \ref log
- \ref preview
- \ref ptz
- \ref playback
- \ref download
- \ref alarm
- \ref voice
- \ref serial
- \ref record
- \ref resuming
- \ref capture
- \ref system
- \ref player
- \ref ui_api

<b>Revision History:</b>
<table width="80%" class="doxtable">
<tr><td>Revision</td><td>Date</td><td>Description</td><td>Author</td></tr>
<tr><td>1.00</td><td>05/06/2010</td><td>Release Version</td><td>Xiao Jian</td></tr>
<tr><td>1.10</td><td>06/17/2011</td><td>Doxygen Version</td><td>Xiao Jian</td></tr>
</table>

\else

\mainpage 编程指南

本开发指南分为以下部分：
- \subpage callseq
- \subpage faq
- \subpage player_callseq
- \subpage player_faq

以下是按功能分组的API指南:
- \ref sdk
- \ref login
- \ref config
- \ref log
- \ref preview
- \ref ptz
- \ref playback
- \ref download
- \ref alarm
- \ref voice
- \ref serial
- \ref record
- \ref resuming
- \ref capture
- \ref system
- \ref player
- \ref ui_api

<b>修订记录:</b>
<table width="80%" class="doxtable">
<tr><td>版本号</td><td>日期</td><td>说明</td><td>修订人</td></tr>
<tr><td>1.00</td><td>05/06/2010</td><td>文档发行版本</td><td>Xiao Jian</td></tr>
<tr><td>1.10</td><td>06/17/2011</td><td>Doxygen文档发行版本</td><td>Xiao Jian</td></tr>
</table>

\endif
*/

/*! 
\eng
\page callseq Function calling sequence
This page shows common calling sequence of the SDK APIs.

\section section1 Initialization
<table width="80%" class="doxtable">
<tr><td width="50%">Initialize SDK</td><td>::NET_API_Init</td></tr>
<tr><td>Set callback for SDK Exception message</td><td>::NET_API_SetExceptionCallBack</td></tr>
<tr><td>User Login</td><td>::NET_API_Login</td></tr>
<tr><td>Set callback for device message</td><td>::NET_API_SetDVRMessageCallBack</td></tr>
<tr><td>Get last error</td><td>::NET_API_GetLastError</td></tr>
</table>
\see login, sdk

\section section2 Preview
<table width="80%" class="doxtable">
<tr><td width="50%">Start preview</td><td>::NET_API_RealPlay</td></tr>
<tr><td>Set/Get device configuration</td><td>::NET_API_SetDVRConfig<br/>
::NET_API_GetDVRConfig</td></tr>
<tr><td>Start playing audio</td><td>::NET_API_OpenSound</td></tr>
<tr><td>Adjust volume</td><td>::NET_API_Volume</td></tr>
<tr><td>Stop playing audio</td><td>::NET_API_CloseSound</td></tr>
<tr><td>Capture data</td><td>
::NET_API_SaveRealData<br/>
::NET_API_StopSaveRealData<br/>
::NET_API_SetRealDataCallBack<br/>
::NET_API_SetRealDataCallBackEx2</td></tr>
<tr><td>Capture picture</td><td>::NET_API_CapturePicture</td></tr>
<tr><td>Capture picture to jpeg</td><td>::NET_API_CapturePictureToJpeg</td></tr>
<tr><td>Draw on screen</td><td>::NET_API_RegisterDrawFun</td></tr>
<tr><td>PTZ control</td><td>::NET_API_GetPTZCtrl<br/>
::NET_API_PTZControl<br/>
::NET_API_PTZControl_Ex<br/>
::NET_API_PTZControlWithSpeed<br/>
::NET_API_TransPTZ<br/>
::NET_API_PTZPreset<br/>
::NET_API_PTZGuardPoint<br/>
::NET_API_PTZCruise<br/>
::NET_API_PTZTrack</td></tr>
<tr><td>Stop preview</td><td>::NET_API_StopRealPlay</td></tr>
</table>
\see preview, ptz, capture

\section section3 File search and playback
<table width="80%" class="doxtable">
<tr><td width="50%">Search file</td><td>::NET_API_FindFile<br/>
::NET_API_FindNextFile<br/>
::NET_API_FindClose</td></tr>
<tr><td>Playback</td><td>::NET_API_PlayBackByName<br/>
::NET_API_PlayBackByTime<br/>
::NET_API_PlayBackControl<br/>
::NET_API_StopPlayBack</td></tr>
<tr><td>Capture data</td><td>::NET_API_SetPlayDataCallBack<br/>
::NET_API_SetRealDataCallBackEx2</td></tr>
<tr><td>Capture picture</td><td>::NET_API_CapturePicture</td></tr>
<tr><td>Capture picture to jpeg</td><td>::NET_API_CapturePictureToJpeg</td></tr>
<tr><td>Download file</td><td>::NET_API_GetFileByName<br/>
::NET_API_GetDownloadPos<br/>
::NET_API_StopGetFile</td></tr>
</table>
\see playback, download, capture

\section section4 System operation
<table width="80%" class="doxtable">
<tr><td width="50%">Restore default configuration</td><td>::NET_API_RestoreConfig</td></tr>
<tr><td>Set/Get configuration</td><td>::NET_API_GetDVRConfig<br/>::NET_API_SetDVRConfig</td></tr>
<tr><td>Save configuration</td><td>::NET_API_SaveConfig</td></tr>
<tr><td>Device reboot</td><td>::NET_API_RebootDVR</td></tr>
<tr><td>Remote upgrade</td><td>::NET_API_Upgrade<br/>::NET_API_GetUpgradeState<br/>::NET_API_CloseUpgradeHandle</td></tr>
<tr><td>Remote disk format</td><td>::NET_API_FormatDisk<br/>::NET_API_GetFormatProgress<br/>::NET_API_CloseFormatHandle</td></tr>
</table>
\see config, system

\section section5 Alarm
<table width="80%" class="doxtable">
<tr><td width="50%">Start listen alarm messages</td><td>::NET_API_SetDVRMessageCallBack<br/>::NET_API_StartListen<br/>::NET_API_SetupAlarmChan</td></tr>
<tr><td>Stop listen alarm messages</td><td>::NET_API_StopListen<br/>::NET_API_CloseAlarmChan</td></tr>
<tr><td>Get/set alarm output state</td><td>::NET_API_SetAlarmOut<br/>::NET_API_GetAlarmOut</td></tr>
</table>
\see alarm

\section section6 Audio/Video communication
<table width="80%" class="doxtable">
<tr><td width="50%">Voice communication</td><td>::NET_API_StartVoiceCom <br/>::NET_API_SetVoiceComClientVolume <br/>::NET_API_StopVoiceCom</td></tr>
<tr><td>Audio/Video communication</td><td>::NET_API_StartVoiceCom_MR<br/>::NET_API_StopVoiceCom</td></tr>
<tr><td>Send audio data</td><td>::NET_API_VoiceComSendData</td></tr>
<tr><td>Send audio and video data</td><td>::NET_API_VoiceComSendDataEx</td></tr>
</table>
\see voice

\section section7 Transparent channel	
<table width="80%" class="doxtable">
<tr><td width="50%">Transparent channel</td><td>::NET_API_SerialStart <br/>::NET_API_SerialSend <br/>::NET_API_SerialStop</td></tr>
</table>
\see serial

\section section8 Manual recording
<table width="80%" class="doxtable">
<tr><td width="50%">Manual recording</td><td>::NET_API_StartDVRRecord <br/>::NET_API_StopDVRRecord</td></tr>
</table>
\see record

\section section9 Log
<table width="80%" class="doxtable">
<tr><td width="50%">Log</td><td>::NET_API_FindDVRLog <br/>::NET_API_FindNextLog <br/>::NET_API_FindLogClose</td></tr>
</table>
\see log

\section section10 Release resources
<table width="80%" class="doxtable">
<tr><td width="50%">Logout user</td><td>::NET_API_Logout</td></tr>
<tr><td>Release SDK resources</td><td>::NET_API_Cleanup</td></tr>
</table>
\see login, sdk

\else

\page callseq 函数调用顺序
本页面对SDK API的一般调用顺序进行了说明。

\section section1 初始化
<table width="80%" class="doxtable">
<tr><td width="50%">初始化SDK</td><td>::NET_API_Init</td></tr>
<tr><td>设置接收SDK异常消息的回调函数</td><td>::NET_API_SetExceptionCallBack</td></tr>
<tr><td>用户登录设备</td><td>::NET_API_Login</td></tr>
<tr><td>设置接收设备消息的回调函数</td><td>::NET_API_SetDVRMessageCallBack</td></tr>
<tr><td>获取最后发生的错误</td><td>::NET_API_GetLastError</td></tr>
</table>
\see login, sdk

\section section2 预览
<table width="80%" class="doxtable">
<tr><td width="50%">启动图像预览</td><td>::NET_API_RealPlay</td></tr>
<tr><td>设置/获取设备参数</td><td>::NET_API_SetDVRConfig<br/>::NET_API_GetDVRConfig</td></tr>
<tr><td>播放声音</td><td>::NET_API_OpenSound</td></tr>
<tr><td>调整音量大小</td><td>::NET_API_Volume</td></tr>
<tr><td>停止播放声音</td><td>::NET_API_CloseSound</td></tr>
<tr><td>捕获数据</td><td>::NET_API_SaveRealData<br/>::NET_API_StopSaveRealData<br/>::NET_API_SetRealDataCallBack<br/>::NET_API_SetRealDataCallBackEx2</td></tr>
<tr><td>抓图</td><td>::NET_API_CapturePicture</td></tr>
<tr><td>Jpeg抓图</td><td>::NET_API_CapturePictureToJpeg</td></tr>
<tr><td>叠加字幕/图像</td><td>::NET_API_RegisterDrawFun</td></tr>
<tr><td>云台控制</td><td>::NET_API_GetPTZCtrl<br/>
::NET_API_PTZControl<br/>
::NET_API_PTZControl_Ex<br/>
::NET_API_PTZControlWithSpeed<br/>
::NET_API_TransPTZ<br/>
::NET_API_PTZPreset<br/>
::NET_API_PTZGuardPoint<br/>
::NET_API_PTZCruise<br/>
::NET_API_PTZTrack</td></tr>
<tr><td>停止预览</td><td>::NET_API_StopRealPlay</td></tr>
</table>
\see preview, ptz, capture

\section section3 文件检索和回放
<table width="80%" class="doxtable">
<tr><td width="50%">查找文件</td><td>::NET_API_FindFile<br/>
::NET_API_FindNextFile<br/>
::NET_API_FindClose</td></tr>
<tr><td>回放</td><td>::NET_API_PlayBackByName<br/>
::NET_API_PlayBackByTime<br/>
::NET_API_PlayBackControl<br/>
::NET_API_StopPlayBack</td></tr>
<tr><td>捕获数据</td><td>::NET_API_SetPlayDataCallBack<br/>
::NET_API_SetRealDataCallBackEx2</td></tr>
<tr><td>抓图</td><td>::NET_API_CapturePicture</td></tr>
<tr><td>Jpeg抓图</td><td>::NET_API_CapturePictureToJpeg</td></tr>
<tr><td>下载录像文件</td><td>::NET_API_GetFileByName<br/>
::NET_API_GetDownloadPos<br/>
::NET_API_StopGetFile</td></tr>
</table>
\see playback, download, capture

\section section4 系统操作
<table width="80%" class="doxtable">
<tr><td width="50%">恢复默认值</td><td>::NET_API_RestoreConfig</td></tr>
<tr><td>获取设置参数</td><td>::NET_API_GetDVRConfig<br/>::NET_API_SetDVRConfig</td></tr>
<tr><td>保存参数</td><td>::NET_API_SaveConfig</td></tr>
<tr><td>重启设备</td><td>::NET_API_RebootDVR</td></tr>
<tr><td>远程升级</td><td>::NET_API_Upgrade<br/>::NET_API_GetUpgradeState<br/>::NET_API_CloseUpgradeHandle</td></tr>
<tr><td>远程格式化硬盘</td><td>::NET_API_FormatDisk<br/>::NET_API_GetFormatProgress<br/>::NET_API_CloseFormatHandle</td></tr>
</table>
\see config, system

\section section5 报警
<table width="80%" class="doxtable">
<tr><td width="50%">开始监听报警信息</td><td>::NET_API_SetDVRMessageCallBack<br/>::NET_API_StartListen<br/>::NET_API_SetupAlarmChan</td></tr>
<tr><td>停止监听报警信息</td><td>::NET_API_StopListen<br/>::NET_API_CloseAlarmChan</td></tr>
<tr><td>获取/设置报警输出状态</td><td>::NET_API_SetAlarmOut<br/>::NET_API_GetAlarmOut</td></tr>
</table>
\see alarm

\section section6 语音对讲
<table width="80%" class="doxtable">
<tr><td width="50%">语音对讲</td><td>::NET_API_StartVoiceCom <br/>::NET_API_SetVoiceComClientVolume <br/>::NET_API_StopVoiceCom</td></tr>
<tr><td>音视频转发</td><td>::NET_API_StartVoiceCom_MR<br/>::NET_API_StopVoiceCom</td></tr>
<tr><td>发送音频数据</td><td>::NET_API_VoiceComSendData</td></tr>
<tr><td>发送音视频数据</td><td>::NET_API_VoiceComSendDataEx</td></tr>
</table>
\see voice

\section section7 透明通道	
<table width="80%" class="doxtable">
<tr><td width="50%">透明通道</td><td>::NET_API_SerialStart <br/>::NET_API_SerialSend <br/>::NET_API_SerialStop</td></tr>
</table>
\see serial

\section section8 远程手动录像
<table width="80%" class="doxtable">
<tr><td width="50%">远程手动录像</td><td>::NET_API_StartDVRRecord <br/>::NET_API_StopDVRRecord</td></tr>
</table>
\see record

\section section9 日志
<table width="80%" class="doxtable">
<tr><td width="50%">检索远程日志</td><td>::NET_API_FindDVRLog <br/>::NET_API_FindNextLog <br/>::NET_API_FindLogClose</td></tr>
</table>
\see log

\section section10 释放资源
<table width="80%" class="doxtable">
<tr><td width="50%">注销用户</td><td>::NET_API_Logout</td></tr>
<tr><td>释放软件包资源</td><td>::NET_API_Cleanup</td></tr>
</table>
\see login, sdk

\endif
*/

/*! 
\eng
\page faq Frequently asked question
This page shows FAQ for the SDK.

\section faq1 Login always failed
- Check if ::NET_API_Init has been called before login. \n
- Check device ip address and port are correct. Default port is 30000.

\else
\page faq 常见问题
本页面列举了SDK常见的使用问题.

\section faq1 登录始终失败
- 检查 ::NET_API_Init 是否在登录前被调用了. \n
- 检查设备的IP地址和端口号是否正确. 默认端口为30000.

\endif

*/
