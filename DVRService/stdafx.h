// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define BOOST_LIB_DIAGNOSTIC
#define BOOST_ALL_DYN_LINK

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "lib/curl/libcurl.lib")
#pragma comment(lib, "lib/dvr/H264_NET_API.lib")
#pragma comment(lib, "lib/boost/lib/boost_system-vc100-mt-1_57.lib")
#pragma comment(lib, "lib/boost/lib/boost_filesystem-vc100-mt-1_57.lib")
#pragma comment(lib, "lib/boost/lib/boost_thread-vc100-mt-1_57.lib")
#pragma comment(lib, "lib/boost/lib/boost_date_time-vc100-mt-1_57.lib")
#pragma comment(lib, "lib/boost/lib/boost_chrono-vc100-mt-1_57.lib")

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#if defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WINDOWS__) || defined(__TOS_WIN__)
#include <Windows.h>
#include <io.h>
#include <BaseTsd.h>
#include <WinDef.h>
#include <WinNT.h> 
inline void delay( unsigned long ms ) {
	Sleep( ms );
}
#else  /* presume POSIX */
#include <unistd.h>
inline void delay( unsigned long ms ){
	usleep( ms * 1000 );
}
#endif
#ifdef _MSC_VER
#include <boost/config/compiler/visualc.hpp>
#endif

using namespace std;

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/bind.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "lib/dvr/H264_NET_API.h"
#include <cassert>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <utility>
#include <sys/stat.h>
#include "lib/curl/curl.h"
#include "CurlX.h"
#include "RequestProcess.h"

//using namespace boost;
using namespace std;
// TODO: reference additional headers your program requires here