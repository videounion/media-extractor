/** \file	h264_ui_api.h
 *  \eng
 *  \brief	This file contains interfaces for show common dialogs provide by sdk.
 *	\else
 *  \brief	此文件包含显示SDK公用对话框的接口.
 *	\endif
 *  \author Jian.Xiao
 *	\version 1.0
 */

#ifndef H264_UI_API_H
#define H264_UI_API_H

#ifdef __cplusplus
extern "C" {
#endif	
/** 
 *	\eng
 *  \defgroup ui_api Common UI API
 *  API provide by SDK UI library. 
 *	\else
 *  \defgroup ui_api 共用对话框接口函数
 *  SDK共用界面库提供的接口函数. 
 *	\endif
 */
//@{
/**
 *	\eng
 *  \brief		Set UI language for UI library. If this function is not called, UI library will use system default language, or English.
 *  \param[in]  wszLocalName		Wide char string of UI language code, now support following languages:
 *											\li English - L"en-US"
 *											\li Chinese - L"zh-CN"
 *											\li Turkish - L"tr-TR"
 *											\li System default - NULL
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		设置UI库所使用的语言. 如果本函数没有被调用，UI库将会使用系统默认语言，或者英文。
 *  \param[in]  wszLocalName		UI语言区域代码（宽字节字符串）, 当前支持以下语言:
 *											\li 英文 - L"en-US"
 *											\li 中文 - L"zh-CN"
 *											\li 土耳其语 - L"tr-TR"
 *											\li 系统默认语言 - NULL
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_SetLang(LPCWSTR wszLocalName);
/**
 *	\eng
 *  \brief		Show log query dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示日志检索对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowLogUI(int hDVR, HWND hParent);
/**
 *	\eng
 *  \brief		Show jpeg query dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示JPEG检索对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowJpegQueryUI(int hDVR, HWND hParent);
/**
 *	\eng
 *  \brief		Show settings dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示参数设置对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowSettingsUI(int hDVR, HWND hParent);
/**
 *	\eng
 *  \brief		Show record query dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示录像检索对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowPlaybackUI(int hDVR, HWND hParent);

//! \~english Record query parameters. \~chinese 录像查询参数
typedef struct
{
	int hDVR;		//!< \~english Login ID returned by ::NET_API_Login for group \~chinese ::NET_API_Login 返回的分组中多个设备的登录ID
	unsigned int nChannel;//!< \~english The channel number in group list \~chinese 分组列表中对应的设备通道号
	wchar_t sChannelName[256];//!< \~english Channel name in group list \~chinese 分组列表中对应的设备通道名称
}H264_UI_RECORD_QUERY_PARAMETERS;
/**
 *	\eng
 *  \brief		Show single channle record query dialog.
 *  \param[in]  rqParameters		Array of record query parameters
 *  \param[in]  nCount				Array size of record query parameters
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示单路录像检索对话框。
 *  \param[in]  rqParameters		录像查询参数数组
 *  \param[in]  nCount				录像查询参数数组大小
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowPlaybackUI_GROUP(H264_UI_RECORD_QUERY_PARAMETERS* rqParameters, unsigned int nCount, HWND hParent);
/**
 *	\eng
 *  \brief		Show multi-channel record query dialog.
 *  \param[in]  rqParameters		Array of record query parameters
 *  \param[in]  nCount				Array size of record query parameters
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示多路录像检索对话框。
 *  \param[in]  rqParameters		录像查询参数数组
 *  \param[in]  nCount				录像查询参数数组大小
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowRecordQueryUI(H264_UI_RECORD_QUERY_PARAMETERS* rqParameters, unsigned int nCount, HWND hParent);

//! \~english Playback by name parameters. \~chinese 按文件名回放参数
typedef struct 
{
	int hDVR;//!< \~english Login ID returned by ::NET_API_Login for group \~chinese ::NET_API_Login 返回的分组中多个设备的登录ID
	wchar_t sFileNames[256][32];//!< \~english File names for playback \~chinese 回放的文件名列表
	unsigned int sFileCount;	//!< \~english File count in playback list \~chinese 回放文件数量
	wchar_t sChannelName[256];//!< \~english Channel name in group list \~chinese 分组列表中对应的设备通道名称
}H264_UI_PLAYBACK_PARAMETERS_BYNAME;

//! \~english Playback by time parameters. \~chinese 按时间回放参数
typedef struct
{
	int hDVR;//!< \~english Login ID returned by ::NET_API_Login for group \~chinese ::NET_API_Login 返回的分组中多个设备的登录ID
	unsigned int nChannel;//!< \~english The channel number for playback \~chinese 回放的通道号
	NET_API_TIME StartTime;//!< \~english The start time for playback \~chinese 回放的起始时间
	NET_API_TIME StopTime;//!< \~english The stop time for playback \~chinese 回放的结束时间
	wchar_t sChannelName[256];//!< \~english Channel name in group list \~chinese 分组列表中对应的设备通道名称
}H264_UI_PLAYBACK_PARAMETERS_BYTIME;

/**
 *	\eng
 *  \brief		Show multi-channel playback dialog.
 *  rqParametersByName	Array of playback by name parameters
 *  \param[in]  rqParametersByTime	Array of playback by time parameters
 *  \param[in]  nCount				Array size of record query parameters
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示多路录像回放对话框。
 *  \param[in]  rqParametersByName	按名称回放参数数组
 *  \param[in]  rqParametersByTime	按时间回放参数数组
 *  \param[in]  nCount				回放参数数组大小
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowMultiPlaybackUI(H264_UI_PLAYBACK_PARAMETERS_BYNAME* rqParametersByName,H264_UI_PLAYBACK_PARAMETERS_BYTIME* rqParametersByTime, unsigned int nCount, HWND hParent);

//! \~ \brief	\~english Show disk space calculator dialog \~chinese 显示磁盘空间计算对话框
//! \~ \param[in]  hParent	\~english Handle of dialog's parent window \~chinese 对话框的父窗口句柄
//! \~ \return	\~english Success: TRUE, 		Fail: FALSE	\~chinese 成功: TRUE, 失败: FALSE
BOOL PASCAL H264_UI_ShowDiskCalculateToolUI(HWND hParent);

/**
 *	\eng
 *  \brief		Show disk status dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示磁盘状态对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowDiskStatusUI(int hDVR, HWND hParent);

/**
 *	\eng
 *  \brief		Show device status dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示设备状态对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowDeviceStatusUI(int hDVR, HWND hParent);

/**
 *	\eng
 *  \brief		Show alarm status dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示报警状态对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowAlarmStatusUI(int hDVR, HWND hParent);

/**
 *	\eng
 *  \brief		Show channel status dialog.
 *  \param[in]  hDVR				Login ID returned by ::NET_API_Login 
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示通道状态对话框。
 *  \param[in]  hDVR				::NET_API_Login 返回的登录ID
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_ShowChannelStatusUI(int hDVR, HWND hParent);

struct  device_baseinfo
{
	unsigned char cmd;
    NET_API_DEVICECFG device_info;
    NET_API_TIME_ZONE time_zone;
    char version_string[64];
    char ip[16];
    unsigned short port;
};
/**
 *	\eng
 *  \brief		Show Available Device info dialog.
 *  \param[in]  hParent				Handle of dialog's parent window
 *  \param[in]  lParam				Message Function in dialog's parent window （Recive Double-Click Event）
 *  \return		Success: TRUE, 		Fail: FALSE
 *	\else
 *  \brief		显示在线设备信息。
 *  \param[in]  hParent				对话框的父窗口句柄
 *  \param[in]  lParam				父窗口接收对话框双击事件的消息函数，允许为NULL
 *  \return		成功: TRUE, 失败: FALSE
 *	\endif
 */
BOOL PASCAL H264_UI_GetAvailableDeviceInfo( HWND hParent,LPARAM lParam);
//@}

#ifdef __cplusplus
}//	extern "C"
#endif	

#endif // #ifndef H264_UI_API_H
