#pragma once
class CurlX {
	public:
		std::string get(std::string url, std::string name, std::string code, std::string request);
		std::string upload( string url, string name, string code, string status, string status_message, boost::property_tree::ptree::value_type p, string filePath, string newName );
		std::string ftpUpload( const char * local_file, const char * remote_url, string name );
};

